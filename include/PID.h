#define PID_D_FILTER_LENGTH	5

class PID {
private:
	double reference;
	double feedback;
	double output;

	double Kp, Ki, Kd;

	// internal state
	double integrator;
	double lastError;
	double errorHistory[PID_D_FILTER_LENGTH];
	double errorHistoryI[PID_D_FILTER_LENGTH];

	bool started;


public:
	PID();
	virtual ~PID();

	inline void setGains(const double p, const double i, const double d){ Kp = p; Ki = i; Kd = d; }
	inline void getGains(double& p, double& i, double& d){ p = Kp; i = Ki, d = Kd; }
	inline void setReference(double ref){ reference = ref; }
	inline void setFeedback(double measure){ feedback = measure; }
	double getOutput();

	void reset();

};