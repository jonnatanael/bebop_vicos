#include <string>
#include <sstream>
#include <vector>
#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/video/tracking.hpp>

class util {
private:


public:
	static std::string getImgType(int imgTypeInt);
	static std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems);
	static std::vector<std::string> split(const std::string &s, char delim);
	static void printVector(const std::vector<std::string> v);
	static void init_KF(cv::KalmanFilter* KF, cv::Mat_<float>* measurement, int cam_w = 640, int cam_h = 368);
	static cv::Point update_KF(cv::KalmanFilter* KF, cv::Mat_<float>* measurement, int x, int y, int h, int w);
	static void toggle(bool& a);
	static double rad2deg(float input);
	static double deg2rad(float input);
};