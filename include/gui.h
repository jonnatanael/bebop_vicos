#include <cv_bridge/cv_bridge.h>
#include <opencv2/highgui/highgui.hpp>

class gui {
private:
	static const int linetype = CV_AA;
	static const int font = cv::FONT_HERSHEY_TRIPLEX;

public:
	static void writeText(cv_bridge::CvImagePtr cv_ptr, cv::Point position, std::string text, cv::Scalar color = cv::Scalar(255, 255, 255));
	static void writeText(cv::Mat img, cv::Point position, std::string text, cv::Scalar color = cv::Scalar(255, 255, 255));
	static void writeStatusText(cv_bridge::CvImagePtr cv_ptr, bool tracking, bool lost);
	static void drawRec(cv_bridge::CvImagePtr cv_ptr);
	static void drawBattery(cv_bridge::CvImagePtr cv_ptr, int battery);
	static void drawAlphaBorder(cv_bridge::CvImagePtr cv_ptr, cv::Rect r, double opacity, cv::Scalar clr, int thickness, bool alt = false);
	static void drawAlphaRectangle(cv_bridge::CvImagePtr cv_ptr, cv::Rect r, double opacity, cv::Scalar clr, int thickness);
	static void drawAlphaLine(cv_bridge::CvImagePtr cv_ptr, cv::Point a, cv::Point b, double opacity, cv::Scalar clr, int thickness);
	static void handle_queue(cv_bridge::CvImagePtr cv_ptr, std::vector<cv::Point>* v, cv::Point p, cv::Scalar clr, int v_lim = 40);
};