#include <nodelet/nodelet.h>
#include "ros/ros.h"
#include <std_msgs/Empty.h>
#include <sensor_msgs/Image.h>
#include <bebop_vicos/Im_reg.h>
#include "tracker.h"
#include <opencv2/features2d/features2d.hpp>

namespace bebop_vicos
{

class feature_nodelet : public nodelet::Nodelet
{
private:

	ros::Subscriber sub_image;
	ros::Subscriber sub_shutdown;

	ros::Publisher pub_feat;
	ros::Publisher pub_KP;

	cv::Mat im_prev;
	std::vector<cv::Point2f> features;
	std::vector<cv::KeyPoint> featuresKP;

	int n_feat;
	int threshold;
	int method;

	void publishFeatures();
	void publishKP();
	

public:
	virtual void onInit();
	void imageCallback(const sensor_msgs::ImageConstPtr& cam_msg);
	void shutdownCallback(std_msgs::Empty request);

};

}