#include <nodelet/nodelet.h>
#include "ros/ros.h"
#include <std_msgs/Empty.h>
#include <sensor_msgs/Image.h>
#include <geometry_msgs/Point.h>
#include <bebop_vicos/Im_reg.h>
#include "tracker.h"

namespace bebop_vicos
{

class tracker_nodelet : public nodelet::Nodelet
{
private:

	ros::Subscriber sub_image;
	ros::Subscriber sub_im_reg;
	ros::Subscriber sub_shutdown;
	ros::Subscriber sub_stop;
	ros::Subscriber sub_reinit;
	ros::Subscriber sub_set_pos;

	ros::Publisher pub_region;
	ros::Publisher pub_bundle;
	ros::Publisher pub_status;

	bool tracking;
	bool lost;
	bool redetect;

	Ptr<legit::tracker::Tracker> t;
	Config config;

	string par;
	string image_topic = "/bebop/image_raw";

	int cam_h;
	int cam_w;

	bool inited;
	bool tracker_created;

	int lost_threshold = 7;

	double psr = -1;
	int psr_count = 1;
	int psr_count_backup = 1;
	double psr_prev = -1;

public:
	virtual void onInit();
	void trackerCallback(const sensor_msgs::ImageConstPtr& cam_msg);
	double getPSR(cv::Mat corr);
	void get_glob_mask(cv::Mat &region, cv::Mat &response, cv::Mat &mask_fg);
	void handle_config();
	void stopCallback(std_msgs::Empty request);
	void shutdownCallback(std_msgs::Empty request);
	void initCallback(bebop_vicos::Im_reg msg);
	void setPosCallback(geometry_msgs::Point msg);

};

}