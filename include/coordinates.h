#include <cv_bridge/cv_bridge.h>
#include <opencv2/highgui/highgui.hpp>

class coordinates {
private:

	// image size
	static const int cam_h = 368;
	static const int cam_w = 640;

	static constexpr double tz = -15; // camera translation

public:
	static void drawAngles(cv_bridge::CvImagePtr cv_ptr, double rotX, double rotY, double rotZ);

};