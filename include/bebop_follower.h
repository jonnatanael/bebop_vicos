// includes for ROS
#include "ros/ros.h"
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <std_msgs/Empty.h>
#include <std_msgs/Int16.h>
#include <std_msgs/Bool.h>
#include <geometry_msgs/Twist.h>

#include <bebop_msgs/Ardrone3PilotingStateAttitudeChanged.h>
#include <bebop_msgs/Ardrone3CameraStateOrientation.h>
#include <bebop_msgs/Ardrone3PilotingStateFlyingStateChanged.h>

#include <bebop_vicos/Region.h>
#include <bebop_vicos/drone_PID.h>
#include <bebop_vicos/Im_reg.h>

// includes for PID
#include "PID.h"

using namespace std;
using namespace cv;

static const std::string OPENCV_WINDOW = "Image window";

// system properties
int cam_h = 368;
int cam_w = 640;
double fov_x = 80;
double fov_y = 50;
float focal_length = 400; // approx., should be read from calibration file
float max_pan = 35;
float max_tilt = 35;


// publishers
ros::Publisher cmd_pub;
ros::Publisher drone_PID_pub;

// subscribers
ros::Subscriber sub_shutdown;
ros::Subscriber sub_stop;
ros::Subscriber sub_region;
ros::Subscriber sub_attitude;
ros::Subscriber sub_im_reg;
ros::Subscriber sub_target_angle_status;
ros::Subscriber sub_gamma;
ros::Subscriber sub_cam_pos;
ros::Subscriber sub_flying_state;


// auxiliary variables

int image_center_x = cam_w / 2;
int image_center_y = cam_h / 2;

// PID controller references
double x_ref = 0; // error related to image center
double y_ref = 0;
double d_ref = 0; // error related to init area size

// specifies target camera position
double cam_x_ref = 0;
double cam_y_ref = 0;

float cam_x = 1e5;
float cam_y = 1e5;

// camera correction params
double pan_factor, tilt_factor;

geometry_msgs::Twist msg;

float navdata_pitch, navdata_roll, navdata_yaw;

//PID controllers
PID pid_pitch, pid_roll, pid_yaw, pid_z;

// PID parameters
float pitch_Kp, pitch_Ki, pitch_Kd;
float roll_Kp, roll_Ki, roll_Kd;
float yaw_Kp, yaw_Ki, yaw_Kd;
float z_Kp, z_Ki, z_Kd;

// tracker region
int x, y, w, h, x_c, y_c;
int gamma_angle;
float gamma_rad;

double area = -1;
double area_ref = -1;
bool target_angle_enabled = false;

int flying_state = -1; // 0=landed, 1=taking_off, 2=hovering, 3=flying, 4=landing, 5=emergency

// comparison
bool pestana = false;
