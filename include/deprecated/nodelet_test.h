#include <nodelet/nodelet.h>
#include <sensor_msgs/Image.h>
#include "ros/ros.h"
#include <cv_bridge/cv_bridge.h>
#include <opencv2/core/core.hpp>
#include <bebop_msgs/Ardrone3MediaRecordStatePictureStateChangedV2.h>
#include <bebop_msgs/Ardrone3PilotingStateAttitudeChanged.h>
#include <bebop_msgs/CommonCommonStateBatteryStateChanged.h>
#include <bebop_msgs/Ardrone3PilotingStateAttitudeChanged.h>
#include <bebop_msgs/Ardrone3PilotingStateSpeedChanged.h>
#include <bebop_vicos/Region.h>
#include <bebop_vicos/Status.h>

namespace bebop_vicos
{

class nodelet_test : public nodelet::Nodelet
{
private:
	ros::Subscriber cam_sub;
	ros::Subscriber rec_sub;
	ros::Subscriber battery_sub;
	ros::Subscriber attitude_sub;
	ros::Subscriber speed_sub;
	ros::Subscriber status_sub;

	// tracker related subs
	ros::Subscriber sub_region;


	ros::Publisher im_reg_pub;
  	ros::Publisher shutdown_pub;

  	std::string OPENCV_WINDOW = "Master window";

	int cam_h = 368;
	int cam_w = 640;

	int battery = 95;

	// drone attitude angles
	double rotX, rotY, rotZ;

	// object region data
	int x, y, h, w;

	bool test = 1;
	// for tracker failure
	bool tracking = false;
	bool lost = false;

	// for mouse
	bool clicked;
	cv::Rect sel;
	cv::Point P1;
	cv::Point P2;
	bool redetecting = false;

	//sensor_msgs::Image frame;

public:
	virtual void onInit();
	void ImageCallback(const sensor_msgs::ImageConstPtr& cam_msg);
	void Ardrone3MediaRecordStatePictureStateChangedCallback(const bebop_msgs::Ardrone3MediaRecordStatePictureStateChangedV2& msg);
	void batteryCallback(const bebop_msgs::CommonCommonStateBatteryStateChanged& msg);
	void attitudeCallback(const bebop_msgs::Ardrone3PilotingStateAttitudeChanged& msg);
	void speedCallback(const bebop_msgs::Ardrone3PilotingStateSpeedChanged& msg);
	void regionCallback(const bebop_vicos::Region& reg);
	void processRegion(cv_bridge::CvImagePtr cv_ptr);
	void statusCallback(const bebop_vicos::Status& status);
	static void onMouse( int event, int x, int y, int f, void* param);


};

}