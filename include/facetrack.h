#include <vector>
#include <fstream>
#include <string>
#include <boost/filesystem.hpp>

// includes for OpenCV
#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/tracking.hpp>

// includes for ROS
#include "ros/ros.h"
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/Image.h>
#include <std_msgs/Empty.h>
#include <geometry_msgs/Twist.h>

// custom drone messages for tracking
#include <bebop_vicos/Region.h>
#include <bebop_vicos/Status.h>
#include <bebop_vicos/Im_reg.h>

// face detector message
#include <facedetector/Detection.h>

// bebop messages
#include <bebop_msgs/Ardrone3CameraStateOrientation.h>

static const std::string OPENCV_WINDOW = "Facetrack window";

int cam_h = 368;
int cam_w = 640;


// subscribers
ros::Subscriber sub_image;
ros::Subscriber sub_fd;
ros::Subscriber sub_region;
ros::Subscriber sub_status;
ros::Subscriber sub_cam_pos;

// publishers
ros::Publisher cam_move_pub;
ros::Publisher im_reg_pub;
ros::Publisher shutdown_pub;
ros::Publisher stop_pub;

sensor_msgs::Image frame; // needed for mouse initialization

// for tracker
std::string par; // tracker config file
int x, y, h, w, psr; // current tracker region


// for object selection with mouse
bool clicked = false;
bool tracking = false;
cv::Rect sel(0, 0, 0, 0);
cv::Point P1(0, 0);
cv::Point P2(0, 0);
bool lost = false; // for tracker failure
bool redetecting = false;

// for virtual camera pan/tilt
int cam_y = 0;
int cam_z = 0;
int camera_shift_angle = 7;
int camera_pan_limit= 35;
int camera_tilt_limit_up = 35;
int camera_tilt_limit_down = -70;


// trackbar parameters
int mode = 1;
int parameter = 1;

// detected faces
facedetector::Detection faces;