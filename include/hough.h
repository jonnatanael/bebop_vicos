#include <vector>
#include <fstream>
#include <string>
#include <boost/filesystem.hpp>

// includes for OpenCV
#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/tracking.hpp>

// includes for ROS
#include "ros/ros.h"
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/Image.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/Empty.h>


static const std::string OPENCV_WINDOW = "Hough window";

int cam_h = 368;
int cam_w = 640;

// za označevanje objekta z miško
bool clicked = false;
bool tracking = false;
cv::Rect sel(0, 0, 0, 0);
cv::Point P1(0, 0);
cv::Point P2(0, 0);


ros::Subscriber sub_image;
ros::Publisher cam_move_pub;

int cam_y = 0;
int cam_z = 0;
int camera_shift_angle = 7;
int camera_pan_limit= 35;
int camera_tilt_limit_up = 35;
int camera_tilt_limit_down = -70;

int prob = 1;
int minLen = 30;
double rhoRes = 1;
double thetaRes = CV_PI / 180;
int maxGap = 10;