#include <vector>
#include <fstream>
#include <string>
#include <boost/filesystem.hpp>

// includes for OpenCV
#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/tracking.hpp>

// includes for ROS
#include "ros/ros.h"
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/Image.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Point.h>
#include <std_msgs/Empty.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Int16.h>

// custom drone messages for tracking
#include <bebop_vicos/Region.h>
#include <bebop_vicos/Status.h>
#include <bebop_vicos/Im_reg.h>
#include <bebop_vicos/PointVector.h>
#include <bebop_vicos/Point2D.h>
#include <bebop_vicos/KeyPoint.h>
#include <bebop_vicos/KPVector.h>
#include <bebop_vicos/drone_PID.h>

// bebop messages
#include <bebop_msgs/Ardrone3PilotingStateAttitudeChanged.h>
#include <bebop_msgs/Ardrone3PilotingStateAltitudeChanged.h>
#include <bebop_msgs/Ardrone3PilotingStateFlatTrimChanged.h>
#include <bebop_msgs/CommonCommonStateBatteryStateChanged.h>
#include <bebop_msgs/Ardrone3PilotingStateSpeedChanged.h>
#include <bebop_msgs/Ardrone3CameraStateOrientation.h>
#include <bebop_msgs/Ardrone3PilotingStateFlyingStateChanged.h>


// face detector message
#include <facedetector/Detection.h>

#include "PID.h"


static const std::string OPENCV_WINDOW = "Master window";
static const std::string DATA_WINDOW = "data";

int cam_h = 368;
int cam_w = 640;
int image_center_x = cam_w / 2;
int image_center_y = cam_h / 2;
float focal_length = 400; // approx., should be read from cailibration file

/*int cam_h = 720;
int cam_w = 1280;*/

// for object selection with mouse
bool clicked = false;
bool tracking = false;
cv::Rect sel(0, 0, 0, 0);
cv::Point P1(0, 0);
cv::Point P2(0, 0);
bool lost = false; // for tracker failure
bool redetecting = false;

// for one-click-init
cv::Point mouse;
float init_h = -1;
float init_w = -1;
cv::Rect init_region;
float init_scale = 0.1;

bool rec = false;

ros::Publisher cam_move_pub;
ros::Publisher drone_pub;
ros::Publisher im_reg_pub;
ros::Publisher shutdown_pub;
ros::Publisher stop_pub;
ros::Publisher snapshot_pub;
ros::Publisher record_pub;
ros::Publisher fd_toggle_pub;
ros::Publisher test_pub;
ros::Publisher tracker_set_reg_pub;
ros::Publisher target_angle_status_pub;
ros::Publisher gamma_pub;

ros::Subscriber sub_image;
ros::Subscriber sub_features;
ros::Subscriber sub_featuresKP;
ros::Subscriber sub_attitude;
ros::Subscriber sub_speed;
ros::Subscriber sub_altitude;
ros::Subscriber sub_flat_trim;
ros::Subscriber sub_bat;
ros::Subscriber sub_region;
ros::Subscriber sub_cam_pos;
ros::Subscriber sub_status;
ros::Subscriber sub_fd;
ros::Subscriber sub_util_buttons;
ros::Subscriber sub_drone_PID;

sensor_msgs::Image frame;

// tracker config file
std::string par;

// current tracker region
int x, y, h, w, psr;
int x_c, y_c;

// for battery status
int battery = 50;

// drone attitude angles
double rotX, rotY, rotZ;

// drone altitude
double altitude = -1;

// virtual camera position
float cam_y = 1e5;
float cam_x = 1e5;
int camera_shift_angle = 20;
int camera_pan_limit = 35;
int camera_tilt_limit = 35;
int camera_tilt_limit_up = 35;
int camera_tilt_limit_down = -35;

// for trajectory
bool trajectory = false;
int v_lim = 40;
std::vector<cv::Point> v;
std::vector<cv::Point> v_kf;
cv::KalmanFilter KF(4, 2, 0);
cv::Mat_<float> measurement(2, 1);

// for displaying error from drone pid
bool show_pid_error = false;


std::string image_topic = "/bebop/image_raw";


// recording files
std::string status_filename;
std::string region_filename;
std::string attitude_filename;
std::string altitude_filename;
std::string pid_cam_filename;
std::string pid_drone_filename;
std::string subdir;
std::string im_dir;
std::ofstream status_file;
std::ofstream region_file;
std::ofstream pid_cam_file;
std::ofstream pid_drone_file;
std::ofstream attitude_file;
std::ofstream altitude_file;

std::ofstream params_file;

cv::VideoWriter vw;
bool video; // whether the video should be saved (mostly for videos from bag files)

bool display_features = false;
bebop_vicos::PointVector pv;
bebop_vicos::KPVector kpv;
facedetector::Detection faces;
bool face_detector_running = false;

// recording on drone
bool bebop_rec = false;


bool move_cam; // whether camera moves to follow object
bool camera_move_pending = false;
bool look_at = false; // whether camera centers on the object
int init_mode = 0; // 0 -> rectangle, 1 -> click, 2-> face detector


// PID stuff for dynamic camera
PID pid_x, pid_y;
float x_Kp, x_Ki, x_Kd;
float y_Kp, y_Ki, y_Kd;
// target angle difference
double x_ref = 0;
double y_ref = 0;

bool target_angle_enabled;
int target_angle;

// for displaying data
cv::Mat data_image;
bool show_data;


// testing variables

cv::RNG rng;

float distance_from_center = 0;
bool test_running = false;
bool rec_inited = false;

cv::Rect predicted;
bool prediction_pending = false;
cv::Point prediction;
int default_delay = 15;
int delay = default_delay;

//int reg_x, reg_y, reg_w, reg_h;

int gt_x, gt_y, gt_w, gt_h, gt_pan, gt_tilt;
bool gt_set = false;

// function declarations
void set_tracker_pos(int x, int y);
bool move_circle(int value);

// za display PID rezultatov
float roll, pitch, yaw, z;

cv::Rect reference(0, 0, 0, 0);


// random move timeout
int move_timeout;
int move_timeout_backup = move_timeout;

int test_mode;
int move_size;

int test_counter = 0;
int center_deadband = 10;
bool predict = false;
int first_counter = -1;

double camera_control_deadband;


double cam_pid_x = 0;
double cam_pid_y = 0;