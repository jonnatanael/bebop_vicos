#include <nodelet/nodelet.h>
#include "ros/ros.h"
#include <std_msgs/Empty.h>
#include <sensor_msgs/Image.h>
#include <bebop_vicos/Im_reg.h>

namespace bebop_vicos
{

class boilerplate_nodelet : public nodelet::Nodelet
{
private:

	ros::Subscriber sub_image;
	ros::Subscriber sub_shutdown;

	ros::Publisher pub_region;
	ros::Publisher pub_bundle;
	ros::Publisher pub_status;

	int cam_h;
	int cam_w;


public:
	virtual void onInit();
	void imageCallback(const sensor_msgs::ImageConstPtr& cam_msg);
	void shutdownCallback(std_msgs::Empty request);

};

}