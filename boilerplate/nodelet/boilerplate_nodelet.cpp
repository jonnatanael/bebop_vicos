#include <boilerplate_nodelet.h>
#include <pluginlib/class_list_macros.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <sensor_msgs/image_encodings.h>
#include <std_msgs/Empty.h>

PLUGINLIB_EXPORT_CLASS(bebop_vicos::boilerplate_nodelet, nodelet::Nodelet);

namespace bebop_vicos
{
void boilerplate_nodelet::onInit()
{
	ros::NodeHandle& nh = getNodeHandle();
	ros::NodeHandle& private_nh = getPrivateNodeHandle();

	sub_image = nh.subscribe("/bebop/image_raw", 1, &boilerplate_nodelet::imageCallback, this);
	sub_shutdown = nh.subscribe("/master/shutdown", 1, &boilerplate_nodelet::shutdownCallback, this);

	// example publisher
	//pub_region = nh.advertise<bebop_vicos::Region>("/tracker/region", 1000);

	cam_h = 368;
	cam_w = 640;

	// example parameter
	//private_nh.getParam("cfg", par);

	NODELET_INFO("Initializing nodelet finished...");
	return;
}

void boilerplate_nodelet::shutdownCallback(std_msgs::Empty request) {
	cout << "nodelet shutdown" << endl;
	ros::shutdown();
}

void boilerplate_nodelet::imageCallback(const sensor_msgs::ImageConstPtr& cam_msg) {

	cv_bridge::CvImagePtr cv_ptr;
	try {
		cv_ptr = cv_bridge::toCvCopy(cam_msg, sensor_msgs::image_encodings::BGR8);
	}
	catch (cv_bridge::Exception& e) {
		NODELET_ERROR("cv_bridge exception: %s", e.what());
		return;
	}

	// do something with image

}

}