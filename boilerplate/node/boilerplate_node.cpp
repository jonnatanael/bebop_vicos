#include "boilerplate_node.h"

using namespace std;
using namespace cv;

void publish_camera_move(int y, int z)
{
  geometry_msgs::Twist msg;
  msg.angular.y = y;
  msg.angular.z = z;
  cam_move_pub.publish(msg);
}


void handle_key(int key, uint32_t sec, uint32_t nsec) {
  switch (key) {
  case (27): // 'esc' triggers shutdown
    ros::shutdown();
    break;
  case ((int)('w')):
    if (cam_y + camera_shift_angle <= camera_tilt_limit_up)
      cam_y += camera_shift_angle;
    else
      cam_y = camera_tilt_limit_up;
    publish_camera_move(cam_y, cam_z);
    break;
  case ((int)('s')):
    if (cam_y - camera_shift_angle >= camera_tilt_limit_down)
      cam_y -= camera_shift_angle;
    else
      cam_y = camera_tilt_limit_down;
    publish_camera_move(cam_y, cam_z);
    break;
  case ((int)('a')):
    if (cam_z - camera_shift_angle >= -camera_pan_limit)
      cam_z -= camera_shift_angle;
    else
      cam_z = -camera_pan_limit;
    publish_camera_move(cam_y, cam_z);
    break;
  case ((int)('d')):
    if (cam_z + camera_shift_angle <= camera_pan_limit)
      cam_z += camera_shift_angle;
    else
      cam_z = camera_pan_limit;
    publish_camera_move(cam_y, cam_z);
    break;
  }
}

void imageCallback(const sensor_msgs::ImageConstPtr& cam_msg) {
  cv_bridge::CvImagePtr cv_ptr;

  try {
    cv_ptr = cv_bridge::toCvCopy(cam_msg, sensor_msgs::image_encodings::BGR8);
  }
  catch (cv_bridge::Exception& e) {
    ROS_ERROR("cv_bridge exception: %s", e.what());
    return;
  }

  cv::imshow(OPENCV_WINDOW, cv_ptr->image);

  handle_key(cv::waitKey(1), cam_msg->header.stamp.sec, cam_msg->header.stamp.nsec); // needs sec and nsec for creating record directories

}

void on_trackbar( int, void* )
{

}


int main(int argc, char **argv)
{

  ros::init(argc, argv, "boilerplate_node");

  ros::NodeHandle n("~");

  sub_image = n.subscribe("/bebop/image_raw", 1, imageCallback);
  cam_move_pub = n.advertise<geometry_msgs::Twist>("/bebop/camera_control", 1000);

  cv::namedWindow(OPENCV_WINDOW, CV_WINDOW_AUTOSIZE);

  // example trackbars
  createTrackbar("Bool parameter", OPENCV_WINDOW, &prob, 1, on_trackbar);
  createTrackbar("Parameter", OPENCV_WINDOW, &parameter, 100);

  ros::spin();

  return 0;
}