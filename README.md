# Code for using vision algorihms on Parrot bebop drone with ROS.

## Notes (may be outdated due to significant changes):

Master node is standalone and handles visualization, gui and initialization of other programs

Tracker is implemented as a nodelet, which can be load into the bebop_nodelet_manager, or used standalone, not much difference was observed (possibly needs to explicitly use shared pointers, TODO).

Detector is not included yet, so tracker should always be run with redetect parameter set to false.

The package depends on bebop_autonomy package which includes the `bebop_driver` node and handles the ROS communication with the drone.

### Build:
The package is best built with: "catkin_make --pkg bebop_vicos" from the catkin workspace. All the necessary libraries are included in the github repository.

### Usage:
First, the drone is turned on and connected to via wifi, then "roslaunch bebop_driver bebop_nodelet.launch" (or node, depending if additional nodelets are used) is launched.
The master window can then be launched via "roslaunch bebop_vicos bebop_master.launch" which runs the master node and the tracker node in standalone mode (makes testing faster as the driver is launched separately).

### Nodes:
- `bebop_master`: handles the display of images and data, used to initialize the tracker
- `joy`: maps the joypad commands to the cmd_vel topic messages (`geometry_msgs/Twist`)
- `bebop_follower`: uses the tracker region data to control the drone to follow the tracked object
- `hough`: runs openCV Hough line detector in either original or probabilistic mode
- `flow`: runs LK optical flow on detected features (optional TODO port the functionality to master)

### Nodelets:
- `tracker_nodelet`: receives the init region and image, then uses camera images to update the position which it posts to /tracker/region topic
- `feature_nodelet`: runs cv::goodFeaturesToTrack and posts the points to /bebop/features // TODO stop and start the calculation on demand

### Launch files:
- bebop_master: runs master node and all nodelets in standalone mode
- joy: runs the ROS joy node and the joystick controller
- follow: runs the master node, tracker nodelet and follower node (TODO when follower will be ready it should also launch joypad control nodes)
- fd: runs the master node along with face detector and displays detected faces

### Flying and control:
Launch "roslaunch bebop_vicos joy.launch" to run joy.py and ros joy node. The logitech wireless controller must be set to X and mode must be turned OFF so the predefined controls work properly.

### Boilerplate code:
There are examples of both node and nodelet that include basic functionalities used in bebop image processing/control.
Node: subscription to image, image display, virtual pan/tilt
Nodelet: subscription to image, subscription to shutdown signal
Also added in CMakeLists.txt are boilerplate cmake instructions for compiling them.
For adding nodelets, nodelet_plugins.xml should also be edited.

### Key mapping:
* A -> land
* RB + Y -> takeoff
* D-pad -> moves virtual camera
* L-stick L-R -> roll
* L-stick U-D -> pitch
* R-stick L-R -> yaw
* R-stick U-D -> altitude
* X -> emergency
* B -> flat trim (doesn't work)

##### Util buttons:
* LB -> center camera
* LT -> shutdown tracker
* RT -> N/A
* back button -> N/A
* start button -> N/A
* L-stick button -> N/A
* R-stick button -> N/A

### OpenCV window guide:
* q -> stops tracker
* w,a,s,d -> move virtual camera
* r -> starts recording images and data
* e -> switch tracker initialization mode
* h -> moves camera to default position (0,0)
* t -> toggle trajectory visibility
* f -> toggle visibility of feature points
* g -> toggle face detection display
* c -> snapshot internal memory
* v -> toggle onboard video recording
* esc -> stops related nodes and exits

* mouse LB -> used to draw a rectangle around the tracked object for initialization or click to initialize tracker (depends on tracker init mode)
* mouse RB -> set tracker search region center

### Recording:
When recording is turned on, every image callback saves the unmodified image, every attitude callback saves the drone angles (roll, pitch, yaw), and every region callback saves the region received from the tracker. All data includes timestamps so they can be matched in post processing (if synchronisation is needed in real time, the callbacks should be synchronised within ROS). // TODO include feature point recording as well

### Batteries:
We can use both batteries that came with the bebop, also I've managed to start up the bebop using the ones we used for Parrot 2.0 but not sure if they are suitable for actual flight.

### Other:

7.1.16 Fritz asked why there is a delay when displaying and tracking simultaneously. I know because of some processing/message passing but can't remember exactly what the reason was. Will write it here if I remember.
10.2.16 It's because of the asynchronous nature of ROS. The master node displays the current image but the tracker detection corresponds to an image some frames in the past (the delay introduced by the tracker's processing time). I.e. the image and the detection are not aligned in time (it's a conscious decision as I believe the image feed should be delayed as little as possible).


// iz developer foruma, nekaj o onemogočanju stabilizacije, je sicer za bebop 2 (se mi zdi) ampak verjetno je na tem dost podobno

Exemple pour enlever la stabilisation de la caméra : 
Dans le dossier "/etc/init.d/" ajoutez le startup_script: #!/bin/sh
kill -9 ps | grep dragon | grep '/' | awk '{print $1}'
/usr/bin/dragon-prog -S 0 &

Dans le fichier "/etc/init.d/rcS" ajoutez vers la fin du script, juste après "sleep 1", ajoutez: /etc/init.d/startup_script - Sauvez (press ESC, type ":x").

Pour remettre la stabilisation de la caméra :
Dans "/etc/init.d/rcS" file, commentez la ligne avec vi /etc/init.d/startup_script -- Ajoutez "#" devant --> # /etc/init.d/startup_script


// rad bi naštimal default band na 5GHz, mislim da je to na dronu (telnet 192.168.42.1) v fajlu /etc/default-dragon.conf pod wifi_band sem spremenil iz 0 v 1 (for future reference)
spremenil še v /data/dragon.conf channel v 1
http://forums.whirlpool.net.au/forum-replies.cfm?t=2346273&p=2&#r31

// eh, pritisneš gumb na dronu za 5 sekund, LED zasveti oranžno, in stvar bi naj delala
// !!! ampak ne. resno. Ne morem pogruntat zakaj. Bo najbrž treba inštalirat app in z njim zrihtat. Jesus.
19.2.16 nekako zrihtal tole, kombinacija poskušanja po navodilih (po petih sekundah moramo narediti še hard reset, t.j. 15 sekund držat gumb). Pa par conf datotek sem popravljal na dronu. Pa forget network na linuxu.

// hacking, upam da bo delalo kaj od tega. baje celo lahko dobiš raw sensor data
http://forum.parrot.com/usa/viewtopic.php?id=29397&p=2
https://community.parrot.com/t5/Bebop-Drone/Turning-off-stabilzation-for-better-FPV/m-p/128477#U128477
http://forum.parrot.com/usa/viewtopic.php?id=29397&p=2

// router mod
http://forum.parrot.com/usa/viewtopic.php?pid=54256#p54256

// onboard applications
https://wiki.paparazziuav.org/wiki/Bebop#Onboard_applications

// recording to .bag files
rosbag record -a // records all topics

// recording selected topics
rosbag record /bebop/image_raw /bebop/camera_info

// to convert videos to bag files call:
cd ~/Desktop/catkin_ws;
rosrun bebop_vicos readVideo /home/jon/Desktop/bebop_slam_1.mp4 /home/jon/Desktop/bebop_bag/bebop_slam_1.bag

// developer forum
http://forum.developer.parrot.com/c/drone-sdk/bebop

// SDK docs
http://developer.parrot.com/docs/bebop/

// AutonomyLab github
https://github.com/AutonomyLab/bebop_autonomy

// driver docs
http://bebop-autonomy.readthedocs.io/

// to format README
https://en.wikipedia.org/wiki/Markdown

