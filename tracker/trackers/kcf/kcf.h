#ifndef __LEGIT_KCF_TRACKER
#define __LEGIT_KCF_TRACKER

#include <string.h>

#include <opencv2/core/core.hpp>
#include <opencv2/video/tracking.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "common/utils/config.h"
#include "tracker.h"

#include "hog/gradients.h"


using namespace cv;
using namespace std;
using namespace legit::common;
using namespace legit::tracker;

namespace legit
{
namespace tracker
{
class KCFTracker : public Tracker
{

public:

	KCFTracker(Config& config, string instance = "default");
	~KCFTracker();

	virtual void initialize(Image& image, cv::Rect region);

	virtual void update(Image& image);

	virtual cv::Rect region();

	virtual Point2f position();

	virtual bool is_tracking();

	virtual void visualize(Canvas& canvas);

	virtual void add_observer(Ptr<Observer> observer);

	virtual void remove_observer(Ptr<Observer> observer);

	virtual string get_name();

	virtual void track_kcf(Mat & image);

	virtual void update_kcf(Mat & image);

	virtual void set_region(cv::Rect region);

	virtual cv::Rect get_tracked_reg();

	virtual Mat get_response();

	virtual void calculate_pos(Mat &resp);

	virtual void set_pos(int x, int y);

protected:

	virtual Mat detect(Mat & image);

	virtual Mat gaussian_correlation(vector<Mat> xf, vector<Mat> yf, float sigma);

	virtual Mat get_subwindow(Mat & image, Point2f pos, int w, int h);

	virtual void gaussian_shaped_labels(float sigma, int w, int h);

	virtual Mat circshift(cv::Mat matrix, int dx, int dy);

	virtual int modul(int a, int b);

	virtual vector<Mat> get_features(Mat & patch);

	virtual vector<Mat> get_features_rgb(Mat & patch);

	virtual vector<Mat> get_features_hog(Mat & image, int bin_size, int n_orients, float clip, float crop);

	//virtual vector<Mat> get_features_hog_c(Mat & image);

	virtual double get_max(Mat & m);

	virtual double get_min(Mat & m);

	virtual void visualize_test(Mat m, string s);

	virtual float subpixel_peak(string s, Point2f p);

	virtual Mat divide_mat_complex(Mat nom, Mat den);

	virtual void print_vect_mat(vector<Mat> v);

	Config configuration;

	string instance;

	vector<Ptr<Observer> > observers;

	cv::Rect reg;

	cv::Rect tracked_reg;

	Mat response;

	Mat model_num;

	Mat model_den;

	vector<Mat> model_xf;

	Mat model_alphaf;

	Mat yf;

	Mat cos_window;

	Point2f target_sz;

	Point2f template_sz;

	float template_size;

	Point2f scale;

	Point2f pos;

	float peak_val;

	float output_sigma_factor;

	float output_sigma;

	float kernel_sigma;

	float hog_orientations;

	float cell_size;

	float lambda;

	float interp_factor;

	float padding;

	float pixel_padding;

	float hog_clip;

	float hog_crop;

	float scale_weight = 0.95;

	float scale_step = 1.05;
	
	float min_scale = 0.25;
	
	float max_scale = 3;

	bool adapt_scale;

	bool localization;

	bool tracking;

	bool draw = false;

};
}
}


#ifdef REGISTER_TRACKER
REGISTER_TRACKER(KCFTracker, "kcf");
#endif

#endif