#include "kcf.h"
#include "FHOG.hpp"
#include <opencv2/objdetect/objdetect.hpp>


namespace legit
{
namespace tracker
{

KCFTracker::KCFTracker(Config& config, string inst)
{
	configuration = config;

	instance = inst;

	// read parameters from configuration file
	padding = configuration.read<float>("padding", 1.5);
	lambda = configuration.read<float>("lambda", 0.0001);
	output_sigma_factor = configuration.read<float>("output_sigma_factor", 0.1);
	template_size = configuration.read<float>("template_size", 100);
	pixel_padding = configuration.read<float>("pixel_padding", 0);
	interp_factor = configuration.read<float>("interp_factor", 0.012);
	kernel_sigma = configuration.read<float>("kernel_sigma", 0.6);
	hog_orientations = configuration.read<float>("hog_orientations", 9);
	cell_size = configuration.read<float>("cell_size", 4);
	hog_clip = configuration.read<float>("hog_clip", 0);
	hog_crop = configuration.read<float>("hog_crop", 0.2);
	// scale parameters
	scale_weight = configuration.read<float>("scale_weight", 0.95);
	scale_step = configuration.read<float>("scale_step", 1.05);
	min_scale = configuration.read<float>("min_scale", 0.25);
	max_scale = configuration.read<float>("max_scale", 3.0);
	adapt_scale = configuration.read<bool>("adapt_scale", 1);
	localization = configuration.read<bool>("localization", 1);

	tracking = false;
}

KCFTracker::~KCFTracker()
{
}

void KCFTracker::initialize(Image& image, cv::Rect region)
{
	reg = region;
	Mat img = image.get_rgb();
	// initialize parameters according to the input region
	// note the dimensions: x -> height, y -> width
	target_sz = Point2f(region.height, region.width);
	pos = Point2f(region.y + target_sz.x / 2, region.x + target_sz.y / 2);
	float prod = padding * sqrt(target_sz.x * target_sz.y);
	template_sz = Point2f(floor(target_sz.x + prod), floor(target_sz.y + prod));

	if (template_sz.x > template_sz.y) {
		scale = Point2f(template_sz.x / template_size, template_sz.x / template_size);
	} else {
		scale = Point2f(template_sz.y / template_size, template_sz.y / template_size);
	}

	template_sz.x = floor(template_sz.x / scale.x);
	template_sz.y = floor(template_sz.y / scale.y);

	target_sz.x = target_sz.x / scale.x;
	target_sz.y = target_sz.y / scale.y;
	// calculate sigma parameter
	output_sigma = sqrt((template_sz.x / (1 + padding)) * (template_sz.y / (1 + padding))) *
	               output_sigma_factor / cell_size;
	// create gaussian peak yf
	gaussian_shaped_labels(output_sigma, floor((template_sz.y - pixel_padding) / cell_size),
	                       floor((template_sz.x - pixel_padding) / cell_size));
	// create cosine window cos_win
	createHanningWindow(cos_window, yf.size(), CV_32F);
	// crop image patch with padding from input image
	Mat patch = get_subwindow(img, pos, floor(scale.y * template_sz.y),
	                          floor(scale.x * template_sz.x));
	// resize image patch to the reference size
	resize(patch, patch, Size(template_sz.y, template_sz.x));  // default: bilinear interpolation
	// get features from the image patch, e.g., HoG
	vector<Mat> ftrs = get_features(patch);
	// transform features into the Fourier domain
	vector<Mat> xf;
	Mat feature;
	for (int k = 0; k < ftrs.size(); k++) {
		ftrs.at(k).convertTo(feature, CV_32F);
		dft(feature, feature, DFT_COMPLEX_OUTPUT);
		xf.push_back(feature);
	}
	// calculate gaussian correlation of the features
	Mat kf = gaussian_correlation(xf, xf, kernel_sigma);
	// calculate numinator: yf .* kf
	mulSpectrums(yf, kf, model_num, 0, false);
	// calculate denominator kf .* (kf+lambda)
	mulSpectrums(kf, kf + lambda, model_den, 0, false);
	// set model
	model_xf = xf;
	// calculate model_alpha - separate function for complex division is neccessary
	model_alphaf = divide_mat_complex(model_num, model_den);
	// set dummy variables
	peak_val = -1;
	tracking = true;

	// release structures
	img.release();
	kf.release();
	patch.release();
	feature.release();
	vector<Mat>().swap(ftrs);
}

void KCFTracker::update(Image& image)
{
	// get rgb image
	Mat img = image.get_rgb();
	// track on new image to get response and best location
	track_kcf(img);
	// update tracker
	update_kcf(img);
	// release image
	img.release();
}

void KCFTracker::track_kcf(Mat& image) {
	// track the target with the scale adaptation
	tracked_reg = cv::Rect(pos.x, pos.y, floor(scale.y * template_sz.y), floor(scale.x * template_sz.x));

	// calculate response of correlation filter on current image
	response = detect(image);
	double max_val = get_max(response);
	// check if scale adaptation is on
	if (adapt_scale) {
		// try on smaller scale
		if (scale.x / scale_step > min_scale) {
			Point2f _scale = Point2f(scale);
			scale = Point2f((1.0 / scale_step) * _scale);
			Mat new_res = detect(image);
			if (scale_weight * get_max(new_res) > max_val) {
				response = new_res;
				max_val = get_max(new_res);
			} else {
				scale = _scale;
			}
			new_res.release();
		}
		// try on larger scale
		if (scale.x * scale_step < max_scale) {
			Point2f _scale = Point2f(scale);
			scale = Point2f(scale_step * _scale);
			Mat new_res = detect(image);
			if (scale_weight * get_max(new_res) > max_val) {
				response = new_res;
				max_val = get_max(new_res);
			} else {
				scale = _scale;
			}
			new_res.release();
		}
	}
	// find maximum location of response
	minMaxLoc(response, NULL, &max_val, NULL, NULL);
	peak_val = (float) max_val;
	// check if localization is done by KCF of comes from other tracker
	if (localization) {
		calculate_pos(response);
	}
}

Mat KCFTracker::detect(Mat & image) {
	// crop patch from image
	Mat patch = get_subwindow(image, pos, floor(scale.y * template_sz.y), floor(scale.x * template_sz.x));
	// resize patch to the reference size
	resize(patch, patch, Size(template_sz.y, template_sz.x));
	// get features from image patch, e.g., HoG
	vector<Mat> ftrs = get_features(patch);
	// transform features into the Fourier domain
	vector<Mat> zf;
	Mat feature;
	for (int k = 0; k < ftrs.size(); k++) {
		ftrs.at(k).convertTo(feature, CV_32F);
		dft(feature, feature, DFT_COMPLEX_OUTPUT);
		zf.push_back(feature);
	}
	// calculate gaussian correlation of the features
	Mat kzf = gaussian_correlation(zf, model_xf, kernel_sigma);
	// calculate product model_alphaf .* kzf
	Mat tmp;
	mulSpectrums(model_alphaf, kzf, tmp, 0, false);
	// transform back to spatial domain
	Mat res;
	idft(tmp, res, DFT_SCALE + DFT_REAL_OUTPUT);

	tmp.release();
	kzf.release();
	feature.release();
	patch.release();
	vector<Mat>().swap(zf);
	vector<Mat>().swap(ftrs);

	return res;
}

float KCFTracker::subpixel_peak(string s, Point2f p) {
	int i_p0, i_p_l, i_p_r;	// indexes in response
	float p0, p_l, p_r;		// values in response
	if (s.compare("vertical") == 0) {
		// neighbouring rows
		i_p0 = p.y;
		i_p_l = modul(p.y - 1, response.rows);
		i_p_r = modul(p.y + 1, response.rows);
		p0 = response.at<float>(i_p0, p.x);
		p_l = response.at<float>(i_p_l, p.x);
		p_r = response.at<float>(i_p_r, p.x);
	} else if (s.compare("horizontal") == 0) {
		// neighbouring cols
		i_p0 = p.x;
		i_p_l = modul(p.x - 1, response.cols);
		i_p_r = modul(p.x + 1, response.cols);
		p0 = response.at<float>(p.y, i_p0);
		p_l = response.at<float>(p.y, i_p_l);
		p_r = response.at<float>(p.y, i_p_r);
	} else {
		cout << "Warning: unknown subpixel peak direction!" << endl;
		return 0;
	}
	float delta = 0.5 * (p_r - p_l) / (2 * p0 - p_r - p_l);
	if (!isfinite(delta)) { delta = 0; }

	return delta;
}

void KCFTracker::update_kcf(Mat & image)
{
	// crop patch from image on current target location
	Mat patch = get_subwindow(image, pos, floor(scale.y * template_sz.y),
	                          floor(scale.x * template_sz.x));
	// resize patch to reference size
	resize(patch, patch, Size(template_sz.y, template_sz.x));  // default: bilinear interpolation
	// get features from image patch, e.g., HoG
	vector<Mat> ftrs = get_features(patch);
	// transform features into the Fourier domain
	vector<Mat> xf;
	Mat feature;
	for (int k = 0; k < ftrs.size(); k++) {
		ftrs.at(k).convertTo(feature, CV_32F);
		dft(feature, feature, DFT_COMPLEX_OUTPUT);
		xf.push_back(feature);
	}
	// calculate gaussian correlation of the features
	Mat kf = gaussian_correlation(xf, xf, kernel_sigma);
	// calculate current numerator and denominator
	Mat _model_num;
	Mat _model_den;
	mulSpectrums(yf, kf, _model_num, 0, false);
	mulSpectrums(kf, kf + lambda, _model_den, 0, false);
	// autoregresive update of the numerator and denominator
	model_num = (1 - interp_factor) * model_num + interp_factor * _model_num;
	model_den = (1 - interp_factor) * model_den + interp_factor * _model_den;
	// update also model of the features
	for (int k = 0; k < model_xf.size(); k++) {
		model_xf.at(k) = (1 - interp_factor) * model_xf.at(k) + interp_factor * xf.at(k);
	}
	// calculate new model_alphaf - with the updated numerator and denominator
	// separate function is needed for division: model_alphaf = model_num ./ model_den
	model_alphaf = divide_mat_complex(model_num, model_den);

	_model_num.release();
	_model_den.release();
	feature.release();
	patch.release();
	kf.release();
	vector<Mat>().swap(xf);
	vector<Mat>().swap(ftrs);
}

void KCFTracker::calculate_pos(Mat &resp) {
	// find maximum location of response
	Point max_loc;
	minMaxLoc(resp, NULL, NULL, NULL, &max_loc);
	// take into account also subpixel accuracy
	float row = ((float) max_loc.y) + subpixel_peak("vertical", max_loc);
	float col = ((float) max_loc.x) + subpixel_peak("horizontal", max_loc);
	if (row >= resp.rows / 2) {
		row = row - resp.rows;
	}
	if (col >= resp.cols / 2) {
		col = col - resp.cols;
	}
	// calculate x and y displacements
	// Matlab version: row-1, col-1
	pos = pos + Point2f(scale.x * cell_size * (row), scale.y * cell_size * (col));
	reg.x = pos.y - scale.y * target_sz.y / 2;
	reg.y = pos.x - scale.x * target_sz.x / 2;
	reg.width = scale.y * target_sz.y;
	reg.height = scale.x * target_sz.x;
}

void KCFTracker::set_pos(int x, int y) {
	pos.x = x;
	pos.y = y;
	//draw = true;
}

cv::Rect KCFTracker::region()
{
	return reg;
}

Point2f KCFTracker::position()
{
	return Point2f(reg.x + (reg.width / 2), reg.y + (reg.height / 2));
}

bool KCFTracker::is_tracking()
{
	return true;
}

cv::Rect KCFTracker::get_tracked_reg() {
	return tracked_reg;
}

Mat KCFTracker::get_response() {
	return response;
}

void KCFTracker::set_region(cv::Rect region) {
	// get dimensions of previous region - to calculate scale
	float _w = (float) reg.width;
	float _h = (float) reg.height;
	// set region
	reg = region;
	// calculate and set new scale
	scale.y = scale.y + ((region.width - _w) / _w);
	scale.x = scale.x + ((region.height - _h) / _h);
}

void KCFTracker::visualize(Canvas& canvas)
{

}

void KCFTracker::add_observer(Ptr<Observer> observer)
{
	if (observer)
		observers.push_back(observer);
}

void KCFTracker::remove_observer(Ptr<Observer> observer)
{

}

string KCFTracker::get_name()
{
	return "KCF tracker";
}

void KCFTracker::gaussian_shaped_labels(float sigma, int w, int h)
{
	// create 2D Gaussian peak, convert to Fourier space and stores it into the yf
	Mat _yf = Mat::zeros(h, w, CV_32F);
	float w2 = floor(w / 2);
	float h2 = floor(h / 2);
	// calculate for each pixel separatelly
	for (int i = 0; i < _yf.rows; i++) {
		for (int j = 0; j < _yf.cols; j++) {
			_yf.at<float>(i, j) = exp((-0.5 / pow(sigma, 2)) * (pow((i + 1 - h2), 2) + pow((j + 1 - w2), 2)));
		}
	}
	// wrap-arround with the circulat shifting
	_yf = circshift(_yf, -floor(_yf.cols / 2), -floor(_yf.rows / 2));

	dft(_yf, yf, DFT_COMPLEX_OUTPUT);
	_yf.release();
}

cv::Mat KCFTracker::circshift(Mat matrix, int dx, int dy)
{
	Mat matrix_out = matrix.clone();
	int idx_y = 0;
	int idx_x = 0;
	for (int i = 0; i < matrix.rows; i++) {
		for (int j = 0; j < matrix.cols; j++) {
			idx_y = modul(i + dy + 1, matrix.rows);
			idx_x = modul(j + dx + 1, matrix.cols);
			matrix_out.at<float>(idx_y, idx_x) = matrix.at<float>(i, j);
		}
	}

	return matrix_out;
}

int KCFTracker::modul(int a, int b)
{
	// function calculates the module of two numbers and it takes into account also negative numbers
	return ((a % b) + b) % b;
}

Mat KCFTracker::get_subwindow(Mat & image, Point2f pos, int w, int h)
{
	// crop patch from image on a specific position
	// take into account that border pixels are stretched if bounding box goes out from image
	Mat subwindow(h, w, image.type());
	int x_start = pos.y - floor(w / 2);
	int y_start = pos.x - floor(h / 2);
	int x = x_start;
	int y = y_start;
	// go over each pixel separately and copy each pixel to the output patch
	for (int i = 0; i < h; i++) {
		y = y_start + i;
		if (y < 0) { y = 0; }
		if (y >= image.rows) { y = image.rows - 1; }
		for (int j = 0; j < w; j++) {
			x = x_start + j;
			if (x < 0) { x = 0; }
			if (x >= image.cols) { x = image.cols - 1; }
			if (image.channels() == 1) {
				subwindow.at<uchar>(i, j) = image.at<uchar>(y, x);
			} else {
				subwindow.at<Vec3b>(i, j) = image.at<Vec3b>(y, x);
			}
		}
	}

	return subwindow;
}

vector<Mat> KCFTracker::get_features(Mat & patch) {
	// extract features from patch - call dollar's HoG features extractor
	//return get_features_hog(patch, cell_size, hog_orientations, hog_clip, hog_crop);


	vector<Mat> a = get_features_hog(patch, cell_size, hog_orientations, hog_clip, hog_crop);

	//vector<Mat> a = get_features_hog_c(patch);

	/*for (unsigned int i = 0; i < a.size(); i++) {
		//std::cout << features.at(i)(cv::Range(0, 1), cv::Range(0,9)) << std::endl;
		//std::cout << a.at(i).size() << std::endl;
		std::cout << i << std::endl;
		Mat cur = a.at(i);
		imshow("fhog", cur);
		waitKey(1);
	}*/
	return a;
	//return b;
	//return get_features_rgb(patch);
}

vector<Mat> KCFTracker::get_features_rgb(Mat & patch) {
	vector<Mat> channels;
	split(patch, channels);
	for (int k = 0; k < channels.size(); k++) {
		channels.at(k).convertTo(channels.at(k), CV_32F);
		channels.at(k) = (channels.at(k) / 255.0) - 0.5;
		channels.at(k) = channels.at(k) - mean(channels.at(k))[0];
		resize(channels.at(k), channels.at(k), cos_window.size(), INTER_CUBIC);
		channels.at(k) = channels.at(k).mul(cos_window);
	}

	return channels;
}

/*vector<Mat> KCFTracker::get_features_hog_c(Mat &im) {
	HogFeature FHOG(cell_size, 1);
	//imshow("test", im);
	//waitKey(0);
	return FHOG.getFeatureVector(im);
}*/

vector<Mat> KCFTracker::get_features_hog(Mat &im, int bin_size, int n_orients, float clip, float crop) {
	// convert image patch to floats
	// split channels to vector of matrices
	//cout << "image size: " << im.size() << endl;
	Mat image = im.t();
	vector<Mat> channels;
	split(image, channels);
	for (int k = 0; k < channels.size(); k++) {
		channels.at(k).convertTo(channels.at(k), CV_32F);
		//channels.at(k).convertTo(channels.at(k), CV_32F, 1/255.0);
		channels.at(k) = channels.at(k) / 255.0;
	}
	// put all image channels into one concatenated vector
	float* I = (float*) malloc(channels.size() * image.rows * image.cols * sizeof(float));
	for (int k = 0; k < channels.size(); k++) {
		memcpy(I + k * image.rows * image.cols, channels.at(k).data,
		       image.rows * image.cols * sizeof(float));
	}
	// allocate structures for magnitude and orientations
	float* M = (float*) calloc(image.rows * image.cols, sizeof(float));
	float* O = (float*) calloc(image.rows * image.cols, sizeof(float));
	// get gradients and orientations
	gradMag(I, M, O, image.cols, image.rows, channels.size(), true);
	// calculate number of channels in hog features
	int n_channels = (hog_orientations * 3) + 5;
	// calclate dimensions of one channel of the features
	int w_hog = image.cols / cell_size;
	int h_hog = image.rows / cell_size;
	// allocate memory for hog features
	float* H = (float*) calloc(n_channels * h_hog * w_hog, sizeof(float));
	// get hog features
	//cout << "image size before fhog: " << im.size() << endl;
	fhog(M, O, H, image.cols, image.rows, cell_size, hog_orientations, -1, 0.2);
	// put data from array to vector of matrices
	vector<Mat> features(n_channels - 1);
	for (int k = 0; k < n_channels - 1; k++) {
		// allocate temporary array for each HoG channel
		float* temp = (float*) calloc(w_hog * h_hog, sizeof(float));
		// copy from HoG channel from H to temporary array
		memcpy(temp, H + k * w_hog * h_hog, w_hog * h_hog * sizeof(float));
		// put data from temporary array to Mat structure and assign it to features vector
		features.at(k) = Mat(h_hog, w_hog, CV_32F, temp).t();
		// multiply with cosine window
		multiply(features.at(k), cos_window, features.at(k));
		free(temp);
	}

	vector<Mat>().swap(channels);
	free(H);
	free(I);
	free(M);
	free(O);

	return features;
}

Mat KCFTracker::gaussian_correlation(vector<Mat> xf, vector<Mat> yf, float sigma)
{
	// calculate the squared norm of each feature channel and sum it
	int h = xf.at(0).rows;
	int w = xf.at(0).cols;
	float N = (float) w * h;
	float sq_norm_x = 0;
	float sq_norm_y = 0;
	for (int k = 0; k < xf.size(); k++) {
		sq_norm_x += (xf.at(k)).dot(xf.at(k));
		sq_norm_y += (yf.at(k)).dot(yf.at(k));
	}
	// normalization
	sq_norm_x /= N;
	sq_norm_y /= N;
	// correlation over each channel and convert the result back to spatial domain
	Mat xyf_k;
	Mat xy = Mat::zeros(Size(w, h), CV_32F);
	Mat xy_k;
	for (int k = 0; k < xf.size(); k++) {
		mulSpectrums(xf.at(k), yf.at(k), xyf_k, 0, true);
		idft(xyf_k, xy_k, DFT_SCALE + DFT_REAL_OUTPUT);
		add(xy_k, xy, xy);
	}
	// use gaussian kernel for the correlation for better localization
	Mat _kf = Mat::zeros(xy.size(), CV_32F);
	float _kf_el;
	for (int i = 0; i < _kf.rows; i++) {
		for (int j = 0; j < _kf.cols; j++) {
			_kf_el = (sq_norm_x + sq_norm_y - (2 * xy.at<float>(i, j))) / ((float) h * w * xf.size());
			if (_kf_el < 0) {
				_kf_el = 0.0;
			}
			_kf_el = exp((-1.0 / (sigma * sigma) * _kf_el));
			_kf.at<float>(i, j) = _kf_el;
		}
	}
	// convert the correlation result to the fourier domain
	Mat kf;
	_kf.convertTo(kf, CV_32F);
	dft(kf, kf, DFT_COMPLEX_OUTPUT);

	_kf.release();
	xy.release();
	xyf_k.release();
	xy_k.release();

	return kf;
}

Mat KCFTracker::divide_mat_complex(Mat num, Mat den) {
	// function divides two complex matrices: num / den and returns a complex matrix
	// temporary matrices of numerator and denuminator for real and imaginary part
	vector<Mat> _num;
	vector<Mat> _den;

	split(num, _num);
	split(den, _den);

	Mat m1 = _num.at(0);  // numerator - real part
	Mat m2 = _num.at(1);  // numerator - imaginary part
	Mat m3 = _den.at(0);  // denominator - real part
	Mat m4 = _den.at(1);  // denominator - imaginary part
	// prepare result matrix
	Mat result = Mat::zeros(num.size(), CV_32FC2);
	// divide pixel-by-pixel to divide complex number correctly
	for (int i = 0; i < num.rows; i++) {
		for (int j = 0; j < num.cols; j++) {
			result.at<std::complex<float> >(i, j) =
			    std::complex<float>(m1.at<float>(i, j), m2.at<float>(i, j)) /
			    std::complex<float>(m3.at<float>(i, j), m4.at<float>(i, j));
		}
	}

	m1.release();
	m2.release();
	m3.release();
	m4.release();
	vector<Mat>().swap(_num);
	vector<Mat>().swap(_den);

	return result;
}


// ---------------- Help functions ------------------
double KCFTracker::get_max(Mat &m) {
	double val;
	minMaxLoc(m, NULL, &val, NULL, NULL);
	return val;
}

double KCFTracker::get_min(Mat &m) {
	double val;
	minMaxLoc(m, &val, NULL, NULL, NULL);
	return val;
}

void KCFTracker::visualize_test(Mat matrix, string s) {
	// if matrix has more than one channel, visualize only one
	if (matrix.type() > 6) {
		vector<Mat> channels;
		split(matrix, channels);
		matrix = channels.at(0);
	}
	/*
		for(int i=0; i<matrix.rows; i++) {
			for(int j=0; j<matrix.cols; j++) {
				cout << i << ", " << j << " --> " << matrix.at<float>(i,j) << endl;
			}
		}
	*/
	Mat m = matrix - ((float) get_min(matrix));
	m = m / ((float) get_max(m));
	m = m * 255;
	Mat tmp;
	m.convertTo(tmp, CV_8U);
	namedWindow(s, WINDOW_NORMAL);
	imshow(s, tmp);
}

void KCFTracker::print_vect_mat(vector<Mat> v) {
	// function that prints out every element of the matrices in the vector
	for (int k = 0; k < v.size(); k++) {
		cout << "k=" << k << endl;
		for (int i = 0; i < v.at(k).rows; i++) {
			for (int j = 0; j < v.at(k).cols; j++) {
				cout << i << ", " << j << " --> " << v.at(k).at<float>(i, j) << endl;
			}
		}
	}
}

}
}