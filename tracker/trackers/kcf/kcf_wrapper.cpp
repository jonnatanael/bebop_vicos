// Uncomment line below if you want to use rectangles
#define VOT_RECTANGLE
#include "kcf.h"
#include "vot.h"

int main(int argc, char* argv[])
{

    VOT vot; // Initialize the communcation

    VOTRegion region = vot.region(); // Get region and first frame
    string path = vot.frame();

    Image frame;
    frame.load(path);

    // path to the configuration file; should this be given as a argument??
    string configFile = "/home/alan/Dropbox/linuxWorkspace/workspace/legit-master/config/kcf.cfg";
    // create configuration
    Config config;
    config.load_config(string(configFile));
    // create tracker
    KCFTracker kcf(config);
    // get groundtruth region
    cv::Rect rect;
    region.get(rect);
    // initialize tracker on first frame with the given region
    kcf.initialize(frame, rect);

    //track   
    while (true) {
        path = vot.frame(); // Get the next frame
        if (path.empty()) break; // Are we done?

        frame.load(path);
        // perform tracking step with the tracker
        kcf.update(frame);
        // get region from the tracker
        region.set(kcf.region());

        vot.report(region); // Report the position of the tracker
    }

    // Finishing the communication is completed automatically with the destruction
    // of the communication object (if you are using pointers you have to explicitly
    // delete the object).

    return 0;
}