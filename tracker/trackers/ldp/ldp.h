#ifndef __LEGIT_LDP_TRACKER
#define __LEGIT_LDP_TRACKER

#include <string.h>
#include <opencv2/core/core.hpp>
#include <opencv2/video/tracking.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "common/utils/config.h"
#include "tracker.h"

#include "segmentation/src/segment.h"
#include "../kcf/kcf.h"


using namespace cv;
using namespace std;
using namespace legit::common;
using namespace legit::tracker;

namespace legit
{
namespace tracker
{
class LDPTracker : public Tracker
{

public:

	LDPTracker(Config& config, string instance = "default");
	~LDPTracker();

	virtual void initialize(Image& image, cv::Rect region);

	virtual void update(Image& image);

	virtual cv::Rect region();

	virtual Point2f position();

	virtual bool is_tracking();

	virtual void visualize(Canvas& canvas);

	virtual void add_observer(Ptr<Observer> observer);

	virtual void remove_observer(Ptr<Observer> observer);

	virtual string get_name();

	virtual void set_pos(int x, int y);

protected:

	virtual void segment_region(Mat & img);

	virtual void optimize_springs();

	virtual void track_ldp(Image & image);

	virtual Mat circshift(cv::Mat matrix, int dx, int dy);

	virtual int modul(int a, int b);

	virtual Mat get_subwindow(Mat & image, Point2f pos, int w, int h);

	virtual void extract_histograms(Mat &image, cv::Rect reg, Histogram & hf, Histogram & hb);

	virtual void update_histograms(Mat &image);

	virtual double get_max(Mat & m);

	virtual double get_min(Mat & m);

	virtual void visualize_test(Mat m, string s);

	Config configuration;

	string instance;

	vector<Ptr<Observer> > observers;

	Config config_root;

	KCFTracker root_part;

	//SpringSystem spring_system;

	Mat segmentation_mask;

	Mat root_response;

	cv::Rect reg;

	int nbins;

	Histogram hist_foreground;

	Histogram hist_background;

	float histogram_lr;

	int background_ratio;

	double uniform_component;

	double p_b;

	bool tracking;

};
}
}


#ifdef REGISTER_TRACKER
REGISTER_TRACKER(LDPTracker, "ldp");
#endif

#endif