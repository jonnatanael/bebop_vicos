// Uncomment line below if you want to use rectangles
#define VOT_RECTANGLE
#define VOT_POLYGON
#include "ldp.h"
#include "vot.h"


/* ----------------- Modification: Alan ---------------------- */
// this functions are needed for better estimation of the initialization region

float mean_vect_value(std::vector<float> v)
{
  float sum = std::accumulate(v.begin(), v.end(), 0.0);
  return sum / v.size();
}

int min_vect_position(std::vector<float> v)
{
  int min_i = 0;
  float min_val = v.at(0);
  for(int i=1; i<v.size(); i++) {
    if(v.at(i) < min_val) {
      min_val = v.at(i);
      min_i = i;
    }
  }

  return min_i;
}

int max_vect_position(std::vector<float> v)
{
  int max_i = 0;
  float max_val = v.at(0);
  for(int i=1; i<v.size(); i++) {
    if(v.at(i) > max_val) {
      max_val = v.at(i);
      max_i = i;
    }
  }

  return max_i;
}

float distance(float x1, float y1, float x2, float y2) {
  return std::sqrt(std::pow(x1 - x2, 2) + std::pow(y1 - y2, 2));
}

bool unique_indexes(int i1, int i2, int i3, int i4)
{
  if(i1 == i2 || i1 == i3 || i1 == i4 || i2 == i3 || i2 == i4 || i3 == i4) {
    return false;
  }
  return true;
}

void get_closest2(std::vector<float> X, std::vector<float> Y, int &i1, int &i2, int &i3, int &i4)
{
  i1 = min_vect_position(X);
  i2 = min_vect_position(Y);
  i3 = max_vect_position(X);
  i4 = max_vect_position(Y);
}

int get_closest(std::vector<float> X, std::vector<float> Y, cv::Point2f p)
{
  int best_i = 0;
  float min_dist = distance(X.at(0), Y.at(0), p.x, p.y);
  for(int i=1; i<X.size(); i++) {
    float d = distance(X.at(i), Y.at(i), p.x, p.y);
    if(d < min_dist) {
      min_dist = d;
      best_i = i;
    }
  }

  return best_i;
}

cv::Rect transform_region(std::vector<float> X, std::vector<float> Y)
{
  float x1 = *std::min_element(std::begin(X), std::end(X));
  float x2 = *std::max_element(std::begin(X), std::end(X));
  float y1 = *std::min_element(std::begin(Y), std::end(Y));
  float y2 = *std::max_element(std::begin(Y), std::end(Y));

  int i1 = get_closest(X, Y, cv::Point2f(x1, y2));
  int i2 = get_closest(X, Y, cv::Point2f(x1, y1));
  int i3 = get_closest(X, Y, cv::Point2f(x2, y1));
  int i4 = get_closest(X, Y, cv::Point2f(x2, y2));

  if(!unique_indexes(i1, i2, i3, i4)) {
    get_closest2(X, Y, i1, i2, i3, i4);
  }

  cv::Point2f T1 = cv::Point2f(X.at(i1), Y.at(i1));
  cv::Point2f T2 = cv::Point2f(X.at(i2), Y.at(i2));
  cv::Point2f T3 = cv::Point2f(X.at(i3), Y.at(i3));
  cv::Point2f T4 = cv::Point2f(X.at(i4), Y.at(i4));

  cv::Point2f _h = T1 - T2;
  cv::Point2f _w = T2 - T3;

  float h = std::sqrt(_h.x*_h.x + _h.y*_h.y);
  float w = std::sqrt(_w.x*_w.x + _w.y*_w.y);

  std::vector<float> xx(4);
  xx.at(0) = T1.x;
  xx.at(1) = T2.x;
  xx.at(2) = T3.x;
  xx.at(3) = T4.x;

  std::vector<float> yy(4);
  yy.at(0) = T1.y;
  yy.at(1) = T2.y;
  yy.at(2) = T3.y;
  yy.at(3) = T4.y;

  float x = mean_vect_value(xx);
  float y = mean_vect_value(yy);

  cv::Rect r;
  r.x = x - floor(w/2);
  r.y = y - floor(h/2);
  r.width = w;
  if((w > x2-x1) || (w < 1)) {
    r.width = x2 - x1;
  }
  r.height = h;
  if((h > y2-y1) || (h<1)) {
    h = y2 - y1;
  }

  return r;
}

cv::Rect poly_to_rect(float *x, float *y, int n)
{

  std::vector<float> x_poly(n);
  std::vector<float> y_poly(n);

  for(int i=0; i<n; i++) {
    x_poly.at(i) = x[i];
    y_poly.at(i) = y[i];
  }

  cv::Rect rect = transform_region(x_poly, y_poly);
  return rect;
}

/* ----------------- End Modification ------------------------- */




int main(int argc, char* argv[])
{
  // Initialize the communcation
  VOT vot; 
  // Get region and first frame
  VOTRegion region = vot.region();
  // get the path to the first frame
  string path = vot.frame();
  // load first frame
  Image frame;
  frame.load(path);
  // path to the configuration file; TODO: set this to the LDP config file!
  string configFile = "/home/alan/workspace/tracking/legit-master/config/ldp.cfg";
  // create configuration
  Config config;
  config.load_config(string(configFile));
  // create tracker
  LDPTracker ldp(config);
  // get groundtruth region
  cv::Rect rect;
  if(region.count() == 4) {
    // region is given ascoordinates of 4 corner points
    float x[4] = {region.get_x(0), region.get_x(1), region.get_x(2), region.get_x(3)};
    float y[4] = {region.get_y(0), region.get_y(1), region.get_y(2), region.get_y(3)};
    // transform the region to the axis-aligned rectangle
    rect = poly_to_rect(x, y, region.count());
  } else {
    // region is rectangle
    region.get(rect);
  }
  // initialize tracker on first frame with the given region
  ldp.initialize(frame, rect);
  // tracking loop
  while (true) {
    // Get the next frame
    path = vot.frame();
    // Are we done?
    if (path.empty()) break;
    // get next frame
    frame.load(path);
    // perform tracking step with the tracker
    ldp.update(frame);
    // get region from the tracker
    region.set(ldp.region());
    // Report the position of the tracker
    vot.report(region);
  }

  // Finishing the communication is completed automatically with the destruction
  // of the communication object (if you are using pointers you have to explicitly
  // delete the object).

  return 0;
}