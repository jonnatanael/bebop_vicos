#include "ldp.h"


namespace legit
{
namespace tracker
{

LDPTracker::LDPTracker(Config& config, string inst) :
	config_root(config.read<string>("root_config")),
	root_part(config_root)
{
	configuration = config;
	instance = inst;
	// read parameters from configuration file
	nbins = configuration.read<int>("nbins", 64);
	background_ratio = configuration.read<int>("background_ratio", 3);
	histogram_lr = configuration.read<float>("histogram_lr", 0.05);
	uniform_component = configuration.read<double>("uniform_component", 0.1);
	
	tracking = false;
}

LDPTracker::~LDPTracker()
{
}

void LDPTracker::initialize(Image& image, cv::Rect region)
{
	reg = region;
	Mat img = image.get_rgb();
	// initialize KCF for root part
	root_part.initialize(image, region);
	if(nbins > 0) {
		// create histograms
		hist_foreground = Histogram(img.channels(), nbins);
		hist_background = Histogram(img.channels(), nbins);
		// extract histograms from image
		extract_histograms(img, region, hist_foreground, hist_background);
	}
	img.release();

	tracking = true;
}

void LDPTracker::update(Image& image)
{
	Mat img = image.get_rgb();

	track_ldp(image);

	if(nbins > 0) {
		update_histograms(img);
	}
	
	img.release();
}

void LDPTracker::track_ldp(Image& image) {
	// call tracking steps for root part
	// get rgb image
	Mat img = image.get_rgb();
	root_part.track_kcf(img);
	// get response map of the root part
	root_response = root_part.get_response();
	// get maximum value of root response - before it is multyplied with segmentation mask
	float response_max_before = (float) get_max(root_response);
	// set response matrix as tracker property - before multiplying with segmentation mask
	set_mat_property(1, circshift(root_response, -floor(root_response.cols/2)+1,
		-floor(root_response.rows/2)+1));
	if(nbins > 0) {
		// get segmentation mask
		segment_region(img);
		// copy segmentation mask to maintain original mask dimensions
		Mat mask;
		segmentation_mask.copyTo(mask);
		// resize so that root repsense map and segmentation mask will be the same size
		resize(segmentation_mask, segmentation_mask, root_response.size());
		// circulate shift of the segmentation mask
		segmentation_mask = circshift(segmentation_mask, -floor(segmentation_mask.cols/2)+1, 
			-floor(segmentation_mask.rows/2)+1);
		// combine response of the root part and segmentation mask
		multiply(segmentation_mask, root_response, root_response);
		// set only segmentation mask as tracker property
		set_mat_property(4, mask);
	}
	// set response matrix as tracker property - after multiplying with segmentation mask
	set_mat_property(2, circshift(root_response, -floor(root_response.cols/2)+1,
		-floor(root_response.rows/2)+1));
	// calulate position in combined response map
	root_part.calculate_pos(root_response);
	// update KCF on the new position
	root_part.update_kcf(img);
	// get region from KCF and set it in LDP
	reg = root_part.region();

	// get maximum value of root response combined with segmentation mask
	float response_max_after = (float) get_max(root_response);
	// set both maximum responses - as tracker property
	set_property(1, response_max_before);
	set_property(2, response_max_after);
	// set search region coordinates so that they are publicly available
	// note that search rectangle must be converted to matrix due to library compatibility
	cv::Rect tracked_reg = root_part.get_tracked_reg();
	Mat search_rectangle = Mat::zeros(1, 4, CV_32F);
	search_rectangle.at<float>(0,0) = tracked_reg.x;
	search_rectangle.at<float>(0,1) = tracked_reg.y;
	search_rectangle.at<float>(0,2) = tracked_reg.width;
	search_rectangle.at<float>(0,3) = tracked_reg.height;
	set_mat_property(3, search_rectangle);
	
	img.release();
}

void LDPTracker::segment_region(Mat &img) {
	// get subwindow from image to calculate backprojection within it
	cv::Rect tracked_reg = root_part.get_tracked_reg();
	Point2f pos = Point2f(tracked_reg.x, tracked_reg.y);
	Mat patch = get_subwindow(img, pos, tracked_reg.width, tracked_reg.height);
	// split multi-channel image to vector of matrices
	vector<Mat> img_channels;
	split(patch, img_channels);
	for(int k=0; k<img_channels.size(); k++) {
    	img_channels.at(k).convertTo(img_channels.at(k), CV_8UC1);
    }
	// calculate segmentation
	pair<Mat, Mat> probs = Segment::computePosteriors2(img_channels, 0, 0, patch.cols, patch.rows, 
		p_b, Mat(), Mat(), Mat(), hist_foreground, hist_background);
	// calculate segmentation mask - use also uniform componenet
	Mat mask;
	probs.first.copyTo(mask);
	mask = mask / (get_max(mask));
	mask = mask + uniform_component;
	mask = mask / (get_max(mask));
	mask.convertTo(mask, CV_32FC1);

	segmentation_mask = mask;

	vector<Mat>().swap(img_channels);
	pair<Mat, Mat>().swap(probs);
}

void LDPTracker::optimize_springs() {
	
}

cv::Rect LDPTracker::region()
{
	return reg;
}

Point2f LDPTracker::position()
{
	return Point2f(reg.x + (reg.width / 2), reg.y + (reg.height / 2));
}

bool LDPTracker::is_tracking()
{
	return tracking;
}

void LDPTracker::visualize(Canvas& canvas)
{

}

void LDPTracker::add_observer(Ptr<Observer> observer)
{
	if (observer)
		observers.push_back(observer);
}

void LDPTracker::remove_observer(Ptr<Observer> observer)
{

}

string LDPTracker::get_name()
{
	return "LDP tracker";
}

void LDPTracker::set_pos(int x, int y){
	root_part.set_pos(x,y);
}

cv::Mat LDPTracker::circshift(Mat matrix, int dx, int dy)
{
	// circulant shifting of the image for the certain displacement in each direction
	Mat matrix_out = matrix.clone();
	int idx_y = 0;
	int idx_x = 0;
	for(int i=0; i<matrix.rows; i++) {
		for(int j=0; j<matrix.cols; j++) {
			// calculate destination index
			idx_y = modul(i+dy+1, matrix.rows);
			idx_x = modul(j+dx+1, matrix.cols);
			// copy pixel to the desired destination
			matrix_out.at<float>(idx_y, idx_x) = matrix.at<float>(i,j);
		}
	}

	return matrix_out;
}

int LDPTracker::modul(int a, int b)
{
	// function calculates the module of two numbers and it takes into account also negative numbers
	return ((a % b) + b) % b;
}

Mat LDPTracker::get_subwindow(Mat & image, Point2f pos, int w, int h)
{
	// crop patch from image on a specific position
	// take into account that border pixels are stretched if bounding box goes out from image
	Mat subwindow(h, w, image.type());
	int x_start = pos.y - floor(w/2);
	int y_start = pos.x - floor(h/2);
	int x = x_start;
	int y = y_start;
	// go over each pixel separatelly and copy each pixel to the output patch
	for(int i=0; i<h; i++) {
		y = y_start + i;
		// check if outside the image - if yes copy border pixel
		if(y < 0) { y = 0; }
		if(y >= image.rows) { y = image.rows - 1; }
		for(int j=0; j<w; j++) {
			x = x_start + j;
			// check if outside the image - if yes copy border pixel
			if(x < 0) { x = 0; }
			if(x >= image.cols) { x = image.cols - 1; }
			// check is the image is single- or multy-channel
			if(image.channels() == 1) {
				subwindow.at<uchar>(i,j) = image.at<uchar>(y,x);
			} else {
				subwindow.at<Vec3b>(i,j) = image.at<Vec3b>(y,x);
			}
		}
	}

	return subwindow;
}

void LDPTracker::extract_histograms(Mat &image, cv::Rect region, Histogram &hf, Histogram &hb) {
	// get coordinates of the region
	int x1 = std::min(std::max(0, region.x), image.cols-1);
	int y1 = std::min(std::max(0, region.y), image.rows-1);
	int x2 = std::min(std::max(0, region.x + region.width), image.cols-1);
	int y2 = std::min(std::max(0, region.y + region.height), image.rows-1);
	// calculate coordinates of the background region
	int offsetX = (x2-x1+1) / background_ratio;
    int offsetY = (y2-y1+1) / background_ratio;
    int outer_y1 = std::max(0, (int)(y1-offsetY));
    int outer_y2 = std::min(image.rows, (int)(y2+offsetY+1));
    int outer_x1 = std::max(0, (int)(x1-offsetX));
    int outer_x2 = std::min(image.cols, (int)(x2+offsetX+1));
    // calculate probability for the background
    p_b = 1.0 - ( (x2-x1+1) * (y2-y1+1) ) / 
		( (double) (outer_x2-outer_x1+1) * (outer_y2-outer_y1+1) );
    // split multi-channel image into the vector of matrices
    vector<Mat> img_channels(image.channels());
    split(image, img_channels);
    for(int k=0; k<img_channels.size(); k++) {
    	img_channels.at(k).convertTo(img_channels.at(k), CV_8UC1);
    }
	// extract histograms from image
	hf.extractForegroundHistogram(img_channels, Mat(), false, x1, y1, x2, y2);
	hb.extractBackGroundHistogram(img_channels, x1, y1, x2, y2, 
		outer_x1, outer_y1, outer_x2, outer_y2);

	vector<Mat>().swap(img_channels);
}

void LDPTracker::update_histograms(Mat &image) {
	// create temporary histograms
	Histogram _hf(image.channels(), nbins);
	Histogram _hb(image.channels(), nbins);
	// extract temporary histograms from current image and tracked region
	extract_histograms(image, reg, _hf, _hb);
	// get histogram vectors from temporary histograms
	vector<double> hf_vect_new = _hf.getHistogramVector();
	vector<double> hb_vect_new = _hb.getHistogramVector();
	// get histogram vectors from learned histograms
	vector<double> hf_vect = hist_foreground.getHistogramVector();
	vector<double> hb_vect = hist_background.getHistogramVector();
	// update histograms - use learning rate
	for(int i=0; i<hf_vect.size(); i++) {
		hf_vect_new.at(i) = (1-histogram_lr)*hf_vect.at(i) + histogram_lr*hf_vect_new.at(i);
		hb_vect_new.at(i) = (1-histogram_lr)*hb_vect.at(i) + histogram_lr*hb_vect_new.at(i);
	}
	// set updated histograms to the learned histograms
	hist_foreground.setHistogramVector(&hf_vect_new[0]);
	hist_background.setHistogramVector(&hb_vect_new[0]);

	vector<double>().swap(hf_vect);
	vector<double>().swap(hb_vect);
}

// ---------------- Help functions ------------------
double LDPTracker::get_max(Mat &m) {
	double val;
	minMaxLoc(m, NULL, &val, NULL, NULL);
	return val;
}

double LDPTracker::get_min(Mat &m) {
	double val;
	minMaxLoc(m, &val, NULL, NULL, NULL);
	return val;
}

void LDPTracker::visualize_test(Mat matrix, string s) {
	// if matrix has more than one channel, visualize only one
	if(matrix.type() > 6) {
		vector<Mat> channels;
		split(matrix, channels);
		matrix = channels.at(0);
	}
/*
	for(int i=0; i<matrix.rows; i++) {
		for(int j=0; j<matrix.cols; j++) {
			cout << i << ", " << j << " --> " << matrix.at<float>(i,j) << endl;
		}
	}
*/
	Mat m = matrix - ((float) get_min(matrix));
	m = m / ((float) get_max(m));
	m = m * 255;
	Mat tmp;
	m.convertTo(tmp, CV_8U);
	namedWindow(s, WINDOW_NORMAL);
	imshow(s, tmp);
}

}
}