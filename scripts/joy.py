#!/usr/bin/env python

# The Joystick Controller Node for the tutorial "Up and flying with the AR.Drone and ROS | Joystick Control"
# https://github.com/mikehamer/ardrone_tutorials

# This controller implements the base DroneVideoDisplay class, the DroneController class and subscribes to joystick messages

# Import the ROS libraries, and load the manifest file which through <depend package=... /> will give us access to the project dependencies
import roslib;
import rospy

# Load the DroneController class, which handles interactions with the drone, and the DroneVideoDisplay class, which handles video display
from controller import BasicDroneController

# Import the joystick message
from sensor_msgs.msg import Joy
from bebop_msgs.msg import Ardrone3CameraStateOrientation


# define the default mapping between joystick buttons and their corresponding actions
ButtonEmergency = 0
ButtonLand      = 1
ButtonTakeoff   = 2
ButtonTakeoffEnable = 3
ButtonFlatTrim  = 4
ButtonUtil1 = 5
ButtonUtil4 = 6
ButtonUtil5 = 7
ButtonUtil6 = 8
ButtonUtil7 = 9

# define the default mapping between joystick axes and their corresponding directions
AxisRoll        = 0
AxisPitch       = 1
AxisYaw         = 3
AxisZ           = 4
AxisCamX        = 5
AxisCamY        = 6
ButtonUtil2 	= 2
ButtonUtil3 	= 7

# define the default scaling to apply to the axis inputs. useful where an axis is inverted
ScaleRoll       = 1.0
ScalePitch      = 1.0
ScaleYaw        = 1.0
ScaleZ          = 1.0

# define virtual camera position
CameraPan = 0
CameraTilt = 0
CameraShiftAngle = 1
CameraPanLimit = 35
CameraTiltLimitUp = 35
CameraTiltLimitDown = 70
CameraMovePending = False


# handles the reception of joystick packets
def ReceiveJoystickMessage(data):
	global CameraPan
	global CameraTilt
	global CameraMovePending
	if data.buttons[ButtonEmergency]==1:
		rospy.loginfo("Emergency Button Pressed")
		controller.SendEmergency()
	elif data.buttons[ButtonLand]==1:
		rospy.loginfo("Land Button Pressed")
		controller.SendLand()
	elif data.buttons[ButtonTakeoff]==1 and data.buttons[ButtonTakeoffEnable]==1:
		rospy.loginfo("Takeoff Button Pressed")
		controller.SendTakeoff()
	elif data.buttons[ButtonFlatTrim]==1:
		rospy.loginfo("Flat Trim Button Pressed")
		controller.SendFlatTrim()
	elif data.buttons[ButtonUtil1]==1:
		rospy.loginfo("Util Button 1 Pressed")
		controller.SendUtil(1)
	elif data.buttons[ButtonUtil4]==1:
		rospy.loginfo("Util Button 4 Pressed")
		controller.SendUtil(4)
	elif data.buttons[ButtonUtil5]==1:
		rospy.loginfo("Util Button 5 Pressed")
		controller.SendUtil(5)
	elif data.buttons[ButtonUtil6]==1:
		rospy.loginfo("Util Button 6 Pressed")
		controller.SendUtil(6)
	elif data.buttons[ButtonUtil7]==1:
		rospy.loginfo("Util Button 7 Pressed")
		controller.SendUtil(7)
	else:
		#print data.axes
		if data.axes[AxisRoll] != 0:
			if data.axes[AxisRoll]>0:
				rospy.loginfo("Rolling left")
			else:
				rospy.loginfo("Rolling right")
		elif data.axes[AxisPitch] != 0:
			if data.axes[AxisPitch]>0:
				rospy.loginfo("Moving forward")
			else:
				rospy.loginfo("Moving backward")
		elif data.axes[AxisYaw] != 0:
			if data.axes[AxisYaw]>0:
				rospy.loginfo("Turning left")
			else:
				rospy.loginfo("Turning right")	
		elif data.axes[AxisZ] != 0:
			if data.axes[AxisZ]>0:
				rospy.loginfo("Moving up")
			else:
				rospy.loginfo("Moving down")
		elif data.axes[AxisCamX] != 0:
			if data.axes[AxisCamX]>0:
				if (CameraPan-CameraShiftAngle >= -CameraPanLimit):
					CameraPan-=CameraShiftAngle
				else:
					CameraPan = -CameraPanLimit
				rospy.loginfo("Moving camera left")
			else:				
				if (CameraPan+CameraShiftAngle <= CameraPanLimit):
					CameraPan+=CameraShiftAngle
				else:
					CameraPan = CameraPanLimit
				rospy.loginfo("Moving camera right")
			CameraMovePending = True
		elif data.axes[AxisCamY] != 0:
			if data.axes[AxisCamY]>0:
				if (CameraTilt+CameraShiftAngle <= CameraTiltLimitUp):
					CameraTilt+=CameraShiftAngle
				else:
					CameraTilt = CameraTiltLimitUp
				rospy.loginfo("Moving camera up")
			else:
				if (CameraTilt-CameraShiftAngle >= -CameraTiltLimitDown):
					CameraTilt-=CameraShiftAngle
				else:
					CameraTilt = -CameraTiltLimitDown
				rospy.loginfo("Moving camera down")
			CameraMovePending = True
		elif data.axes[ButtonUtil2] == -1:
			rospy.loginfo("Util Button 2 Pressed")
			controller.SendUtil(2)
		elif data.axes[ButtonUtil3] == -1:
			controller.SendUtil(3)
			rospy.loginfo("Util Button 3 Pressed")
		if CameraMovePending:
			controller.SendCameraPosition(CameraPan,CameraTilt)
			CameraMovePending = False
		controller.SetCommand(data.axes[AxisRoll]/ScaleRoll,data.axes[AxisPitch]/ScalePitch,data.axes[AxisYaw]/ScaleYaw,data.axes[AxisZ]/ScaleZ)


def ReceiveCameraOrientation(data):
	global CameraPan
	global CameraTilt
	global CameraMovePending
	
	if CameraMovePending:
		if CameraPan == data.pan and CameraTilt == data.tilt:
			CameraMovePending = False
	else:
		CameraPan = data.pan
		CameraTilt = data.tilt

# Setup the application
if __name__=='__main__':
	import sys
	# Firstly we setup a ros node, so that we can communicate with the other packages
	rospy.init_node('bebop_joystick_controller')

	# Next load in the parameters from the launch-file
	ButtonEmergency = int (   rospy.get_param("~ButtonEmergency",ButtonEmergency) )
	ButtonLand      = int (   rospy.get_param("~ButtonLand",ButtonLand) )
	ButtonTakeoff   = int (   rospy.get_param("~ButtonTakeoff",ButtonTakeoff) )
	ButtonTakeoffEnable   = int (   rospy.get_param("~ButtonTakeoffEnable",ButtonTakeoffEnable) )
	ButtonFlatTrim   = int (   rospy.get_param("~ButtonFlatTrim",ButtonFlatTrim) )
	# utility buttons
	ButtonUtil1   = int (   rospy.get_param("~ButtonUtil1",ButtonUtil1) )
	ButtonUtil2   = int (   rospy.get_param("~ButtonUtil2",ButtonUtil2) )
	ButtonUtil3   = int (   rospy.get_param("~ButtonUtil3",ButtonUtil3) )
	ButtonUtil4   = int (   rospy.get_param("~ButtonUtil4",ButtonUtil4) )
	ButtonUtil5   = int (   rospy.get_param("~ButtonUtil5",ButtonUtil5) )
	ButtonUtil6   = int (   rospy.get_param("~ButtonUtil6",ButtonUtil6) )
	ButtonUtil7   = int (   rospy.get_param("~ButtonUtil7",ButtonUtil7) )


	AxisRoll      = int (   rospy.get_param("~AxisRoll",AxisRoll) )
	AxisPitch     = int (   rospy.get_param("~AxisPitch",AxisPitch) )
	AxisYaw       = int (   rospy.get_param("~AxisYaw",AxisYaw) )
	AxisZ         = int (   rospy.get_param("~AxisZ",AxisZ) )
	AxisCamX      = int (   rospy.get_param("~AxisCameraX",AxisCamX) )
	AxisCamY      = int (   rospy.get_param("~AxisCameraY",AxisCamY) )
	ScaleRoll     = float ( rospy.get_param("~ScaleRoll",ScaleRoll) )
	ScalePitch    = float ( rospy.get_param("~ScalePitch",ScalePitch) )
	ScaleYaw      = float ( rospy.get_param("~ScaleYaw",ScaleYaw) )
	ScaleZ        = float ( rospy.get_param("~ScaleZ",ScaleZ) )

	# angle for which the virtual camera moves
	CameraShiftAngle= float ( rospy.get_param("~CameraShiftAngle",CameraShiftAngle) )

	controller = BasicDroneController()

	subJoystick = rospy.Subscriber('/joy', Joy, ReceiveJoystickMessage)
	subCamera = rospy.Subscriber('/bebop/states/ardrone3/CameraState/Orientation', Ardrone3CameraStateOrientation, ReceiveCameraOrientation)
	#sub_cam_pos = n.subscribe("/bebop/states/ARDrone3/CameraState/Orientation", 1, ReceiveCameraOrientation);

	# reset camera position at launch
	controller.SendCameraPosition(0,0)
	
	rospy.spin()