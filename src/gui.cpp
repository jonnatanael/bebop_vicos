#include <iostream>
#include <string>
#include "gui.h"
#include "util.h"
#include <opencv2/core/core.hpp>

void gui::writeText(cv_bridge::CvImagePtr cv_ptr, cv::Point position, std::string text, cv::Scalar color) {
	double scale = 0.5;
	int thickness = 1;
	int baseline = 0;
	int padding = 10;
	int lineSpacing = 10;

	// split text by newlines
	std::vector<std::string> x = util::split(text, '\n');

	int lines = x.size();

	cv::Size textSize;
	textSize = cv::getTextSize(text, font, scale, thickness, &baseline);
	/*cv::rectangle(cv_ptr->image, cv::Point(position.x - padding, position.y*lines - textSize.height - padding),
				cv::Point(position.x + textSize.width + padding, position.y + padding), bgColor, -1);*/

	int cnt = 0;
	for (std::vector<std::string>::const_iterator i = x.begin(); i != x.end(); ++i) {
		cv::putText(cv_ptr->image, *i, cv::Point(position.x, position.y + (textSize.height + lineSpacing)*cnt), font, scale, color, thickness, linetype);
		cnt++;
	}
	//TODO add a warning if the text would show outside the image

}

void gui::writeText(cv::Mat img, cv::Point position, std::string text, cv::Scalar color) {
	double scale = 1.2;
	int thickness = 1;
	int baseline = 0;
	int padding = 10;
	int lineSpacing = 10;

	// split text by newlines
	std::vector<std::string> x = util::split(text, '\n');

	int lines = x.size();

	cv::Size textSize;
	textSize = cv::getTextSize(text, font, scale, thickness, &baseline);
	/*cv::rectangle(cv_ptr->image, cv::Point(position.x - padding, position.y*lines - textSize.height - padding),
				cv::Point(position.x + textSize.width + padding, position.y + padding), bgColor, -1);*/

	int cnt = 0;
	for (std::vector<std::string>::const_iterator i = x.begin(); i != x.end(); ++i) {
		cv::putText(img, *i, cv::Point(position.x, position.y + (textSize.height + lineSpacing)*cnt), cv::FONT_HERSHEY_PLAIN, scale, color, thickness, linetype);
		cnt++;
	}
	//TODO add a warning if the text would show outside the image

}

void gui::writeStatusText(cv_bridge::CvImagePtr cv_ptr, bool tracking, bool lost) {
	// writes system status onto window
	cv::Scalar textColor = cv::Scalar(255, 255, 255);
	cv::Scalar bgColor = cv::Scalar(0, 0, 0);
	cv::Point origin = cv::Point(15, 330); // origin is actually in left bottom corner, despite documentation

	double scale = 0.5;
	int thickness = 1;
	int baseline = 0;
	int padding = 10;

	cv::Size text;
	std::string label;


	if (!tracking) {
		label = "WAITING FOR INPUT";
		text = cv::getTextSize(label, font, scale, thickness, &baseline);
		cv::rectangle(cv_ptr->image, cv::Point(origin.x - padding, origin.y - text.height - padding),
		              cv::Point(origin.x + text.width + padding, origin.y + padding), bgColor, -1);
		cv::putText(cv_ptr->image, label, origin, font, scale, textColor, thickness, linetype);

	}
	else {
		if (lost) {
			label = "REDETECTING OBJECT";
			text = cv::getTextSize(label, font, scale, thickness, &baseline);
			cv::rectangle(cv_ptr->image, cv::Point(origin.x - padding, origin.y - text.height - padding),
			              cv::Point(origin.x + text.width + padding, origin.y + padding), bgColor, -1);
			cv::putText(cv_ptr->image, label, origin, font, scale, textColor, thickness, linetype);
		}
		else {
			label = "TRACKING OBJECT";
			text = cv::getTextSize(label, font, scale, thickness, &baseline);
			cv::rectangle(cv_ptr->image, cv::Point(origin.x - padding, origin.y - text.height - padding),
			              cv:: Point(origin.x + text.width + padding, origin.y + padding), bgColor, -1);
			cv::putText(cv_ptr->image, label, origin, font, scale, textColor, thickness, linetype);
		}
	}
}

void gui::drawRec(cv_bridge::CvImagePtr cv_ptr) {
	// draw recording indicator
	cv::Scalar textColor = cv::Scalar(255, 255, 255);
	cv::Scalar clr = cv::Scalar(0, 0, 255);
	cv::Point origin = cv::Point(550, 50);
	cv::Size text;
	double scale = 0.65;
	int thickness = 1;
	int baseline = 0;
	std::string label = "REC";
	text = cv::getTextSize(label, font, scale, thickness, &baseline);
	cv::putText(cv_ptr->image, label, origin, font, scale, textColor, thickness, linetype);
	cv::circle(cv_ptr->image, cv::Point(origin.x + text.width + 2 * baseline, origin.y - (text.height / 2)), text.height / 2 , clr, -1) ;
}

void gui::drawBattery(cv_bridge::CvImagePtr cv_ptr, int battery) {
	// draw battery icon
	cv::Scalar clr = cv::Scalar(0, 0, 0);
	cv::Point origin = cv::Point(550, 320); // origin is actually in left bottom corner, despite documentation
	double scale = 0.5;
	int thickness = 2;
	int h = 20;
	int w = 40;
	double b = battery / (double)100;

	cv::rectangle(cv_ptr->image, cv::Point(origin.x, origin.y), cv::Point(origin.x + w , origin.y + h), clr, 2);
	cv::rectangle(cv_ptr->image, cv::Point(origin.x + w, origin.y + (h / 4)),
	              cv::Point(origin.x + w + 3 , origin.y + h - (h / 4)), clr, -1);

	if (battery < 20)
		clr = cv::Scalar(0, 0, 255);
	else
		clr = cv::Scalar(0, 255, 0);
	cv::rectangle(cv_ptr->image, cv::Point(origin.x + thickness, origin.y + thickness),
	              cv::Point(origin.x + (w * b) - thickness, origin.y + h - thickness), clr, -1);
}

void gui::drawAlphaBorder(cv_bridge::CvImagePtr cv_ptr, cv::Rect r, double opacity, cv::Scalar clr, int thickness, bool alt) {
	// for drawing transparent rectangles
	cv::Mat overlay;
	cv_ptr->image.copyTo(overlay);
	double perc = 0.25;
	double sz = std::min(r.width, r.height) * perc;
	if (alt) {
		cv::line(cv_ptr->image, cv::Point(r.x, r.y), cv::Point(r.x + sz, r.y), clr, thickness);
		cv::line(cv_ptr->image, cv::Point(r.x, r.y), cv::Point(r.x, r.y + sz), clr, thickness);

		cv::line(cv_ptr->image, cv::Point(r.x + r.width, r.y), cv::Point(r.x + r.width - sz, r.y), clr, thickness);
		cv::line(cv_ptr->image, cv::Point(r.x + r.width, r.y), cv::Point(r.x + r.width, r.y + sz), clr, thickness);

		cv::line(cv_ptr->image, cv::Point(r.x, r.y + r.height), cv::Point(r.x, r.y + r.height - sz), clr, thickness);
		cv::line(cv_ptr->image, cv::Point(r.x, r.y + r.height), cv::Point(r.x + sz, r.y + r.height), clr, thickness);

		cv::line(cv_ptr->image, cv::Point(r.x + r.width, r.y + r.height), cv::Point(r.x + r.width, r.y + r.height - sz), clr, thickness);
		cv::line(cv_ptr->image, cv::Point(r.x + r.width, r.y + r.height), cv::Point(r.x + r.width - sz, r.y + r.height), clr, thickness);
	}
	else
		cv::rectangle(cv_ptr->image, r, clr, thickness);
	//cv::rectangle(overlay, r, clr, -1);
	cv::addWeighted(cv_ptr->image, opacity, overlay, 1 - opacity, 0, cv_ptr->image);
}

void gui::drawAlphaRectangle(cv_bridge::CvImagePtr cv_ptr, cv::Rect r, double opacity, cv::Scalar clr, int thickness) {
	// for drawing transparent rectangles
	cv::Mat overlay;
	cv_ptr->image.copyTo(overlay);
	cv::rectangle(cv_ptr->image, r, clr, thickness);
	cv::rectangle(overlay, r, clr, -1);
	cv::addWeighted(cv_ptr->image, 1 - opacity, overlay, opacity, 0, cv_ptr->image);
}

void gui::drawAlphaLine(cv_bridge::CvImagePtr cv_ptr, cv::Point a, cv::Point b, double opacity, cv::Scalar clr, int thickness) {
	// for drawing transparent lines
	cv::Mat overlay;
	cv_ptr->image.copyTo(overlay);
	cv::line(cv_ptr->image, a, b, clr, thickness);
	cv::addWeighted(cv_ptr->image, 1 - opacity, overlay, opacity, 0, cv_ptr->image);
}

void gui::handle_queue(cv_bridge::CvImagePtr cv_ptr, std::vector<cv::Point>* v, cv::Point p, cv::Scalar clr, int v_lim) {
	// adds point to vector, removes if length if too great in FIFO manner
	v->push_back(p);
	if (v->size() > v_lim) {
		v->erase(v->begin());
	}
	for (std::vector<cv::Point>::iterator it = v->begin(); it != v->end(); ++it) {
		cv::Point cur = *it;
		if (it != v->begin()) { // for displaying the trajectory of past detections
			cv::Point cur2 = *(--it);
			++it;
			cv::line(cv_ptr->image, cur, cur2, clr, 2, CV_AA);
		}
	}
}

