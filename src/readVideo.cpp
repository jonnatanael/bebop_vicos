#include <stdio.h>
#include <iostream>
#include <rosbag/bag.h>
#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <std_msgs/String.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>
#include <cv_bridge/cv_bridge.h>

using namespace cv;
using namespace std;

/** @function main */
int main( int argc, char** argv ){

	/*
		String infile = "/home/jon/Desktop/bebop_slam_3.mp4";
		String outfile = "/home/jon/Desktop/test.bag";
	*/
	String infile = "";
	String outfile = "";

	if (argc > 1){
		infile = argv[1];
		outfile = argv[2];
	}

	ros::Time::init();

	rosbag::Bag bag;
    bag.open(outfile, rosbag::bagmode::Write);

    double fps = 29;
    double delay = 1/fps;


	//VideoCapture cap("/home/jon/Dropbox/ViCoS/videi/DSC_0153.MOV");
	VideoCapture cap(infile);
	
	cap.set(CV_CAP_PROP_FOURCC, CV_FOURCC('A', 'V', 'C', '1'));
	cap.set(CV_CAP_PROP_FPS, fps);

	if ( !cap.isOpened() )  // if not success, exit program
	{
		cout << "Cannot open the video file" << endl;
		return -1;
	}

	sensor_msgs::ImagePtr msg;

	double nf = 0;

	ros::Time t = ros::Time::now();

	// prebrano iz bebop_autonomy/bebop_driver/data/bebop_camera_calib.yaml
	sensor_msgs::CameraInfo ci;
	ci.height = 368;
	ci.width = 640;
	ci.distortion_model = "plumb_bob";
	ci.D = {-0.001983, 0.015844, -0.003171, 0.001506, 0};
	ci.R = {1, 0, 0, 0, 1, 0, 0, 0, 1};
	ci.P = {400.182373, 0, 323.081936, 0, 0, 403.197845, 172.320207, 0, 0, 0, 1, 0};
	ci.K = {396.17782, 0, 322.453185, 0, 399.798333, 174.243174, 0, 0, 1};


	while(1)
	//while (nf<600)
	{
		Mat frame;
		Mat small_frame;

		bool bSuccess = cap.read(frame); // read a new frame from video

		if (!bSuccess) //if not success, break loop
		{
			cout << "Cannot read the frame from video file" << endl;
			break;
		}

		//resize(frame,small_frame,Size(0,0),0.5,0.5);
		resize(frame,small_frame,Size(640,368),0,0);

		/*imshow("Video", small_frame); //show the frame in "MyVideo" window
		int k = waitKey(20);
		//std::cout << k << std::endl;
		if (k == 1048603)
		{
			//cout << "esc key is pressed by user" << endl;
			break;
		}*/

		std::cout << nf << std::endl;
		ros::Duration d = ros::Duration(nf*delay);

		nf++;

		msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", small_frame).toImageMsg();
		//bag.write("/image_raw", ros::Time::now(), msg);
		bag.write("/image_raw", t+d, msg);
		bag.write("/camera_info", t+d, ci);

	}

	bag.close();

	return 0;
}