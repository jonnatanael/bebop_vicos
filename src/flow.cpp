#include <vector>
#include <fstream>
#include <boost/filesystem.hpp>

// includes for OpenCV
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/tracking.hpp>

// includes for ROS
#include "ros/ros.h"
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/Image.h>
#include <std_msgs/Empty.h>
#include <bebop_vicos/Region.h>
#include <bebop_vicos/Status.h>
#include <bebop_vicos/Im_reg.h>
//#include <ardrone_autonomy/Navdata.h>
//#include <ardrone_autonomy/LedAnim.h>

// includes for custom gui class
#include "gui.h"
#include "util.h"

using namespace cv;
using namespace std;

static const std::string OPENCV_WINDOW = "Master window";

//using namespace legit::tracker;

int cam_h = 368;
int cam_w = 640;

ros::Publisher im_reg_pub;
ros::Publisher shutdown_pub;
ros::Publisher stop_pub;

cv::Mat im_prev;
std::vector<cv::Point2f> features_prev, features_next, features_backup;

//Hough
bool prob = true;


void imageCallback(const sensor_msgs::ImageConstPtr& cam_msg) {
  cv_bridge::CvImagePtr cv_ptr;

  try {
    cv_ptr = cv_bridge::toCvCopy(cam_msg, sensor_msgs::image_encodings::BGR8);
  }
  catch (cv_bridge::Exception& e) {
    ROS_ERROR("cv_bridge exception: %s", e.what());
    return;
  }

  std::vector<uchar> status;
  std::vector<float> err;
  std::vector<cv::Point2f> trackedPts;

  cv::Mat greyMat;
  cv::cvtColor(cv_ptr->image, greyMat, CV_BGR2GRAY);

  int n_feat = 500;


  cv::goodFeaturesToTrack(greyMat, features_next, n_feat, 0.007, 7);
  features_backup = features_next;


  int pts = 0;
  if (!im_prev.empty()) {
    cv::calcOpticalFlowPyrLK(im_prev, greyMat, features_prev, features_next, status, err);

    for (size_t i = 0; i < status.size(); i++)
    {
      if (status[i])
      {
        pts++;
        cv::line(cv_ptr->image, features_prev[i], features_next[i], cv::Scalar(0, 0, 255));
        cv::circle(cv_ptr->image, features_next[i], 2, cv::Scalar(0, 255, 0), -1);
      }
    }
  }

  // track points, if there are not enough remaining, they are redetected
  if (pts < n_feat / 3) {
    features_prev = features_backup;
  }
  else {
    features_prev = features_next;
  }

  im_prev = greyMat;

  cv::imshow(OPENCV_WINDOW, cv_ptr->image);

  int key = cv::waitKey(1);
  switch (key) {
  case (27): // 'esc' triggers shutdown
    ros::shutdown();
    break;
  }
}



int main(int argc, char **argv)
{

  ros::init(argc, argv, "master");

  ros::NodeHandle n("~");
  ros::Subscriber sub_image = n.subscribe("/bebop/image_raw", 1, imageCallback);
  //ros::Subscriber sub_Hough = n.subscribe("/ardrone/image_raw", 1, HoughCallback);

  im_reg_pub = n.advertise<bebop_vicos::Im_reg>("/tracker/init", 1000);

  cv::namedWindow(OPENCV_WINDOW, CV_WINDOW_AUTOSIZE);

  ros::spin();

  return 0;
}