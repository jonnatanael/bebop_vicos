//#include "testing/PID_test.h"
#include "webcam_master.h"

// include for custom gui class
#include "gui.h"
#include "util.h"

using namespace std;
using namespace cv;

void toggle_facedetector() {
    std_msgs::Bool msg;
    if (face_detector_running) {
        msg.data = false;
        face_detector_running = false;
    }
    else {
        msg.data = true;
        face_detector_running = true;
    }
    fd_toggle_pub.publish(msg);
}

void init_PID() {
    pid_x.setGains(x_Kp, x_Ki, x_Kd);
    pid_y.setGains(y_Kp, y_Ki, y_Kd);

    pid_x.setReference(x_ref);
    pid_y.setReference(y_ref);
}

int rangeRandomAlg (int min, int max) {
// for selecting a random integer from a defined range (i.e. selecting a random detected face)
    int n = max - min + 1;
    int remainder = RAND_MAX % n;
    int x;
    do {
        x = rand();
    }
    while (x >= RAND_MAX - remainder);
    return min + x % n;
}

void shutdownNodes() {
    shutdown_pub.publish(std_msgs::Empty());
}

void regionCallback(const bebop_vicos::Region reg) {
    // saves current object region
    x = reg.x; y = reg.y; h = reg.h; w = reg.w; psr = reg.psr;
    x_c = x + w / 2;
    y_c = y + h / 2;


    float margin = 0.1;
    int shift = 0;
    bool move_needed = false;

    // calculate distance from image center
    distance_from_center = sqrt(pow((x_c - image_center_x), 2) + pow((y_c - image_center_y), 2));

    if (video) {
        // save region
        region_file << std::to_string(reg.stamp.sec) << '_' << std::to_string(reg.stamp.nsec) << " ";
        region_file << reg.x << "," << reg.y << "," << reg.h << "," << reg.w << "," << reg.psr << endl;

    }    
}

void statusCallback(const bebop_vicos::Status status) {
    tracking = status.tracking;
    if (tracking == false) {
        psr = 0;
        prediction_pending = false;
    }
    lost = status.lost;
    if (video) {
        //save status
        status_file << std::to_string(status.stamp.sec) << '_' << std::to_string(status.stamp.nsec) << " ";
        status_file << std::to_string(status.tracking) << " " << std::to_string(status.lost) << " ";
        status_file << std::to_string(target_angle_enabled) << " " << std::to_string(target_angle_enabled ? target_angle : 0) << " ";
        status_file << std::to_string(look_at) << " ";
        status_file << std::to_string(test_running) << " ";
        status_file << std::to_string(test_mode) << " ";
        status_file << std::to_string(camera_move_pending) << " ";
        status_file << std::to_string(cam_x) << " ";
        status_file << std::to_string(cam_y) << " ";
        if (camera_move_pending) {
            camera_move_pending = false;
        }
        status_file << endl;

        if (!tracking) {
            region_file << std::to_string(status.stamp.sec) << '_' << std::to_string(status.stamp.nsec) << " ";
            region_file << -1 << "," << -1 << "," << -1 << "," << -1 << "," << -1 << endl;
        }

    }
}

void featuresCallback(const bebop_vicos::PointVector v) {
    pv = v;
}

void featuresKPCallback(const bebop_vicos::KPVector v) {
    kpv = v;
}

void fdCallback(const facedetector::Detection d) {
    faces = d;
}

void processFaces(cv_bridge::CvImagePtr cv_ptr) {
    if (faces.x.size() > 0 && init_mode == 2) {
        if (!tracking) {
            for (int i = 0; i < faces.x.size(); i++) {
                cv::rectangle(cv_ptr->image, cv::Rect(faces.x.at(i), faces.y.at(i), faces.width.at(i), faces.height.at(i)), cv::Scalar(255, 0, 0), 2);
            }

            // if initializing on faces, choose a random face and init tracker
            int randNum = rangeRandomAlg (0, faces.x.size() - 1);
            tracking = true;
            bebop_vicos::Im_reg msg = bebop_vicos::Im_reg();
            msg.image = frame;
            bebop_vicos::Region r = bebop_vicos::Region();
            r.x = faces.x.at(randNum); r.y = faces.y.at(randNum); r.w = faces.width.at(randNum); r.h = faces.height.at(randNum);
            msg.region = r;
            im_reg_pub.publish(msg);
        }
        else
            faces = facedetector::Detection();
    }
}

void processRegion(cv_bridge::CvImagePtr cv_ptr) {
    double opacity = 0.7;
    if (h > 0 && w > 0) {
        cv::Point statePt = util::update_KF(&KF, &measurement, x, y, h, w);
        if (!lost) {
            gui::drawAlphaBorder(cv_ptr, cv::Rect(x, y, w, h), opacity, cv::Scalar(255, 255, 255), 3, true);
        }
        else {
            gui::drawAlphaBorder(cv_ptr, cv::Rect(x, y, w, h), opacity, cv::Scalar(0, 0, 255), 3, true);
        }

        gui::drawAlphaBorder(cv_ptr, cv::Rect(x_c - reference.width / 2, y_c - reference.height / 2, reference.width, reference.height), 0.3, cv::Scalar(255, 255, 255), 2);

        // draw line from center to object, for debugging purposes
        int center_x = x + w / 2;
        int center_y = y + h / 2;

        if (trajectory) {
            gui::handle_queue(cv_ptr, &v, cv::Point(x + w / 2, y + h / 2), cv::Scalar(0, 0, 255)); // trajectory
            gui::handle_queue(cv_ptr, &v_kf, statePt, cv::Scalar(0, 255, 0));
        }

    }
}

void drawText(cv_bridge::CvImagePtr cv_ptr) {
    std::stringstream fmt;

    fmt << "pan: " << cam_x << ", tilt: " << cam_y << "\n";
    //fmt << "battery: " << battery << "%, psr: " << psr << "\n";
    fmt << "target angle: " << target_angle << endl;
    // rounds to 3 decimals
    //fmt << "rotX: " << roundf(rotX * 1000) / 1000 << ", rotY: " << roundf(rotY * 1000) / 1000 << ", rotZ: " << roundf(rotZ * 1000) / 1000;
    fmt << "Initialization: ";
    if (init_mode == 0) {
        fmt << "manual rectangle";
    }
    else if (init_mode == 1) {
        fmt << "manual click";
    }
    else if (init_mode == 2) {
        fmt << "face detection";
    }

    fmt << "\n";

    fmt << "Camera: ";
    if (look_at) {
        fmt << "dynamic";
    }
    else {
        fmt << "static";
    }

    cv::Point pos = cv::Point(5 * cam_w / 100.0, cam_h - 25 * cam_h / 100.0); // text position
    gui::writeText(cv_ptr, pos, fmt.str().c_str());
}

void draw_data_text(Mat& img) {
    std::stringstream col1, col2;
    col1 << "Bebop data:" << endl << endl;

    col1 << "attitude:" << endl;
    col1 << "    rotX: " << roundf(rotX * 1000) / 1000 << endl;
    col1 << "    rotY: " << roundf(rotY * 1000) / 1000 << endl;
    col1 << "    rotZ: " << roundf(rotZ * 1000) / 1000 << endl;

    col1 << "altitude:" << endl;
    col1 << "    " << altitude << endl;

    col1 << "camera position:" << endl;
    col1 << "    pan: " << cam_x << endl;
    col1 << "    tilt: " << cam_y << endl;

    col1 << "battery:" << endl;
    col1 << "    " << battery << "%" << endl;



    col2 << "System data:" << endl << endl;

    col2 << "tracking: " << (tracking ? "true" : "false") << endl;
    col2 << "lost: " << (lost ? "true" : "false") << endl;
    col2 << "psr: " << (tracking ? psr : -1) << endl;

    col2 << "target angle enabled: " << (target_angle_enabled ? "true" : "false") << endl;
    col2 << "target angle: " << (target_angle_enabled ? target_angle : -1) << endl;

    //col2 << "tracker initialization: " << (init_mode == 0 ? "manual" : "face detection") << endl;

    col2 << "tracker initialization: ";
    switch (init_mode) {
    case 0:
        col2 << "rectangle" << endl;
        break;
    case 1:
        col2 << "click" << endl;
        break;
    case 2:
        col2 << "face detection" << endl;
        break;
    }

    col2 << "camera: " << (look_at ? "dynamic" : "static") << endl;

    col2 << "displaying trajectory: " << (trajectory ? "true" : "false") << endl;
    col2 << "displaying features: " << (display_features ? "true" : "false") << endl;
    col2 << "recording locally: " << (video ? "true" : "false") << endl;
    col2 << "recording onboard: " << (bebop_rec ? "true" : "false") << endl;

    // testing
    col2 << "distance from center: " << distance_from_center << endl;


    gui::writeText(img, Point(30, 30), col1.str().c_str());
    gui::writeText(img, Point(img.cols / 2, 30), col2.str().c_str());
}

void handle_key(int key, uint32_t sec, uint32_t nsec) {
    key = key % 256;
    //cout << key << endl;
    switch (key) {
    case ((int)('q')): // if 'q' is pressed, stop tracker
        stop_pub.publish(std_msgs::Empty());
        tracking = false;
        lost = false;
        predicted.width = -1;
        h = 0; w = 0;
        break;
    case ((int)('t')): // sets trajectory visibility
        util::toggle(trajectory);
        break;
    case ((int)('f')): // toggle feature display
        util::toggle(display_features);
        break;
    case ((int)('e')): // toggle initialization mode
        init_mode++;
        init_mode = init_mode % 3;

        // stop tracker when switching modes, seems logical
        stop_pub.publish(std_msgs::Empty());
        tracking = false;
        lost = false;
        break;
    case ((int)('g')):
        cout << "face detection toggled" << endl;
        toggle_facedetector();
        break;
    case ((int)('r')):
        
        break;
    case (49): // '1'
        init_region.width -= init_region.width * init_scale; init_region.height -= init_region.height * init_scale;
        init_region.x = mouse.x - init_region.width / 2; init_region.y = mouse.y - init_region.height / 2;
        break;
    case (50): // '2'
        init_region.width += init_region.width * init_scale; init_region.height += init_region.height * init_scale;
        init_region.x = mouse.x - init_region.width / 2; init_region.y = mouse.y - init_region.height / 2;
        break;
    case (27): // 'esc' triggers shutdown
        shutdownNodes();
        ros::shutdown();
        break;
    }
}

void imageCallback(const sensor_msgs::ImageConstPtr& cam_msg) {
    cv_bridge::CvImagePtr cv_ptr;

    try {
        cv_ptr = cv_bridge::toCvCopy(cam_msg, sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception& e) {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }

    frame = *cam_msg;

    // save image before modifying it
    /*if (rec) {
        string s = im_dir + "/" + subdir + '/' + std::to_string(cam_msg->header.stamp.sec) + '_' + std::to_string(cam_msg->header.stamp.nsec) + ".jpg";
        cv::imwrite(s, cv_ptr->image);
        gui::drawRec(cv_ptr);
    }*/

    // draw features
    if (display_features && pv.v.size() > 0) {
        for (std::vector<bebop_vicos::Point2D>::const_iterator i = pv.v.begin(); i != pv.v.end(); ++i) {
            bebop_vicos::Point2D cur = *i;
            cv::circle(cv_ptr->image, cv::Point(cur.x, cur.y), 2, cv::Scalar(0, 255, 255), -1);
        }
    }

    // draw keypoints
    if (display_features && kpv.v.size() > 0) {
        for (std::vector<bebop_vicos::KeyPoint>::const_iterator i = kpv.v.begin(); i != kpv.v.end(); ++i) {
            bebop_vicos::KeyPoint cur = *i;
            cv::circle(cv_ptr->image, cv::Point(cur.pt.x, cur.pt.y), 2, cv::Scalar(0, 255, 255), -1);
            cv::circle(cv_ptr->image, cv::Point(cur.pt.x, cur.pt.y), cur.size, cv::Scalar(0, 255, 255), 1);
        }
    }

    // draw tracker region
    if (tracking)
        processRegion(cv_ptr);

    processFaces(cv_ptr);

    // draw status text
    //drawText(cv_ptr);

    // draw battery level
    /*if (battery != -1)
        gui::drawBattery(cv_ptr, battery);

    // draw coordinate cross
    coordinates::drawAngles(cv_ptr, rotX, rotY, rotZ);*/

    // draw selection rectangle
    if (init_mode == 0)
        cv::rectangle(cv_ptr->image, sel, cv::Scalar(0, 0, 0), 2);
    else if (init_mode == 1 && !tracking)
        cv::rectangle(cv_ptr->image, init_region, cv::Scalar(0, 0, 0), 2);


    if (tracking && yaw != 0 && show_pid_error) {
        int factor_pitch = 40;
        int line = 2;
        int factor = 100;
        // yaw
        cv::line(cv_ptr->image, cv::Point(x_c, y_c), cv::Point(x_c + yaw * factor, y_c), Scalar(0, 0, 255), line);
        // z
        cv::line(cv_ptr->image, cv::Point(x_c, y_c), cv::Point(x_c, y_c - z * factor), Scalar(255, 0, 0), line);
    }

    if (tracking) {
        // pitch
        // draw line from current vertices to reference size
        gui::drawAlphaLine(cv_ptr, cv::Point(x, y), cv::Point(x_c - reference.width / 2, y_c - reference.height / 2), 0.3, Scalar(255, 255, 255), 2);
        gui::drawAlphaLine(cv_ptr, cv::Point(x + w, y), cv::Point(x_c + reference.width / 2, y_c - reference.height / 2), 0.3, Scalar(255, 255, 255), 2);
        gui::drawAlphaLine(cv_ptr, cv::Point(x, y + h), cv::Point(x_c - reference.width / 2, y_c + reference.height / 2), 0.3, Scalar(255, 255, 255), 2);
        gui::drawAlphaLine(cv_ptr, cv::Point(x + w, y + h), cv::Point(x_c + reference.width / 2, y_c + reference.height / 2), 0.3, Scalar(255, 255, 255), 2);
    }

    if (video)
        vw << cv_ptr->image;

    if (!tracking) {
        v.clear();
        v_kf.clear();
        util::init_KF(&KF, &measurement); // reinit KF
    }

    cv::imshow(OPENCV_WINDOW, cv_ptr->image);

    if (show_data) {
        data_image.setTo(Scalar(0, 0, 0));
        draw_data_text(data_image);
        cv::imshow(DATA_WINDOW, data_image);
    }

    handle_key(cv::waitKey(1), cam_msg->header.stamp.sec, cam_msg->header.stamp.nsec); // needs sec and nsec for creating record directories

}

void onMouse( int event, int x, int y, int f, void* ) {
    // for one-click init
    mouse.x = x; mouse.y = y;
    init_region.x = x - init_region.width / 2; init_region.y = y - init_region.height / 2;

    if (clicked && !tracking && init_mode == 0) {
        if (P1.x > P2.x) {
            sel.x = P2.x;
            sel.width = P1.x - P2.x;
        }
        else {
            sel.x = P1.x;
            sel.width = P2.x - P1.x;
        }

        if (P1.y > P2.y) {
            sel.y = P2.y;
            sel.height = P1.y - P2.y;
        }
        else {
            sel.y = P1.y;
            sel.height = P2.y - P1.y;
        }
    }
    switch (event) {
    case CV_EVENT_LBUTTONDOWN:
        clicked = true;
        P1.x = x;
        P1.y = y;
        P2.x = x;
        P2.y = y;
        break;
    case CV_EVENT_RBUTTONDOWN: {
        
        break;
    }
    case CV_EVENT_LBUTTONUP: {
        P2.x = x;
        P2.y = y;
        // init tracker
        bebop_vicos::Im_reg msg = bebop_vicos::Im_reg();
        bebop_vicos::Region r = bebop_vicos::Region();
        if (!tracking) {
            tracking = true;
            msg.image = frame;

            // if initializing with rectangle
            if (init_mode == 0 && sel.area() > 0) {
                r.x = sel.x; r.y = sel.y; r.w = sel.width; r.h = sel.height;
                reference = sel;
                msg.region = r;
                im_reg_pub.publish(msg);
            }
            else if (init_mode == 1) {
                r.x = init_region.x;
                r.y = init_region.y;
                r.w = init_region.width;
                r.h = init_region.height;
                reference = Rect(r.x, r.y, r.w, r.h);
                msg.region = r;
                im_reg_pub.publish(msg);
            }
        }
        sel = cv::Rect(0, 0, 0, 0);
        clicked = false;
        break;
    }
    case CV_EVENT_MOUSEMOVE:
        if (clicked && init_mode == 0) {
            if (clicked) {
                P2.x = x;
                P2.y = y;
            }
        }
        break;
    default: break;
    }
}

void get_params(ros::NodeHandle n) {

    n.getParam("image_w", cam_w);
    n.getParam("image_h", cam_h);
    
    n.getParam("image_topic", image_topic);

    n.getParam("status_file", status_filename);

    n.getParam("status_file", status_filename);
    n.getParam("region_file", region_filename);
    n.getParam("image_dir", im_dir);

    n.getParam("trajectory", trajectory);
    n.getParam("show_pid_error", show_pid_error);

    n.getParam("save_video", video);
    n.getParam("show_data", show_data);
    n.getParam("init_mode", init_mode);

}

int main(int argc, char **argv) {

    ros::init(argc, argv, "PID_test");

    rng = RNG(::getpid());

    ros::NodeHandle n("~");

    get_params(n);

    // subscribers
    sub_image = n.subscribe(image_topic, 1, imageCallback);
    sub_region = n.subscribe("/tracker/region", 30, regionCallback); // 30 zato, da se pri zapisovanju ne izgubi kakšna slika
    sub_status = n.subscribe("/bebop_vicos/status", 30, statusCallback);
    sub_features = n.subscribe("/bebop/features", 1, featuresCallback);
    sub_featuresKP = n.subscribe("/bebop/KP", 1, featuresKPCallback);
    sub_fd = n.subscribe("/facedetector/faces", 1, fdCallback);


    // publishers
    im_reg_pub = n.advertise<bebop_vicos::Im_reg>("/tracker/init", 1000);
    shutdown_pub = n.advertise<std_msgs::Empty>("/master/shutdown", 1000);
    stop_pub = n.advertise<std_msgs::Empty>("/tracker/stop", 1000);
    fd_toggle_pub = n.advertise<std_msgs::Bool>("/facedetector/toggle", 1000);

    init_PID();

    if (video) {
        ros::Time time = ros::Time::now();
        subdir = std::to_string(time.sec) + '_' + std::to_string(time.nsec);
        boost::filesystem::create_directories(im_dir + '/' + subdir);

        // stup video file
        vw = cv::VideoWriter();
        boost::filesystem::create_directories(im_dir);
        vw.open(im_dir + '/' + subdir + '/' + std::to_string(time.sec) + '_' + std::to_string(time.nsec) + "_video.avi", CV_FOURCC('M', 'P', 'E', 'G'), 30, cv::Size(cam_w, cam_h));

        // setup data files
        status_file.open(im_dir + '/' + subdir + '/' + status_filename);
        region_file.open(im_dir + '/' + subdir + '/' + region_filename);
        params_file.open(im_dir + '/' + subdir + "/params.txt");

        // write launch parameters
        params_file << "PID_camera" << endl;
        params_file << "x_Kp: " << x_Kp << endl;
        params_file << "x_Ki: " << x_Ki << endl;
        params_file << "x_Kd: " << x_Kd << endl;
        params_file << "y_Kp: " << y_Kp << endl;
        params_file << "y_Ki: " << y_Ki << endl;
        params_file << "y_Kd: " << y_Kd << endl;

        params_file << "move_size: " << move_size << endl;
        params_file << "center_deadband: " << center_deadband << endl;


    }

    //cv::namedWindow(OPENCV_WINDOW, CV_WINDOW_AUTOSIZE); // static window
    cv::namedWindow(OPENCV_WINDOW, WINDOW_NORMAL); // resizable window
    cv::resizeWindow(OPENCV_WINDOW, cam_w*1.5, cam_h*1.5);
    if (show_data) {
        cv::namedWindow(DATA_WINDOW, CV_WINDOW_AUTOSIZE); // resizable window
        data_image = Mat(800, 800, CV_8UC3, Scalar(0, 0, 0));
    }

    // set initial region size based on image size
    if (init_w == -1 && init_h == -1) {
        int size = (min(cam_h, cam_w) * 0.15f); // set initial bounding box to 15% of image height (assuming image is landscape)
        init_h = size;
        init_w = size;
        init_region = Rect(-100, -100, (int)init_w, (int)init_h);
    }

    setMouseCallback(OPENCV_WINDOW, onMouse, NULL);

    util::init_KF(&KF, &measurement);

    ros::spin();

    return 0;
}