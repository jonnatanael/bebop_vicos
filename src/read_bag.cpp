#include <stdio.h>
#include <iostream>
#include "ros/ros.h"
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>

#include <cv_bridge/cv_bridge.h>

#include <boost/foreach.hpp>
#define foreach BOOST_FOREACH

using namespace cv;
using namespace std;

// program that reads messages from a.bag file and displays them if they are images

/** @function main */
int main( int argc, char** argv ) {

	rosbag::Bag bag;
	bag.open("/home/jon/Desktop/grotta_del_paranco/astra/2016-10-19-11-17-17.bag", rosbag::bagmode::Read);

	std::vector<std::string> topics;
	//topics.push_back(std::string("/camera/depth/camera_info"));
	//topics.push_back(std::string("/camera/rgb/camera_info"));
	topics.push_back(std::string("/camera/rgb/image_raw"));
	topics.push_back(std::string("/camera/depth/image_raw"));

	rosbag::View view(bag, rosbag::TopicQuery(topics));

	// video write file
	int fps = 30;
	//VideoWriter video("out.avi",CV_FOURCC('M','J','P','G'),fps, Size(640,480),true);

	VideoWriter video;
	int codec = CV_FOURCC('M', 'J', 'P', 'G');  // select desired codec (must be available at runtime)
	video.open("out.avi", codec, fps, Size(640 * 2, 480), true);
	// check if we succeeded
	if (!video.isOpened()) {
		cerr << "Could not open the output video file for write\n";
	}

	Mat m1, m2, m2_rgb, full;


	int i = 0;
	int cnt = 0;
	bool gray = false;
	bool rgb = false;

	foreach (rosbag::MessageInstance const m, view) {
		//cnt = 0;
		//cout << i++ << endl;
		/*std_msgs::String::ConstPtr s = m.instantiate<std_msgs::String>();
		if (s != NULL){
		    //ASSERT_EQ(s->data, std::string("foo"));
		}

		std_msgs::Int32::ConstPtr i = m.instantiate<std_msgs::Int32>();
		if (i != NULL){
		    //ASSERT_EQ(i->data, 42);
		}*/

		//if (i>10)break;

		sensor_msgs::CameraInfo::ConstPtr ci = m.instantiate<sensor_msgs::CameraInfo>();
		if (ci != NULL) {
			//cout << *ci << endl;
		}

		sensor_msgs::Image::ConstPtr img = m.instantiate<sensor_msgs::Image>();
		if (img != NULL) {
			cout << i << endl;
			
			if ((*img).step == (640 * 3)) { // i.e. rgb image
				cnt++;
				rgb = true;
				//cout << "rgb" << endl;
				cv_bridge::CvImagePtr cv_ptr;
				try {
					cv_ptr = cv_bridge::toCvCopy(img, sensor_msgs::image_encodings::BGR8);
				}
				catch (cv_bridge::Exception& e) {
					ROS_ERROR("cv_bridge exception: %s", e.what());
					return -1;
				}
				//cout << cv_ptr->image.type() << endl;
				cv_ptr->image.copyTo(m1);
				//m1.convertTo(m1, CV_8U, 0.00390625);
				//imshow("rgb", cv_ptr->image);
			}
			if ((*img).step == (640 * 2)) {
				cnt++;
				gray = true;
				//cout << "gray" << endl;
				
				//cout << "gray" << endl;
				cv_bridge::CvImagePtr cv_ptr;
				try {
					cv_ptr = cv_bridge::toCvCopy(img, sensor_msgs::image_encodings::MONO16);
				}
				catch (cv_bridge::Exception& e) {
					ROS_ERROR("cv_bridge exception: %s", e.what());
					return -1;
				}
				cv_ptr->image.copyTo(m2);
				m2.convertTo(m2, CV_8U, 0.00390625);
				cvtColor(m2, m2_rgb, COLOR_GRAY2RGB);
				//imshow("gray", cv_ptr->image);
				//waitKey(1);
				i++;
			}
		}
		//cout << cnt << endl;

		//if (cnt == 2) {
		if (rgb && gray){
			//cout << m1.type() << endl;
			//cout << m2.type() << endl;
			hconcat(m1, m2_rgb, full);
			video.write(full);
			//cnt = 0;
			rgb = false;
			gray = false;
		}

	}

	bag.close();

	return 0;
}