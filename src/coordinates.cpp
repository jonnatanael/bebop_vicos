#include <iostream>
#include "coordinates.h"
#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>

using namespace cv;

void coordinates::drawAngles(cv_bridge::CvImagePtr cv_ptr, double rotX, double rotY, double rotZ) {

  // unit coordinate system cross
  Mat cord = (Mat_<double>(7, 3) << 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1,
              -1, 0, 0, 0, -1, 0, 0, 0, -1);


  // location of coordinate system in image
  cv::Point p = cv::Point(50, 50);

  // projection parameters
  Mat cam = (Mat_<double>(3, 3) << coordinates::cam_w, 0, p.x, 0, coordinates::cam_h, p.y, 0, 0, 1);
  Mat trans = (Mat_<double>(3, 1) << 0, 0, coordinates::tz);
  Mat rot = (Mat_<double>(3, 1) << 0, 0, 0);

  // auxiliary variables
  Mat cord_, out, Rx, Ry, Rz;
  Scalar clr(255, 255, 255);
  Point origin, cur;



  Rx = (Mat_<double>(3, 3) << 1, 0, 0, 0, cos(rotX), -sin(rotX), 0, sin(rotX), cos(rotX));
  Ry = (Mat_<double>(3, 3) << cos(rotY), 0, sin(rotY), 0, 1, 0, -sin(rotY), 0, cos(rotY));
  Rz = (Mat_<double>(3, 3) << cos(rotZ), -sin(rotZ), 0, sin(rotZ), cos(rotZ), 0, 0, 0, 1);


  cord_ = Rx * Ry * Rz * cord.t();
  cord_ = cord_.t();
  cv::projectPoints(cord_, rot, trans, cam, Mat(), out);
  origin = Point(out.at<double>(0, 0), out.at<double>(0, 1));

  for (int i = 1; i < out.rows; i++) {
    // draws coordinate system
    cur = Point(out.at<double>(i, 0), out.at<double>(i, 1));
    switch (i % 3) {
    case (1):
      clr = Scalar(255, 0, 0);
      break;
    case (2):
      clr = Scalar(0, 255, 0);
      break;
    case (0):
      clr = Scalar(0, 0, 255);
      break;
    }
    circle(cv_ptr->image, Point(out.at<double>(i, 0), out.at<double>(i, 1)), 3, clr);
    line(cv_ptr->image, origin, cur, clr, 2);
  }
  circle(cv_ptr->image, Point(out.at<double>(2, 0), out.at<double>(2, 1)), 5, Scalar(255, 255, 255), -1);
}

