// this should really be in the implementation (.cpp file)
#include <nodelet_test.h>
#include <pluginlib/class_list_macros.h>
#include <cv_bridge/cv_bridge.h>
//#include "ros/ros.h"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <sensor_msgs/image_encodings.h>
#include "gui.h"
#include "coordinates.h"
#include <std_msgs/Empty.h>
#include <bebop_vicos/Region.h>
#include <bebop_vicos/Tracker_bundle.h>
#include <bebop_vicos/Im_reg.h>
#include <bebop_vicos/Status.h>

// watch the capitalization carefully
PLUGINLIB_EXPORT_CLASS(bebop_vicos::nodelet_test, nodelet::Nodelet);



namespace bebop_vicos
{
void nodelet_test::onInit()
{
	ros::NodeHandle& nh = getNodeHandle();
	ros::NodeHandle& private_nh = getPrivateNodeHandle();

	cam_sub = nh.subscribe("/bebop/image_raw", 1, &nodelet_test::ImageCallback, this);
	rec_sub = nh.subscribe("/bebop/states/ARDrone3/MediaRecordState/PictureStateChangedV2", 1, &nodelet_test::Ardrone3MediaRecordStatePictureStateChangedCallback, this);
	battery_sub = nh.subscribe("/bebop/states/common/CommonState/BatteryStateChanged", 1, &nodelet_test::batteryCallback, this);
	attitude_sub = nh.subscribe("/bebop/states/ARDrone3/PilotingState/AttitudeChanged", 1, &nodelet_test::attitudeCallback, this);
	speed_sub = nh.subscribe("/bebop/states/ARDrone3/PilotingState/SpeedChanged", 1, &nodelet_test::speedCallback, this);
	status_sub = nh.subscribe("/bebop_vicos/status", 1, &nodelet_test::statusCallback, this);

	// tracker related subs
	sub_region = nh.subscribe("/tracker/region", 1, &nodelet_test::regionCallback, this);

	im_reg_pub = nh.advertise<bebop_vicos::Im_reg>("/tracker/init", 1000);
	shutdown_pub = nh.advertise<std_msgs::Empty>("/master/shutdown", 1000);

	x = 0; y = 0; h = 0; w = 0;

	//cv::namedWindow(OPENCV_WINDOW);

	//cv::setMouseCallback("Master window", &nodelet_test::onMouse, NULL);

	/*tracking = false;
	clicked = false;

	sel = cv::Rect(0, 0, 0, 0);
	P1 = cv::Point (0, 0);
	P2 = cv::Point (0, 0);*/
	ros::Duration(1).sleep();
}

void nodelet_test::regionCallback(const bebop_vicos::Region& reg) {
	//cv::rectangle(cv_ptr->image, cv::Rect(x,y,h,w), cv::Scalar(255,0,0), 3);
	//std::cout << "reg" << std::endl;
	//std::cout << reg << std::endl;
	x = reg.x; y = reg.y; h = reg.h; w = reg.w;
}

void nodelet_test::statusCallback(const bebop_vicos::Status& status) {
	tracking = status.tracking;
	//std::cout << tracking << std::endl;
	lost = status.lost;
}


void nodelet_test::processRegion(cv_bridge::CvImagePtr cv_ptr) {
	NODELET_INFO("processing region start");
	if (h > 0 && w > 0) {
		if (!lost) {
			cv::rectangle(cv_ptr->image, cv::Rect(x, y, w, h), cv::Scalar(255, 0, 0), 3);
		}
		else {
			cv::rectangle(cv_ptr->image, cv::Rect(x, y, w, h), cv::Scalar(0, 0, 255), 3);
		}
		h = 0; w = 0;
	}
	NODELET_INFO("processing region end");
}

void nodelet_test::ImageCallback(const sensor_msgs::ImageConstPtr& cam_msg) {
	NODELET_WARN("master image callback start");
	cv_bridge::CvImagePtr cv_ptr;

	try {
		cv_ptr = cv_bridge::toCvCopy(cam_msg, sensor_msgs::image_encodings::BGR8);
	}
	catch (cv_bridge::Exception& e) {
		ROS_ERROR("cv_bridge exception: %s", e.what());
		return;
	}

	//frame = *cam_msg;

	if (battery != -1)
		gui::drawBattery(cv_ptr, battery);

	coordinates::drawAngles(cv_ptr, rotX, rotY, rotZ);

	std::cout << x << y << h << w << std::endl;
	processRegion(cv_ptr);

	if (test == 1 or !tracking) {
		if (im_reg_pub){
			bebop_vicos::Im_reg msg = bebop_vicos::Im_reg();
			msg.image = *cam_msg;
			bebop_vicos::Region r = bebop_vicos::Region();
			r.x = 100; r.y = 100; r.w = 30; r.h = 30;
			msg.region = r;
			im_reg_pub.publish(msg);
			test = 0;
		}
	}


	//gui::writeText(cv_ptr, cv::Point(5*cam_w/100.0,cam_h-5*cam_h/100.0), "testing with a longer string of alphanumeric charact3rs");

	cv::imshow("test", cv_ptr->image);
	int key = cv::waitKey(1);
	switch (key) {
	case ((int)('a')): // 'esc' triggers shutdown
		if (im_reg_pub) {
			bebop_vicos::Im_reg msg = bebop_vicos::Im_reg();
			msg.image = *cam_msg;
			bebop_vicos::Region r = bebop_vicos::Region();
			r.x = 100; r.y = 100; r.w = 30; r.h = 30;
			msg.region = r;
			im_reg_pub.publish(msg);
			test = 0;
		}
		break;
	case (27): // 'esc' triggers shutdown
		ros::shutdown();
		break;
	}
	NODELET_WARN("master image callback end");

}

void nodelet_test::Ardrone3MediaRecordStatePictureStateChangedCallback(const bebop_msgs::Ardrone3MediaRecordStatePictureStateChangedV2& msg) {
	std::cout << msg << std::endl;
}

void nodelet_test::batteryCallback(const bebop_msgs::CommonCommonStateBatteryStateChanged& msg) {
	std::cout << msg << std::endl;
	battery = msg.percent;
}

void nodelet_test::attitudeCallback(const bebop_msgs::Ardrone3PilotingStateAttitudeChanged& msg) {
	std::cout << "attitude changed" << std::endl;
	std::cout << msg << std::endl;
	rotX = -CV_PI / 2 + msg.pitch;
	rotY = msg.roll;
	rotZ = msg.yaw;
}

void nodelet_test::speedCallback(const bebop_msgs::Ardrone3PilotingStateSpeedChanged& msg) {
	std::cout << "speed changed" << std::endl;
	std::cout << msg << std::endl;
}

void nodelet_test::onMouse( int event, int x, int y, int f, void* param) {

	//nodelet_test *this_ = reinterpret_cast<nodelet_test*>(param);
	/*
	if (clicked && !tracking) {
		if (P1.x > P2.x) {
			sel.x = P2.x;
			sel.width = P1.x - P2.x;
		}
		else {
			sel.x = P1.x;
			sel.width = P2.x - P1.x;
		}

		if (P1.y > P2.y) {
			sel.y = P2.y;
			sel.height = P1.y - P2.y;
		}
		else {
			sel.y = P1.y;
			sel.height = P2.y - P1.y;
		}

	}
	switch (event) {
	case CV_EVENT_LBUTTONDOWN:
		clicked = true;
		P1.x = x;
		P1.y = y;
		P2.x = x;
		P2.y = y;
		break;
	case CV_EVENT_LBUTTONUP:
		P2.x = x;
		P2.y = y;
		// init tracker
		if (sel.area() > 0 && !tracking) {
			// send region and image
			tracking = true;
			bebop_vicos::Im_reg msg = bebop_vicos::Im_reg();
			msg.image = frame;
			bebop_vicos::Region r = bebop_vicos::Region();
			r.x = sel.x; r.y = sel.y; r.w = sel.width; r.h = sel.height;
			msg.region = r;
			im_reg_pub.publish(msg);
		}
		sel = cv::Rect(0, 0, 0, 0);
		clicked = false;
		break;
	case CV_EVENT_MOUSEMOVE:
		if (clicked) {
			P2.x = x;
			P2.y = y;
		}
		break;
	default: break;
	}*/
}

}