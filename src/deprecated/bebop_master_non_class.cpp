#include <vector>
#include <fstream>
#include <boost/filesystem.hpp>

#include "bebop_master.h"

// includes for OpenCV
#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/tracking.hpp>

// includes for ROS
#include "ros/ros.h"
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/Image.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/Empty.h>
#include <bebop_vicos/Region.h>
#include <bebop_vicos/Status.h>
#include <bebop_vicos/Im_reg.h>
#include <bebop_msgs/Ardrone3PilotingStateAttitudeChanged.h>
#include <bebop_msgs/Ardrone3PilotingStateAltitudeChanged.h>
#include <bebop_msgs/Ardrone3PilotingStateFlatTrimChanged.h>
#include <bebop_msgs/CommonCommonStateBatteryStateChanged.h>
#include <bebop_msgs/Ardrone3PilotingStateSpeedChanged.h>
#include <bebop_msgs/Ardrone3CameraStateOrientation.h>

/*extern "C"
{
#include "libARCommands/ARCommands.h"
#include "libARDiscovery/ARDiscovery.h"
#include "libARSAL/ARSAL.h"
#include "libARController/ARController.h"
}*/

//includes for tracker
//#include "tracker.h"

// includes for custom gui class
#include "gui.h"

//ARCONTROLLER_Device_t* device_controller_ptr_;

static const std::string OPENCV_WINDOW = "Master window";

//using namespace legit::tracker;
using namespace std;
using namespace cv;

int cam_h = 368;
int cam_w = 640;

// za označevanje objekta z miško
bool clicked = false;
bool tracking = false;
cv::Rect sel(0, 0, 0, 0);
cv::Point P1(0, 0);
cv::Point P2(0, 0);
bool lost = false; // for tracker failure
bool redetecting = false;

bool rec = false;

ros::Publisher cam_move_pub;
ros::Publisher im_reg_pub;
ros::Publisher shutdown_pub;
ros::Publisher stop_pub;

sensor_msgs::Image frame;

// tracker config file
string par;

int x, y, h, w;

// for battery status
int battery = 50;

double rotX, rotY, rotZ;
double rx, ry, rz;
double tz = -15;

// koordinatni sistem
Mat cord = (Mat_<double>(7, 3) << 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1,
            -1, 0, 0, 0, -1, 0, 0, 0, -1);
Mat cord_;

Mat out;
Mat Rx, Ry, Rz;

Point p(50, 50);

Mat cam = (Mat_<double>(3, 3) << cam_w, 0, p.x, 0, cam_h, p.y, 0, 0, 1);
Mat trans = (Mat_<double>(3, 1) << 0, 0, tz);
Mat rot = (Mat_<double>(3, 1) << 0, 0, 0);

int cam_y = 0;
int cam_z = 0;

Point origin, cur;
Scalar clr(255, 255, 255);

void drawAngles(cv_bridge::CvImagePtr cv_ptr) {
  Rx = (Mat_<double>(3, 3) << 1, 0, 0, 0, cos(rotX), -sin(rotX), 0, sin(rotX), cos(rotX));
  Ry = (Mat_<double>(3, 3) << cos(rotY), 0, sin(rotY), 0, 1, 0, -sin(rotY), 0, cos(rotY));
  Rz = (Mat_<double>(3, 3) << cos(rotZ), -sin(rotZ), 0, sin(rotZ), cos(rotZ), 0, 0, 0, 1);
  Mat trans = (Mat_<double>(3, 1) << 0, 0, tz);

  cord_ = Rx * Ry * Rz * cord.t();
  cord_ = cord_.t();
  cv::projectPoints(cord_, rot, trans, cam, Mat(), out);
  origin = Point(out.at<double>(0, 0), out.at<double>(0, 1));

  for (int i = 1; i < out.rows; i++) {
    // draws coordinate system
    cur = Point(out.at<double>(i, 0), out.at<double>(i, 1));
    switch (i % 3) {
    case (1):
      clr = Scalar(255, 0, 0);
      break;
    case (2):
      clr = Scalar(0, 255, 0);
      break;
    case (0):
      clr = Scalar(0, 0, 255);
      break;
    }
    circle(cv_ptr->image, Point(out.at<double>(i, 0), out.at<double>(i, 1)), 3, clr);
    line(cv_ptr->image, origin, cur, clr, 2);
  }
  circle(cv_ptr->image, Point(out.at<double>(2, 0), out.at<double>(2, 1)), 5, Scalar(255, 255, 255), -1);
}

void publish_camera_move(int y, int z)
{
  geometry_msgs::Twist msg;
  msg.angular.y = y;
  msg.angular.z = z;
  cam_move_pub.publish(msg);
}

// testing various callbacks

void attitudeCallback(const bebop_msgs::Ardrone3PilotingStateAttitudeChanged& msg) {
  rotX = -CV_PI / 2 + msg.pitch;
  rotY = msg.roll;
  rotZ = msg.yaw;
}

void speedCallback(const bebop_msgs::Ardrone3PilotingStateSpeedChanged& msg) {
  std::cout << "speed changed" << std::endl;
  std::cout << msg << std::endl;
}

void altitudeCallback(const bebop_msgs::Ardrone3PilotingStateAltitudeChanged& msg) {
  // 5Hz, posts timestamps, altitude only changes when flying
  //cout << "altitude callback" << endl;
  //cout << msg.altitude << endl;
}

void flatTrimCallback(const bebop_msgs::Ardrone3PilotingStateAltitudeChanged& msg) {
  // 5Hz, posts timestamps, altitude only changes when flying
  cout << "flat trim callback" << endl;
  cout << msg << endl;
}

void batteryCallback(const bebop_msgs::CommonCommonStateBatteryStateChanged& msg) {
  cout << msg << endl;
  battery = msg.percent;
}

void cameraPositionCallback(const bebop_msgs::Ardrone3CameraStateOrientation& msg) {
  //cout << "tilt: " << (int)msg.tilt << ", pan: " << (int)msg.pan << endl;
}

void shutdownNodes() {
  shutdown_pub.publish(std_msgs::Empty());
}

void regionCallback(const bebop_vicos::Region reg) {
  //cv::rectangle(cv_ptr->image, cv::Rect(x,y,h,w), cv::Scalar(255,0,0), 3);
  //cout << "reg" << endl;
  //cout << reg.x << endl;
  x = reg.x; y = reg.y; h = reg.h; w = reg.w;
}

void statusCallback(const bebop_vicos::Status status) {
  tracking = status.tracking;
  lost = status.lost;
}

void imageCallback(const sensor_msgs::ImageConstPtr& cam_msg) {
  cv_bridge::CvImagePtr cv_ptr;

  try {
    cv_ptr = cv_bridge::toCvCopy(cam_msg, sensor_msgs::image_encodings::BGR8);
  }
  catch (cv_bridge::Exception& e) {
    ROS_ERROR("cv_bridge exception: %s", e.what());
    return;
  }

  if (h != 0 && w != 0) {
    //cv::Point statePt = KF_update(x, y, h, w); // updates KF
    // draws tracker region
    if (!lost) {
      cv::rectangle(cv_ptr->image, cv::Rect(x, y, w, h), cv::Scalar(255, 0, 0), 3);
    }
    else {
      cv::rectangle(cv_ptr->image, cv::Rect(x, y, w, h), cv::Scalar(0, 0, 255), 3);
    }


    /*if (trajectory) {
      handle_queue(&v, cv::Point(x + w / 2, y + h / 2), cv_ptr, cv::Scalar(0, 0, 255)); // trajectory
      handle_queue(&v_kf, statePt, cv_ptr, cv::Scalar(0, 255, 0));
    }*/

    h = 0; w = 0;
  }

  //publish_camera_move(0,0);


  frame = *cam_msg;

  gui::writeStatusText(cv_ptr, tracking, lost);

  if (battery != -1)
    gui::drawBattery(cv_ptr, battery);

  drawAngles(cv_ptr);

  // draws selection rectangle
  cv::rectangle(cv_ptr->image, sel, cv::Scalar(0, 0, 0), 2);

  cv::imshow(OPENCV_WINDOW, cv_ptr->image);

  int cam_scale = 7;

  int key = cv::waitKey(1);
  //cout << key << endl;
  switch (key) {
  case ((int)('q')): // if 'q' is pressed, stop tracker
    stop_pub.publish(std_msgs::Empty());
    tracking = false;
    lost = false;
    break;
  case ((int)('w')):
    // TODO introduce limits so it doesn't stick
    cam_y += cam_scale;
    publish_camera_move(cam_y, cam_z);
    break;
  case ((int)('s')):
    cam_y -= cam_scale;
    publish_camera_move(cam_y, cam_z);
    break;
  case ((int)('a')):
    cam_z -= cam_scale;
    publish_camera_move(cam_y, cam_z);
    break;
  case ((int)('d')):
    cam_z += cam_scale;
    publish_camera_move(cam_y, cam_z);
    break;
  case (27): // 'esc' triggers shutdown
    shutdownNodes();
    ros::shutdown();
    break;
  }
}

void onMouse( int event, int x, int y, int f, void* ) {

  if (clicked && !tracking) {
    if (P1.x > P2.x) {
      sel.x = P2.x;
      sel.width = P1.x - P2.x;
    }
    else {
      sel.x = P1.x;
      sel.width = P2.x - P1.x;
    }

    if (P1.y > P2.y) {
      sel.y = P2.y;
      sel.height = P1.y - P2.y;
    }
    else {
      sel.y = P1.y;
      sel.height = P2.y - P1.y;
    }

  }
  switch (event) {
  case CV_EVENT_LBUTTONDOWN:
    clicked = true;
    P1.x = x;
    P1.y = y;
    P2.x = x;
    P2.y = y;
    break;
  case CV_EVENT_LBUTTONUP:
    P2.x = x;
    P2.y = y;
    // init tracker
    if (sel.area() > 0 && !tracking) {
      // send region and image
      tracking = true;
      bebop_vicos::Im_reg msg = bebop_vicos::Im_reg();
      msg.image = frame;
      bebop_vicos::Region r = bebop_vicos::Region();
      r.x = sel.x; r.y = sel.y; r.w = sel.width; r.h = sel.height;
      msg.region = r;
      im_reg_pub.publish(msg);
    }
    sel = cv::Rect(0, 0, 0, 0);
    clicked = false;
    break;
  case CV_EVENT_MOUSEMOVE:
    if (clicked) {
      P2.x = x;
      P2.y = y;
    }
    break;
  default: break;
  }
}

int main(int argc, char **argv)
{

  ros::init(argc, argv, "master");

  ros::NodeHandle n("~");
  ros::Subscriber sub_image = n.subscribe("/bebop/image_raw", 1, imageCallback);
  ros::Subscriber sub_attitude = n.subscribe("/bebop/states/ARDrone3/PilotingState/AttitudeChanged", 1, attitudeCallback);
  ros::Subscriber sub_speed = n.subscribe("/bebop/states/ARDrone3/PilotingState/SpeedChanged", 1, speedCallback);
  ros::Subscriber sub_altitude = n.subscribe("/bebop/states/ARDrone3/PilotingState/AltitudeChanged", 1, altitudeCallback);
  ros::Subscriber sub_flat_trim = n.subscribe("/bebop/states/ARDrone3/PilotingState/FlatTrimChanged", 1, flatTrimCallback);
  ros::Subscriber sub_bat = n.subscribe("/bebop/states/common/CommonState/BatteryStateChanged", 1, batteryCallback);
  ros::Subscriber sub_region = n.subscribe("/tracker/region", 1, regionCallback);
  ros::Subscriber sub_cam_pos = n.subscribe("/bebop/states/ARDrone3/CameraState/Orientation", 1, cameraPositionCallback);
  ros::Subscriber sub_status = n.subscribe("/bebop_vicos/status", 1, statusCallback);
  /*ros::Subscriber sub_status = n.subscribe("/bebop_vicos/status", 1, statusCallback);
  ros::Subscriber sub_util = n.subscribe("/ardrone/util", 1, utilCallback);
  ros::Subscriber sub_navdata = n.subscribe("/ardrone/navdata", 1, navdataCallback);*/


  cam_move_pub = n.advertise<geometry_msgs::Twist>("/bebop/camera_control", 1000);
  im_reg_pub = n.advertise<bebop_vicos::Im_reg>("/tracker/init", 1000);
  shutdown_pub = n.advertise<std_msgs::Empty>("/master/shutdown", 1000);
  stop_pub = n.advertise<std_msgs::Empty>("/tracker/stop", 1000);

  cv::namedWindow(OPENCV_WINDOW, CV_WINDOW_AUTOSIZE);

  setMouseCallback(OPENCV_WINDOW, onMouse, NULL);

  ros::spin();

  return 0;
}