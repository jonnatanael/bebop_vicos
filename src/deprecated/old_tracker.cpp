// includes for ROS
#include "ros/ros.h"
#include <cv_bridge/cv_bridge.h>
#include <opencv2/opencv.hpp>
#include <sensor_msgs/image_encodings.h>
#include <std_msgs/Empty.h>
#include <bebop_vicos/Region.h>
#include <bebop_vicos/Tracker_bundle.h>
#include <bebop_vicos/Im_reg.h>
#include <bebop_vicos/Status.h>

//includes for tracker
#include "tracker.h"

static const std::string OPENCV_WINDOW = "Tracker window";

double lost_threshold = 0.1;
int cam_h = 368;
int cam_w = 640;

// za označevanje objekta z miško
bool tracking = false;
bool lost = false; // for tracker failure
bool redetect = false; // whether the detector is used or not

ros::Publisher region_pub;
ros::Publisher bundle_pub;
ros::Publisher status_pub;

// tracker object and config
Ptr<legit::tracker::Tracker> t;
Config config;

// tracker config file
string par;

void stopCallback(std_msgs::Empty request) {
  tracking = false;
}

void shutdownCallback(std_msgs::Empty request) {
  ros::shutdown();
}

void initCallback(bebop_vicos::Im_reg msg) {
  tracking = true;

  cv_bridge::CvImagePtr cv_ptr;
  try {
    cv_ptr = cv_bridge::toCvCopy(msg.image,sensor_msgs::image_encodings::RGB8);
  }
  catch (cv_bridge::Exception& e) {
    ROS_ERROR("cv_bridge exception: %s", e.what());
    return;
  }

  legit::common::Image im = legit::common::Image(cv_ptr->image);
  cv::Rect reg = cv::Rect(msg.region.x, msg.region.y, msg.region.w, msg.region.h);

  t->initialize(im, reg);

  lost = false;
}


void get_glob_mask(cv::Mat &region, cv::Mat &response, cv::Mat &mask_fg) {
  // get segmentation mask from tracker and pass it to detector
  cv::Point2f pos = cv::Point2f(region.at<float>(0, 0), region.at<float>(0, 1));
  int w = region.at<float>(0, 2);
  int h = region.at<float>(0, 3);

  int x_start = pos.y - floor(w / 2);
  int y_start = pos.x - floor(h / 2);

  int offX1 = 0;
  int offY1 = 0;

  if (y_start < 0) {
    offY1 = -y_start;
    y_start = 0;
  }
  if (y_start + h >= cam_h) {
    h = cam_h - y_start - 1;
  }
  h -= offY1;
  if (x_start < 0) {
    offX1 = -x_start;
    x_start = 0;
  }
  if (x_start + w >= cam_w) {
    w = cam_w - x_start - 1;

  }
  w -= offX1;

  double minn, maxx;
  cv::minMaxLoc(response, &minn, &maxx);
  response.convertTo(response, 0, 255 / (maxx - minn), - minn);
  response(cv::Rect(offX1, offY1, w, h)).copyTo(mask_fg(cv::Rect(x_start, y_start, w, h)));
}

double getPSR(cv::Mat corr) {
  // calculates PSR as described in MOSSE paper (3.5)
  // http://www.cs.colostate.edu/~vision/publications/bolme_cvpr10.pdf
  Point max;
  cv::minMaxLoc(corr, NULL, NULL, NULL, &max);

  int size = 5;
  int height = 2 * size + 1;
  int width = 2 * size + 1;

  Rect r = Rect(max.x - size, max.y - size, width, width);

  float peak = corr.at<float>(max);
  // if peak region should go over the image edge, reduce size
  if (max.x + size >= corr.cols) {
    width = corr.cols - max.x;
  }
  if (max.y + size >= corr.rows) {
    height = corr.rows - max.y;
  }
  Mat m = Mat(height, width, CV_32F, Scalar(0));
  m.copyTo(corr(Rect(max.x - size, max.y - size, width, height)));

  Scalar mean, stddev;
  meanStdDev(corr, mean, stddev);

  double psr = (peak - mean.val[0]) / stddev.val[0];
  return psr;

}

void trackerCallback(const sensor_msgs::ImageConstPtr& cam_msg) {
  int x, y, w, h;
  cv_bridge::CvImagePtr cv_ptr;

  try {
    cv_ptr = cv_bridge::toCvCopy(cam_msg, sensor_msgs::image_encodings::BGR8);
  }
  catch (cv_bridge::Exception& e) {
    ROS_ERROR("cv_bridge exception: %s", e.what());
    return;
  }

  legit::common::Image im = legit::common::Image(cv_ptr->image);
  cv::Rect reg;

  if (tracking) {
    if (!lost) {
      t->update(im); // update tracker
      reg = t->region();
      x = reg.x; y = reg.y; h = reg.height; w = reg.width;

      cv::Mat corr = t->get_mat_property(1);
      double psr = getPSR(corr); // Peak to Sidelobe Ratio
      if (psr < 7) {
        lost = true;
      }
    }
    else {
      if (!redetect) {
        // continue tracking even if lost, when not redetecting
        t->update(im); // update tracker
        reg = t->region();
        x = reg.x; y = reg.y; h = reg.height; w = reg.width;
      }
    }

    bebop_vicos::Tracker_bundle bundle = bebop_vicos::Tracker_bundle();
    bebop_vicos::Region r = bebop_vicos::Region();
    r.x = x; r.y = y; r.h = h; r.w = w;
    r.stamp = cam_msg->header.stamp;
    bundle.image = *cam_msg;

    if (!lost) {
      region_pub.publish(r);
      bundle.region = r;

      cv::Mat seg_resp, seg_reg, segmentation;
      seg_resp = t->get_mat_property(4);
      seg_reg = t->get_mat_property(3);

      segmentation = cv::Mat(cv_ptr->image.rows, cv_ptr->image.cols, CV_8UC(1));
      segmentation.setTo(Scalar(0));

      get_glob_mask(seg_reg, seg_resp, segmentation);

      cv_bridge::CvImage out_msg;
      out_msg.header   = std_msgs::Header();
      out_msg.encoding = sensor_msgs::image_encodings::MONO8;
      out_msg.image    = segmentation;

      // transform OpenCV image back to ROS compatible message
      bundle.segmentation = *(out_msg.toImageMsg());
    }
    else {
      // if tracker is lost, send empty region
      bundle.region = bebop_vicos::Region();
      // if not redetecting, send region anyway
      if (!redetect) {
        region_pub.publish(r);
      }
    }
    if (redetect) {
      bundle_pub.publish(bundle);
    }
  }
  else {
    lost = false;
  }

  // construct and send status (i.e. timestamp, tracking and lost)
  bebop_vicos::Status status = bebop_vicos::Status();
  status.tracking = tracking;
  status.lost = lost;
  status.stamp = cam_msg->header.stamp;
  status_pub.publish(status);
}

void handle_config() {
  // manually add parameters
  config.add("tracker", "ldp");
  config.add("nbins", 32);
  config.add("root_config", par);
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "tracker");

  ros::NodeHandle n("~");
  ros::Subscriber sub_image = n.subscribe("/bebop/image_raw", 1, trackerCallback);
  ros::Subscriber sub_im_reg = n.subscribe("/tracker/init", 1, initCallback);
  ros::Subscriber sub_shutdown = n.subscribe("/master/shutdown", 1, shutdownCallback);
  ros::Subscriber sub_stop = n.subscribe("/tracker/stop", 1, stopCallback);
  ros::Subscriber sub_reinit = n.subscribe("/tracker/reinit", 1, initCallback);

  region_pub = n.advertise<bebop_vicos::Region>("/tracker/region", 1000);
  bundle_pub = n.advertise<bebop_vicos::Tracker_bundle>("/tracker/bundle", 1000);
  status_pub = n.advertise<bebop_vicos::Status>("/bebop_vicos/status", 1000);

  n.getParam("cfg", par);
  n.getParam("threshold", lost_threshold);
  n.getParam("redetect", redetect);
  handle_config();

  t = legit::tracker::create_tracker("ldp", config, "default");

  ros::spin();

  return 0;
}