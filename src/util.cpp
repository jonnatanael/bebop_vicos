#include "util.h"

std::string util::getImgType(int imgTypeInt) {

    //get image type
    //usage: cout << getImgType(cv_ptr->image.type()) << endl;

    int numImgTypes = 35; // 7 base types, with five channel options each (none or C1, ..., C4)

    int enum_ints[] =       {CV_8U,  CV_8UC1,  CV_8UC2,  CV_8UC3,  CV_8UC4,
                             CV_8S,  CV_8SC1,  CV_8SC2,  CV_8SC3,  CV_8SC4,
                             CV_16U, CV_16UC1, CV_16UC2, CV_16UC3, CV_16UC4,
                             CV_16S, CV_16SC1, CV_16SC2, CV_16SC3, CV_16SC4,
                             CV_32S, CV_32SC1, CV_32SC2, CV_32SC3, CV_32SC4,
                             CV_32F, CV_32FC1, CV_32FC2, CV_32FC3, CV_32FC4,
                             CV_64F, CV_64FC1, CV_64FC2, CV_64FC3, CV_64FC4
                            };

    std::string enum_strings[] = {"CV_8U",  "CV_8UC1",  "CV_8UC2",  "CV_8UC3",  "CV_8UC4",
                                  "CV_8S",  "CV_8SC1",  "CV_8SC2",  "CV_8SC3",  "CV_8SC4",
                                  "CV_16U", "CV_16UC1", "CV_16UC2", "CV_16UC3", "CV_16UC4",
                                  "CV_16S", "CV_16SC1", "CV_16SC2", "CV_16SC3", "CV_16SC4",
                                  "CV_32S", "CV_32SC1", "CV_32SC2", "CV_32SC3", "CV_32SC4",
                                  "CV_32F", "CV_32FC1", "CV_32FC2", "CV_32FC3", "CV_32FC4",
                                  "CV_64F", "CV_64FC1", "CV_64FC2", "CV_64FC3", "CV_64FC4"
                                 };

    for (int i = 0; i < numImgTypes; i++)
    {
        if (imgTypeInt == enum_ints[i]) return enum_strings[i];
    }
    return "unknown image type";
}

std::vector<std::string> &util::split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

std::vector<std::string> util::split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}

void util::printVector(const std::vector<std::string> v) {
    for (std::vector<std::string>::const_iterator i = v.begin(); i != v.end(); ++i)
        std::cout << *i << std::endl;
    std::cout << std::endl;
}

void util::init_KF(cv::KalmanFilter* KF, cv::Mat_<float>* measurement, int cam_w, int cam_h) {
    KF->transitionMatrix = *(cv::Mat_<float>(4, 4) << 1, 0, 2, 0,   0, 1, 0, 2,  0, 0, 1, 0,  0, 0, 0, 1);
    measurement->setTo(cv::Scalar(0));
    KF->statePre.at<float>(0) = cam_w / 2;
    KF->statePre.at<float>(1) = cam_h / 2;
    KF->statePre.at<float>(2) = 0;
    KF->statePre.at<float>(3) = 0;
    setIdentity(KF->measurementMatrix);
    setIdentity(KF->processNoiseCov, cv::Scalar::all(1e-4));
    setIdentity(KF->measurementNoiseCov, cv::Scalar::all(1e-1));
    setIdentity(KF->errorCovPost, cv::Scalar::all(.1));
}

cv::Point util::update_KF(cv::KalmanFilter* KF, cv::Mat_<float>* measurement, int x, int y, int h, int w) {
    cv::Mat prediction = KF->predict();
    cv::Point predictPt(prediction.at<float>(0), prediction.at<float>(1));
    //*measurement(0) = x + w / 2;
    measurement->at<float>(0) = x + w / 2;
    measurement->at<float>(1) = y + h / 2;
    //*measurement(1) = y + h / 2;
    cv::Mat estimated = KF->correct(*measurement);
    //return predictPt;
    return cv::Point(estimated.at<float>(0), estimated.at<float>(1));
}

void util::toggle(bool& a) {
    a ? a = false : a = true;
}

double util::rad2deg(float input) {
    const double halfC = 180 / M_PI;
    return input * halfC;
}

double util::deg2rad(float input) {
    return ( input * M_PI ) / 180 ;
}