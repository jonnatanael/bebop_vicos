#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sstream>


#include <rosbag/bag.h>
#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <std_msgs/String.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>
#include <cv_bridge/cv_bridge.h>

using namespace cv;
using namespace std;

void split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss;
    ss.str(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
}

std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}

int main( int argc, char** argv ) {


	string path = "/home/jon/Desktop/rec/1477470963_498296865/";
	std::ifstream infile(path + "bebop_region.txt");

	string line;
	while (std::getline(infile, line))
	{
		cout << line << endl;
		vector<string> res = split(line, ' ');
		cout << res[0] << endl;
		vector<string> coordinates = split(res[1], ',');
		for (int i = 0; i < coordinates.size(); i++){
			cout << coordinates.at(i) << endl;
		}
	}

	return 0;
}