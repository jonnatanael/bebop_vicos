#include "hough.h"

// include for custom gui class
#include "gui.h"
#include "util.h"


using namespace std;
using namespace cv;

void publish_camera_move(int y, int z)
{
  geometry_msgs::Twist msg;
  msg.angular.y = y;
  msg.angular.z = z;
  cam_move_pub.publish(msg);
}


void handle_key(int key, uint32_t sec, uint32_t nsec) {
  switch (key) {
  case (27): // 'esc' triggers shutdown
    ros::shutdown();
    break;
  case ((int)('w')):
    if (cam_y + camera_shift_angle <= camera_tilt_limit_up)
      cam_y += camera_shift_angle;
    else
      cam_y = camera_tilt_limit_up;
    //cam_y += camera_shift_angle;
    publish_camera_move(cam_y, cam_z);
    break;
  case ((int)('s')):
    if (cam_y - camera_shift_angle >= camera_tilt_limit_down)
      cam_y -= camera_shift_angle;
    else
      cam_y = camera_tilt_limit_down;
    publish_camera_move(cam_y, cam_z);
    break;
  case ((int)('a')):
    if (cam_z - camera_shift_angle >= -camera_pan_limit)
      cam_z -= camera_shift_angle;
    else
      cam_z = -camera_pan_limit;
    publish_camera_move(cam_y, cam_z);
    break;
  case ((int)('d')):
    if (cam_z + camera_shift_angle <= camera_pan_limit)
      cam_z += camera_shift_angle;
    else
      cam_z = camera_pan_limit;
    publish_camera_move(cam_y, cam_z);
    break;
  }
}

void imageCallback(const sensor_msgs::ImageConstPtr& cam_msg) {
  cv_bridge::CvImagePtr cv_ptr;

  try {
    cv_ptr = cv_bridge::toCvCopy(cam_msg, sensor_msgs::image_encodings::BGR8);
  }
  catch (cv_bridge::Exception& e) {
    ROS_ERROR("cv_bridge exception: %s", e.what());
    return;
  }

  Mat canny, src_gray;
  
  Canny( cv_ptr->image, canny, 50, 200, 3 );

  // circle detection
  /*cvtColor( cv_ptr->image, src_gray, CV_BGR2GRAY );
  GaussianBlur( src_gray, src_gray, Size(9, 9), 2, 2 );
  vector<Vec3f> circles;
  HoughCircles( src_gray, circles, CV_HOUGH_GRADIENT, 1, 1, 50, 100, 0, 0 );
  std::cout << circles.size() << std::endl;
  for ( size_t i = 0; i < circles.size(); i++ )
  {
    Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
    int radius = cvRound(circles[i][2]);
    // circle center
    circle( cv_ptr->image, center, 3, Scalar(0, 255, 0), -1, 8, 0 );
    // circle outline
    circle( cv_ptr->image, center, radius, Scalar(0, 0, 255), 3, 8, 0 );
  }*/

  if (prob) {
    vector<Vec4i> lines;
    HoughLinesP(canny, lines, rhoRes, thetaRes, 50, minLen, maxGap);
    for (size_t i = 0; i < lines.size(); i++) {
      Point pt1, pt2;
      pt1.x = lines[i][0];
      pt1.y = lines[i][1];
      pt2.x = lines[i][2];
      pt2.y = lines[i][3];
      line(cv_ptr->image, pt1, pt2, Scalar(0, 0, 255), 2, CV_AA);
    }
  }
  else {
    vector<Vec2f> lines;
    HoughLines(canny, lines, rhoRes, thetaRes, 100);
    for (size_t i = 0; i < lines.size(); i++) {
      float rho = lines[i][0], theta = lines[i][1];
      Point pt1, pt2;
      double a = cos(theta), b = sin(theta);
      double x0 = a * rho, y0 = b * rho;
      pt1.x = cvRound(x0 + 1000 * (-b));
      pt1.y = cvRound(y0 + 1000 * (a));
      pt2.x = cvRound(x0 - 1000 * (-b));
      pt2.y = cvRound(y0 - 1000 * (a));
      line( cv_ptr->image, pt1, pt2, Scalar(0, 0, 255), 1, CV_AA);
    }
  }


  cv::imshow(OPENCV_WINDOW, cv_ptr->image);

  handle_key(cv::waitKey(1), cam_msg->header.stamp.sec, cam_msg->header.stamp.nsec); // needs sec and nsec for creating record directories

}

const int alpha_slider_max = 100;

void on_trackbar( int, void* )
{

}



int main(int argc, char **argv)
{

  ros::init(argc, argv, "hough");

  ros::NodeHandle n("~");

  sub_image = n.subscribe("/bebop/image_raw", 1, imageCallback);
  cam_move_pub = n.advertise<geometry_msgs::Twist>("/bebop/camera_control", 1000);

  cv::namedWindow(OPENCV_WINDOW, CV_WINDOW_AUTOSIZE);

  //setMouseCallback(OPENCV_WINDOW, onMouse, NULL);

  /// Create Trackbars

  createTrackbar("Probabilistic", OPENCV_WINDOW, &prob, 1);
  createTrackbar("minLen", OPENCV_WINDOW, &minLen, 100);
  createTrackbar("maxgap", OPENCV_WINDOW, &maxGap, 100);

  /// Show some stuff
  //on_trackbar( prob, 0 );

  ros::spin();

  return 0;
}