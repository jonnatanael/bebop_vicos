#include <feature_nodelet.h>
#include <pluginlib/class_list_macros.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/tracking.hpp>
//#include <opencv2/nonfree/features2d.hpp>
#include <sensor_msgs/image_encodings.h>
#include <bebop_vicos/Point2D.h>
#include <bebop_vicos/PointVector.h>
#include <bebop_vicos/KeyPoint.h>
#include <bebop_vicos/KPVector.h>
#include "util.h"


PLUGINLIB_EXPORT_CLASS(bebop_vicos::feature_nodelet, nodelet::Nodelet);

using namespace cv;
using namespace std;

namespace bebop_vicos
{
void feature_nodelet::onInit()
{
	ros::NodeHandle& nh = getNodeHandle();
	ros::NodeHandle& private_nh = getPrivateNodeHandle();

	sub_image = nh.subscribe("/bebop/image_raw", 1, &feature_nodelet::imageCallback, this);
	sub_shutdown = nh.subscribe("/master/shutdown", 1, &feature_nodelet::shutdownCallback, this);

	pub_feat = nh.advertise<bebop_vicos::PointVector>("/bebop/features", 1000);
	pub_KP = nh.advertise<bebop_vicos::KPVector>("/bebop/KP", 1000);

	private_nh.getParam("n_feat", n_feat);
	private_nh.getParam("threshold", threshold);
	private_nh.getParam("method", method);

	NODELET_INFO("Initializing nodelet finished...");
	return;
}

void feature_nodelet::shutdownCallback(std_msgs::Empty request) {
	cout << "nodelet shutdown" << endl;
	ros::shutdown();
}

void feature_nodelet::publishFeatures(){
	bebop_vicos::PointVector pv = bebop_vicos::PointVector();

	// push feature points to vector
	for (std::vector<cv::Point2f>::const_iterator i = features.begin(); i != features.end(); ++i){
		cv::Point2f cur = *i;
		bebop_vicos::Point2D curPt = bebop_vicos::Point2D();
		curPt.x = cur.x;
		curPt.y = cur.y;
		pv.v.push_back(curPt);
	}

	pub_feat.publish(pv);
}

void feature_nodelet::publishKP(){
	bebop_vicos::KPVector pv = bebop_vicos::KPVector();

	// push feature points to vector
	for (std::vector<cv::KeyPoint>::const_iterator i = featuresKP.begin(); i != featuresKP.end(); ++i){
		cv::KeyPoint cur = *i;
		bebop_vicos::KeyPoint p = bebop_vicos::KeyPoint();
		bebop_vicos::Point2D curPt = bebop_vicos::Point2D();
		curPt.x = cur.pt.x;curPt.y = cur.pt.y;p.pt = curPt;

		p.size = cur.size;
		p.angle = cur.angle;
		//std::cout << cur.angle << std::endl;
		p.response = cur.response;
		p.octave = cur.octave;
		p.class_id = cur.class_id;

		pv.v.push_back(p);
	}

	pub_KP.publish(pv);
}

void feature_nodelet::imageCallback(const sensor_msgs::ImageConstPtr& cam_msg) {
	cv_bridge::CvImagePtr cv_ptr;

	try {
		cv_ptr = cv_bridge::toCvCopy(cam_msg, sensor_msgs::image_encodings::BGR8);
	}
	catch (cv_bridge::Exception& e) {
		ROS_ERROR("cv_bridge exception: %s", e.what());
		return;
	}

	cv::Mat greyMat;
	cv::cvtColor(cv_ptr->image, greyMat, CV_BGR2GRAY);

	switch (method){
		case 0:{
			cv::goodFeaturesToTrack(greyMat, features, n_feat, 0.007, 7);
			publishFeatures();
			break;
		}
		case 1:{
			cv::FAST(greyMat, featuresKP, threshold);
			publishKP();
			break;
		}
	}

	/*std::vector<cv::KeyPoint> v;
	cv::FAST(greyMat,v,100);

	for (std::vector<cv::KeyPoint>::const_iterator i = v.begin(); i != v.end(); ++i){
		cv::KeyPoint cur = *i;
    	std::cout << cur.pt << std::endl;
	}
  	std::cout << std::endl;*/


	
}

}