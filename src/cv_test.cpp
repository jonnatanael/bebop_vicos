#include <stdio.h>
#include <iostream>
#include <opencv2/opencv.hpp>
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"

using namespace cv;
using namespace std;

void readme();

string getImgType(int imgTypeInt) {

	//get image type
	//usage: cout << getImgType(cv_ptr->image.type()) << endl;

	int numImgTypes = 35; // 7 base types, with five channel options each (none or C1, ..., C4)

	int enum_ints[] =       {CV_8U,  CV_8UC1,  CV_8UC2,  CV_8UC3,  CV_8UC4,
	                         CV_8S,  CV_8SC1,  CV_8SC2,  CV_8SC3,  CV_8SC4,
	                         CV_16U, CV_16UC1, CV_16UC2, CV_16UC3, CV_16UC4,
	                         CV_16S, CV_16SC1, CV_16SC2, CV_16SC3, CV_16SC4,
	                         CV_32S, CV_32SC1, CV_32SC2, CV_32SC3, CV_32SC4,
	                         CV_32F, CV_32FC1, CV_32FC2, CV_32FC3, CV_32FC4,
	                         CV_64F, CV_64FC1, CV_64FC2, CV_64FC3, CV_64FC4
	                        };

	std::string enum_strings[] = {"CV_8U",  "CV_8UC1",  "CV_8UC2",  "CV_8UC3",  "CV_8UC4",
	                              "CV_8S",  "CV_8SC1",  "CV_8SC2",  "CV_8SC3",  "CV_8SC4",
	                              "CV_16U", "CV_16UC1", "CV_16UC2", "CV_16UC3", "CV_16UC4",
	                              "CV_16S", "CV_16SC1", "CV_16SC2", "CV_16SC3", "CV_16SC4",
	                              "CV_32S", "CV_32SC1", "CV_32SC2", "CV_32SC3", "CV_32SC4",
	                              "CV_32F", "CV_32FC1", "CV_32FC2", "CV_32FC3", "CV_32FC4",
	                              "CV_64F", "CV_64FC1", "CV_64FC2", "CV_64FC3", "CV_64FC4"
	                             };

	for (int i = 0; i < numImgTypes; i++)
	{
		if (imgTypeInt == enum_ints[i]) return enum_strings[i];
	}
	return "unknown image type";
}

/** @function main */
int main( int argc, char** argv )
{
	//Mat im = imread("/home/jon/Desktop/cv/a.jpg");
	//Mat im2;

	int h = 480;
	int w = 640;
	int key;
	double step = CV_PI / 12;

	double rx, ry, rz, tz;
	rx = ry = rz = 0;
	/*rz = cos(CV_PI/2);
	rx = 0.2;
	ry = 1.9;
	rz = 2.7;
	tz = -5;*/
	rx = -CV_PI/2;
	rz = CV_PI/2;
	tz = -5;

	Mat im = Mat(480, 640, CV_8UC3, Scalar(0, 0, 0));


	Mat cord = (Mat_<double>(4, 3) << 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1);
	Mat cord_;

	Mat tri = (Mat_<double>(3, 3) << 0.2, 0.2, 0.2, 0.5, 0.5, 0.8, 0.8, 0.8, 0.2);
	Mat tri_;

	Mat cord2 = (Mat_<double>(4, 3) << 0, 0, 0, -1, 0, 0, 0, -1, 0, 0, 0, -1);
	Mat cord2_;

	Mat in, rot, trans, cam, out;
	Mat Rx, Ry, Rz;

	cam = (Mat_<double>(3, 3) << w, 0, w / 2, 0, h, h / 2, 0, 0, 1);
	trans = (Mat_<double>(3, 1) << 0, 0, tz);
	rot = (Mat_<double>(3, 1) << 0, 0, 0);

	Point origin, cur;
	Scalar clr(255, 255, 255);

	while (true) {
		im = Mat(480, 640, CV_8UC3, Scalar(0, 0, 0));

		Rx = (Mat_<double>(3, 3) << 1, 0, 0, 0, cos(rx), -sin(rx), 0, sin(rx), cos(rx));
		Ry = (Mat_<double>(3, 3) << cos(ry), 0, sin(ry), 0, 1, 0, -sin(ry), 0, cos(ry));
		Rz = (Mat_<double>(3, 3) << cos(rz), -sin(rz), 0, sin(rz), cos(rz), 0, 0, 0, 1);
		trans = (Mat_<double>(3, 1) << 0, 0, tz);

		cord_ = Rx * Ry * Rz * cord.t();
		cord_ = cord_.t();
		projectPoints(cord_, rot, trans, cam, Mat(), out);
		origin = Point(out.at<double>(0, 0), out.at<double>(0, 1));

		for (int i = 1; i < out.rows; i++) {
			// draws coordinate system
			cur = Point(out.at<double>(i, 0), out.at<double>(i, 1));
			switch (i) {
			case (1):
				clr = Scalar(255, 0, 0);
				break;
			case (2):
				clr = Scalar(0, 255, 0);
				break;
			case (3):
				clr = Scalar(0, 0, 255);
				break;
			}

			circle(im, Point(out.at<double>(i, 0), out.at<double>(i, 1)), 3, clr);
			line(im, origin, cur, clr, 2);
		}

		cord2_ = Rx * Ry * Rz * cord2.t();
		cord2_ = cord2_.t();
		projectPoints(cord2_, rot, trans, cam, Mat(), out);
		origin = Point(out.at<double>(0, 0), out.at<double>(0, 1));

		for (int i = 1; i < out.rows; i++) {
			// draws coordinate system
			cur = Point(out.at<double>(i, 0), out.at<double>(i, 1));
			switch (i) {
			case (1):
				clr = Scalar(255, 0, 0);
				break;
			case (2):
				clr = Scalar(0, 255, 0);
				break;
			case (3):
				clr = Scalar(0, 0, 255);
				break;
			}

			circle(im, Point(out.at<double>(i, 0), out.at<double>(i, 1)), 3, clr);
			line(im, origin, cur, clr, 2);
		}

		tri_ = Rx * Ry * Rz * tri.t();
		tri_ = tri_.t();
		projectPoints(tri_, rot, trans, cam, Mat(), out);
		clr = Scalar(255, 0, 255);
		for (int i = 0; i < out.rows; i++) {
			circle(im, Point(out.at<double>(i, 0), out.at<double>(i, 1)), 3, clr);
		}
		imshow("Window", im);
		key = waitKey(1);

		switch (key) {
		case ((int)('q')):
			rx += step;
			break;
		case ((int)('w')):
			ry += step;
			break;
		case ((int)('e')):
			rz += step;
			break;
		case ((int)('a')):
			rx -= step;
			break;
		case ((int)('s')):
			ry -= step;
			break;
		case ((int)('d')):
			rz -= step;
			break;
		case (27):
			return 0;
			break;
		case (65362):
			tz += 0.1;
			break;
		case (65364):
			tz -= 0.1;
			break;
		}
	}

	return 0;
}

/** @function readme */
void readme()
{ std::cout << "" << std::endl; }