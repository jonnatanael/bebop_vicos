#include <tracker_nodelet.h>
#include <pluginlib/class_list_macros.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <sensor_msgs/image_encodings.h>
#include <std_msgs/Empty.h>
#include <bebop_vicos/Region.h>
#include <bebop_vicos/Tracker_bundle.h>
#include <bebop_vicos/Im_reg.h>
#include <bebop_vicos/Status.h>

PLUGINLIB_EXPORT_CLASS(bebop_vicos::tracker_nodelet, nodelet::Nodelet);

namespace bebop_vicos
{
void tracker_nodelet::onInit()
{
	ros::NodeHandle& nh = getNodeHandle();
	ros::NodeHandle& private_nh = getPrivateNodeHandle();

	private_nh.getParam("image_w", cam_w);
	private_nh.getParam("image_h", cam_h);

	private_nh.getParam("image_topic", image_topic);

	sub_image = nh.subscribe(image_topic, 1, &tracker_nodelet::trackerCallback, this);
	sub_im_reg = nh.subscribe("/tracker/init", 1, &tracker_nodelet::initCallback, this);
	sub_shutdown = nh.subscribe("/master/shutdown", 1, &tracker_nodelet::shutdownCallback, this);
	sub_stop = nh.subscribe("/tracker/stop", 1, &tracker_nodelet::stopCallback, this);
	sub_reinit = nh.subscribe("/tracker/reinit", 1, &tracker_nodelet::initCallback, this);
	sub_set_pos = nh.subscribe("/tracker/set", 1, &tracker_nodelet::setPosCallback, this);

	pub_region = nh.advertise<bebop_vicos::Region>("/tracker/region", 1000);
	pub_bundle = nh.advertise<bebop_vicos::Tracker_bundle>("/tracker/bundle", 1000);
	pub_status = nh.advertise<bebop_vicos::Status>("/bebop_vicos/status", 1000);

	tracking = false;
	lost = false;
	redetect = false;

	//cam_h = 368;
	//cam_w = 640;

	inited = false;
	tracker_created = false;

	private_nh.getParam("cfg", par);
	private_nh.getParam("threshold", lost_threshold);
	private_nh.getParam("redetect", redetect);
	private_nh.getParam("psr_count", psr_count);
	psr_count_backup = psr_count;
	handle_config();

	t = legit::tracker::create_tracker("ldp", config, "default");
	tracker_created = true;

	NODELET_INFO("Initializing nodelet finished...");
	return;
}

void tracker_nodelet::stopCallback(std_msgs::Empty request) {
	if (tracking) {
		cout << "tracker stopped" << endl;
		tracking = false;
	}
}

void tracker_nodelet::setPosCallback(geometry_msgs::Point msg) {
	//cout << msg << endl;
	t->set_pos(msg.y, msg.x);
}

void tracker_nodelet::shutdownCallback(std_msgs::Empty request) {
	cout << "tracker shutdown" << endl;
	ros::shutdown();
}

void tracker_nodelet::initCallback(bebop_vicos::Im_reg msg) {
	if (tracker_created) {
		cv_bridge::CvImagePtr cv_ptr;
		try {
			cv_ptr = cv_bridge::toCvCopy(msg.image, sensor_msgs::image_encodings::BGR8);
		}
		catch (cv_bridge::Exception& e) {
			NODELET_ERROR("cv_bridge exception: %s", e.what());
			return;
		}

		legit::common::Image im = legit::common::Image(cv_ptr->image);
		cv::Rect reg = cv::Rect(msg.region.x, msg.region.y, msg.region.w, msg.region.h);

		t->initialize(im, reg);

		lost = false;
		inited = true;
		tracking = true;
	}
	else {
		//cout << "tracker not yet created!" << endl;
	}

}

void tracker_nodelet::get_glob_mask(cv::Mat &region, cv::Mat &response, cv::Mat &mask_fg) {
	// get segmentation mask from tracker and pass it to detector
	cv::Point2f pos = cv::Point2f(region.at<float>(0, 0), region.at<float>(0, 1));
	int w = region.at<float>(0, 2);
	int h = region.at<float>(0, 3);

	int x_start = pos.y - floor(w / 2);
	int y_start = pos.x - floor(h / 2);

	int offX1 = 0;
	int offY1 = 0;

	if (y_start < 0) {
		offY1 = -y_start;
		y_start = 0;
	}
	if (y_start + h >= cam_h) {
		h = cam_h - y_start - 1;
	}
	h -= offY1;
	if (x_start < 0) {
		offX1 = -x_start;
		x_start = 0;
	}
	if (x_start + w >= cam_w) {
		w = cam_w - x_start - 1;

	}
	w -= offX1;

	double minn, maxx;
	cv::minMaxLoc(response, &minn, &maxx);
	response.convertTo(response, 0, 255 / (maxx - minn), - minn);
	response(cv::Rect(offX1, offY1, w, h)).copyTo(mask_fg(cv::Rect(x_start, y_start, w, h)));
}

double tracker_nodelet::getPSR(cv::Mat corr) {
	//cout << corr.size() << endl;
	// calculates PSR as described in MOSSE paper (3.5)
	// http://www.cs.colostate.edu/~vision/publications/bolme_cvpr10.pdf

	Point max;
	cv::minMaxLoc(corr, NULL, NULL, NULL, &max);

	int size = 5;
	int height = 2 * size + 1;
	int width = 2 * size + 1;

	Rect r = Rect(max.x - size, max.y - size, width, height);
	/*cout << max << endl;
	imshow("test", corr);
	waitKey(1);*/

	float peak = corr.at<float>(max);
	// if peak region should go over the image edge, reduce size
	if (max.x + width > corr.cols) {
		r.width = corr.cols - max.x;
	}
	if (max.y + height > corr.rows) {
		r.height = corr.rows - max.y;
	}
	if (max.x - size < 0) {
		r.x = 0;
	}
	if (max.y - size < 0) {
		r.y = 0;
	}

	// create zero matrix
	Mat m = Mat(r.height, r.width, CV_32F, Scalar(0));
	// copy it over response
	m.copyTo(corr(r));

	//cout << "m size: " << m.size() << endl;

	Scalar mean, stddev;
	meanStdDev(corr, mean, stddev);
	//cout << peak << endl;
	double psr = (peak - mean.val[0]) / stddev.val[0];
	//cout << psr << endl;
	return psr;

}

void tracker_nodelet::trackerCallback(const sensor_msgs::ImageConstPtr& cam_msg) {
	//cout << "trackerCallback" << endl;
	if (inited) {
		//cout << "tracker callback" << endl;

		int x, y, w, h, x_c, y_c;
		cv_bridge::CvImagePtr cv_ptr;

		try {
			cv_ptr = cv_bridge::toCvCopy(cam_msg, sensor_msgs::image_encodings::BGR8);
		}
		catch (cv_bridge::Exception& e) {
			NODELET_ERROR("cv_bridge exception: %s", e.what());
			return;
		}

		legit::common::Image im = legit::common::Image(cv_ptr->image);
		cv::Rect reg;
		cv::Mat corr;

		if (tracking) {
			if (!lost) {
				try {
					t->update(im); // update tracker
				}
				catch (Exception e) {
					NODELET_ERROR("something wrong with tracker update");
					return;
				}

				reg = t->region();
				x = reg.x; y = reg.y; h = reg.height; w = reg.width;
				x_c = x + w / 2;
				y_c = y + h / 2;

			}
			else {
				if (!redetect) {
					// continue tracking even if lost, when not redetecting
					t->update(im); // update tracker
					reg = t->region();
					x = reg.x; y = reg.y; h = reg.height; w = reg.width;
				}
				else {
					// perform redetection (or just wait for redetection)
					// stop updating
				}
			}
			corr = t->get_mat_property(1);

			if (!corr.empty()) {
				psr = getPSR(corr); // Peak to Sidelobe Ratio
				//cout << psr << endl;
				if (psr < lost_threshold) {
					//cout << "psr too low, count: " << psr_count << endl;
					//cout << psr_count << endl;
					if (psr_count == 0) {
						lost = true;
						cout << "LOST" << endl;
						psr_count = psr_count_backup;
					}
					else if (psr_prev < lost_threshold) {
						//cout << "psr too low" << endl;

						psr_count--;
					}
					else if (psr_prev > lost_threshold) {
						psr_count = psr_count_backup;
					}
				}
				psr_prev = psr;
			}
			
			if (lost) {
				//cout << "LOST" << endl;
			}


			//bebop_vicos::Tracker_bundle bundle = bebop_vicos::Tracker_bundle();
			bebop_vicos::Region r = bebop_vicos::Region();
			r.x = x; r.y = y; r.h = h; r.w = w; r.psr = psr;
			r.stamp = cam_msg->header.stamp;
			//bundle.image = *cam_msg;
			pub_region.publish(r);

			//if (!lost) {
				//if (pub_region)
				//pub_region.publish(r);
				//bundle.region = r;

				/*cv::Mat seg_resp, seg_reg, segmentation;
				seg_resp = t->get_mat_property(4);
				seg_reg = t->get_mat_property(3);

				segmentation = cv::Mat(cv_ptr->image.rows, cv_ptr->image.cols, CV_8UC(1));
				segmentation.setTo(Scalar(0));

				get_glob_mask(seg_reg, seg_resp, segmentation);

				cv_bridge::CvImage out_msg;
				out_msg.header   = std_msgs::Header();
				out_msg.encoding = sensor_msgs::image_encodings::MONO8;
				out_msg.image    = segmentation;

				// transform OpenCV image back to ROS compatible message
				bundle.segmentation = *(out_msg.toImageMsg());*/
			/*}
			else {
				// if tracker is lost, send empty region
				//bundle.region = bebop_vicos::Region();
				// if not redetecting, send region anyway
				if (!redetect) {
					if (pub_region)
						pub_region.publish(r);
				}
			}
			if (redetect) {
				//if (pub_bundle)
				//	pub_bundle.publish(bundle);
			}*/

			// boundary check
			if (x_c > cam_w || x_c < 0 || y_c > cam_h || y_c < 0) {
				//tracking = false;
				//lost = true;
				std_msgs::Empty msg;
				tracker_nodelet::stopCallback(msg);
			}
		}
		else {
			//lost = false;
		}

	}
	else {
		//cout << "not yet inited!" << endl;
	}

	// construct and send status (i.e. timestamp, tracking and lost)
	bebop_vicos::Status status = bebop_vicos::Status();
	status.tracking = tracking;
	status.lost = lost;
	status.stamp = cam_msg->header.stamp;
	if (pub_status)
		pub_status.publish(status);

}

void tracker_nodelet::handle_config() {
	// manually add parameters
	config.add("tracker", "ldp");
	config.add("nbins", 32);
	config.add("root_config", par);
}

}