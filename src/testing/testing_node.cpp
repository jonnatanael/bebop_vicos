#include <vector>
#include <fstream>
#include <string>
#include <boost/filesystem.hpp>

// includes for ROS
#include "ros/ros.h"
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/Image.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/Empty.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Int16.h>

#include <bebop_msgs/Ardrone3CameraStateOrientation.h>


ros::Publisher cam_move_pub;

ros::Subscriber sub_test;
ros::Subscriber sub_cam_pos;


// include for custom gui class
#include "gui.h"
#include "util.h"

// include for coordinate cross drawing
#include "coordinates.h"

using namespace std;
using namespace cv;

float cam_y = 1e5;
float cam_x = 1e5;
int camera_shift_angle = 7;
int camera_pan_limit = 35;
int camera_tilt_limit = 35;
bool camera_move_pending = false;

RNG rng;

void publish_camera_move(float x, float y) {
    geometry_msgs::Twist msg;
    msg.angular.y = y;
    msg.angular.z = x;

    cam_move_pub.publish(msg);
}

void cameraPositionCallback(const bebop_msgs::Ardrone3CameraStateOrientation& msg) {
    if (camera_move_pending) {
        if (cam_y == msg.tilt && cam_x == msg.pan) {
            camera_move_pending = false;
        }
    }
    else {
        cam_y = msg.tilt;
        cam_x = msg.pan;
    }
}

void move_square(int shift) {
    // camera_shift_angle
    int i = 0;
    int move_x, move_y;
    bool stop = false;
    //cout << cam_x << ", " << cam_y << endl;
    while (!stop) {
        switch (i) {
        case 0:
            cout << i << endl;
            move_y = std::min((int)cam_y + shift, camera_tilt_limit);
            cam_y = move_y;
            //cout << cam_x << ", " << cam_y << endl;
            camera_move_pending = true;
            publish_camera_move(cam_x, cam_y);
            break;
        case 1:
            cout << i << endl;
            move_x = std::max((int)cam_x - shift, -camera_pan_limit);
            cam_x = move_x;
            //cout << cam_x << ", " << cam_y << endl;
            camera_move_pending = true;
            publish_camera_move(cam_x, cam_y);
            break;
        case 2:
            cout << i << endl;
            move_y = std::max((int)cam_y - shift, -camera_tilt_limit);
            cam_y = move_y;
            //cout << cam_x << ", " << cam_y << endl;
            camera_move_pending = true;
            publish_camera_move(cam_x, cam_y);
            break;
        case 3:
            cout << i << endl;
            move_x = std::min((int)cam_x + shift, camera_pan_limit);
            cam_x = move_x;
            //cout << cam_x << ", " << cam_y << endl;
            camera_move_pending = true;
            publish_camera_move(cam_x, cam_y);
            break;
        default:
            stop = true;
        }
        i++;
        usleep( 100 * 1000 );
    }

}

void move_random(int range) {
    float rn = range*0.01f;
    //cout << range << endl;
    int mv_x = rng.uniform( -35 * rn, 35 * rn );
    int mv_y = rng.uniform( -35 * rn, 35 * rn );
    cout << mv_x << ", " << mv_y << endl;
    publish_camera_move(mv_x, mv_y);
}

void testCallback(const std_msgs::Int16& msg) {
    //move_square(msg.data);
    move_random(msg.data);
}

int main(int argc, char **argv)
{
    RNG rng( 0xFFFFFFFF );

    ros::init(argc, argv, "testing_node");

    ros::NodeHandle n("~");

    sub_test = n.subscribe("/bebop/init_test", 1, testCallback);
    sub_cam_pos = n.subscribe("/bebop/states/ardrone3/CameraState/Orientation", 1, cameraPositionCallback);

    cam_move_pub = n.advertise<geometry_msgs::Twist>("/bebop/camera_control", 1000);


    ros::spin();

    return 0;
}