#include <iostream>
#include "PID.h"

using namespace std;

PID::PID() {
	//cout << "PID controller created" << endl;
	Kp = 0.0;
	Ki = 0.0;
	Kd = 0.0;
	feedback = 0.0;
	reference = 0.0;
	output = 0.0;
	integrator = 0.0;
	lastError = 0.0;
	for (int i = 0; i < PID_D_FILTER_LENGTH; i++) errorHistory[i] = 0.0;
	started = false;
}

PID::~PID() {

}

void PID::reset() {
	integrator = 0.0;
	lastError = 0.0;
	for (int i = 0; i < PID_D_FILTER_LENGTH; i++) errorHistory[i] = 0.0;
	started = false;
}

double PID::getOutput() {

	// calculate error and derivative
	double error = reference - feedback;

	//cout << "ref: " << reference << ", feedback: " << feedback << endl;

	// nastavi seznam preteklih vrednosti napake
	for (int i = 0; i < PID_D_FILTER_LENGTH - 1; i++)
		errorHistoryI[i] = errorHistoryI[i + 1];
	errorHistoryI[PID_D_FILTER_LENGTH - 1] = error;


	// nastavi seznam preteklih diferenc napake
	for (int i = 0; i < PID_D_FILTER_LENGTH - 1; i++)
		errorHistory[i] = errorHistory[i + 1];
	errorHistory[PID_D_FILTER_LENGTH - 1] = error - lastError;
	lastError = error;

	double dError = 0.0;

	// low pass filter za K_d
	double weight = 2.0 / (PID_D_FILTER_LENGTH + 1);
	for (int i = 0; i < PID_D_FILTER_LENGTH; i++)
		dError += weight * errorHistory[PID_D_FILTER_LENGTH - i - 1];

	integrator = 0.0;
	// enostavna vsota vseh
	for (int i = 0; i < PID_D_FILTER_LENGTH; i++)
		integrator += errorHistoryI[PID_D_FILTER_LENGTH - i - 1];

	//if (PID_D_FILTER_LENGTH > 1)
	//	weight -= (2.0 / (PID_D_FILTER_LENGTH * (PID_D_FILTER_LENGTH + 1)));

	//cout << "integrator: " << integrator << endl;

	/*cout << "dError: " << dError << endl;
	cout << "integrator: " << integrator << endl;
	cout << "error: " << error << endl;*/
	//cout << "Kp: " << Kp << ", Ki: " << Ki << ", Kd: " << Kd << endl;

	output = Kp * error + Ki * integrator + Kd * dError;

	return output;
}

