#include "bebop_follower.h"
#include "util.h"

void shutdownCallback(std_msgs::Empty request) {
    ros::shutdown();
}

void FlyingStateCallback(const bebop_msgs::Ardrone3PilotingStateFlyingStateChanged& msg) {
    //cout << msg << endl;
    flying_state = msg.state;
}

void attitudeCallback(const bebop_msgs::Ardrone3PilotingStateAttitudeChanged& msg) {
    navdata_pitch = msg.pitch;
    navdata_roll = msg.roll;
    navdata_yaw = msg.yaw;
}

void setNav(bebop_vicos::Region reg) {
    // calculates navigation parameters from detection/tracking

    // get features
    double dx = (x_c - image_center_x) / (double)cam_w;
    double dy = (y_c - image_center_y) / (double)cam_h;
    double dd = area - area_ref;
    gamma_rad = util::deg2rad((float) gamma_angle);

    double pitch_corr, yaw_corr, roll_corr, z_corr; // corrections for control values
    double pitch, yaw, roll, z; // storage for actual control values

    float pan_err = pan_factor * (cam_x - cam_x_ref) / max_pan;
    float tilt_err = - tilt_factor * (cam_y - cam_y_ref) / max_tilt;

    int factor = 10; // to get correct ratio of size and y change, was calculated expreimentally
    double alpha = atan2(-dy, -dd * factor) + gamma_rad;

    if (pestana) {
        // Pestana implementation - non-stablized static camera - for experiments
        yaw_corr = dx;
        roll_corr = 0;
        z_corr = dy - (0 - navdata_pitch) / fov_y;
        pitch_corr = dd;
    }
    else {
        if (!target_angle_enabled) {

            pitch_corr = dd;
            yaw_corr = dx + pan_err;
            // don't acknowledge camera tilt error while not flying
            // so drone controller does not interfere with takeoff
            if (flying_state == 2 || flying_state == 3) {
                z_corr = dy + tilt_err;
            }
            else {
                z_corr = 0;
            }
            roll_corr = 0;

        }
        else {
            pitch_corr = dd;
            yaw_corr = dx + pan_err;
            z_corr = dy;
            roll_corr = 0;
        }
    }

    pid_pitch.setFeedback(pitch_corr);
    pid_roll.setFeedback(roll_corr);
    pid_yaw.setFeedback(yaw_corr);
    pid_z.setFeedback(z_corr);

    float pitch_output = pid_pitch.getOutput();
    float roll_output = pid_roll.getOutput();
    float yaw_output = pid_yaw.getOutput();
    float z_output = pid_z.getOutput();

    // if gamma > 0, set pitch and z according to formula
    if (target_angle_enabled) {
        pitch = abs(pitch_output) * cos(alpha);
        z = abs(z_output) * sin(alpha);
    }
    else {
        pitch = pitch_output;
        z = z_output;
    }

    roll = roll_output;
    yaw = yaw_output;

    // send data to master, so it can be saved to file
    bebop_vicos::drone_PID m = bebop_vicos::drone_PID();
    m.stamp = reg.stamp;
    m.roll = roll;
    m.pitch = pitch;
    m.yaw = yaw;
    m.z = z;
    m.flying_state = flying_state;
    m.dx = dx;
    m.dy = dy;
    m.dd = dd;
    m.alpha = alpha;
    drone_PID_pub.publish(m);

    // set and publish control message
    msg.linear.x = pitch;
    msg.linear.y = roll;
    msg.linear.z = z;
    msg.angular.z = yaw;

    cmd_pub.publish(msg);
}

void init_PID() {
    pid_pitch.setGains(pitch_Kp, pitch_Ki, pitch_Kd);
    pid_roll.setGains(roll_Kp, roll_Ki, roll_Kd);
    pid_yaw.setGains(yaw_Kp, yaw_Ki, yaw_Kd);
    pid_z.setGains(z_Kp, z_Ki, z_Kd);

    pid_pitch.setReference(d_ref);
    pid_roll.setReference(x_ref);
    pid_yaw.setReference(x_ref);
    pid_z.setReference(y_ref);
}

void regionCallback(bebop_vicos::Region reg) {
    setNav(reg);

    x = reg.x;
    y = reg.y;
    h = reg.h;
    w = reg.w;
    x_c = x + w / 2;
    y_c = y + h / 2;

    area = sqrt((double)(h * w) / (cam_w * cam_h));
}

void gammaCallback(std_msgs::Int16 msg) {
    gamma_angle = msg.data;
}

void cameraPositionCallback(const bebop_msgs::Ardrone3CameraStateOrientation& msg) {
    cam_y = msg.tilt;
    cam_x = msg.pan;
}

void trackerInitCallback(bebop_vicos::Im_reg msg) {
    area_ref = sqrt((double)(msg.region.h * msg.region.w) / (cam_w * cam_h));
}

void trackerStopCallback(std_msgs::Empty request) {
    //std::cout << "SHUTDOWN" << std::endl;
    //d_ref = -1;
    //std::cout << d_ref << std::endl;
}

void targetAngleCallback(std_msgs::Bool msg) {
    // sets state so controller knows where the camera is pointed
    target_angle_enabled = msg.data;
    if (target_angle_enabled)
        cout << "gamma enabled" << endl;
    else
        cout << "gamma disabled" << endl;
}

void get_params(ros::NodeHandle n) {
    // get PID gains from launch file
    n.getParam("pitch_Kp", pitch_Kp);
    n.getParam("pitch_Ki", pitch_Ki);
    n.getParam("pitch_Kd", pitch_Kd);

    n.getParam("roll_Kp", roll_Kp);
    n.getParam("roll_Ki", roll_Ki);
    n.getParam("roll_Kd", roll_Kd);

    n.getParam("yaw_Kp", yaw_Kp);
    n.getParam("yaw_Ki", yaw_Ki);
    n.getParam("yaw_Kd", yaw_Kd);

    n.getParam("z_Kp", z_Kp);
    n.getParam("z_Ki", z_Ki);
    n.getParam("z_Kd", z_Kd);

    n.getParam("pestana", pestana);

    n.getParam("pan_factor", pan_factor);
    n.getParam("tilt_factor", tilt_factor);
}

int main(int argc, char **argv) {

    ros::init(argc, argv, "bebop_follower");

    ros::NodeHandle n("~");

    ros::Subscriber sub_attitude = n.subscribe("/bebop/states/ARDrone3/PilotingState/AttitudeChanged", 1, attitudeCallback);
    ros::Subscriber sub_cam_pos = n.subscribe("/bebop/states/ardrone3/CameraState/Orientation", 1, cameraPositionCallback);
    ros::Subscriber sub_flying_state = n.subscribe("/bebop/states/ardrone3/PilotingState/FlyingStateChanged", 1, FlyingStateCallback);
    ros::Subscriber sub_shutdown = n.subscribe("/master/shutdown", 1, shutdownCallback);
    ros::Subscriber sub_stop = n.subscribe("/tracker/stop", 1, trackerStopCallback);
    ros::Subscriber sub_region = n.subscribe("/tracker/region", 1, regionCallback);    
    ros::Subscriber sub_im_reg = n.subscribe("/tracker/init", 1, trackerInitCallback);
    ros::Subscriber sub_target_angle_status = n.subscribe("/master/target_angle_status", 1, targetAngleCallback);
    ros::Subscriber sub_gamma = n.subscribe("/master/gamma", 1, gammaCallback);
    

    cmd_pub = n.advertise<geometry_msgs::Twist>("/bebop/cmd_vel", 1000);
    drone_PID_pub = n.advertise<bebop_vicos::drone_PID>("/follower/pid", 1000);

    get_params(n);

    init_PID();

    ros::spin();

    return 0;
}