#include "facetrack.h"

#include "gui.h"
#include "util.h"

using namespace std;
using namespace cv;

void publish_camera_move(int y, int z) {
  geometry_msgs::Twist msg;
  msg.angular.y = y;
  msg.angular.z = z;
  cam_move_pub.publish(msg);
}

void cameraPositionCallback(const bebop_msgs::Ardrone3CameraStateOrientation& msg) {
  //cout << "tilt: " << (int)msg.tilt << ", pan: " << (int)msg.pan << endl;
  cam_y = msg.tilt;
  cam_z = msg.pan;
}

void shutdownNodes() {
  shutdown_pub.publish(std_msgs::Empty());
}

int shift_function(int shift) {
  // a testing place for different weight functions for tracker-based camera shift.
  // seems a linear function is best
  float factor = 0.25;
  return shift * factor;
}

void regionCallback(const bebop_vicos::Region reg) {
  // saves current object region
  x = reg.x; y = reg.y; h = reg.h; w = reg.w; psr = reg.psr;
  int center_x = x + w / 2;
  int center_y = y + h / 2;

  float margin = 0.1;
  int shift = 0;

  if (!lost) {
    // check if region is near edge, if the camera can be moved and if psr is large enough
    if (center_x < cam_w * margin || center_x > cam_w * (1 - margin)) {
      cout << "horizontal camera move required" << endl;
      if (center_x < cam_w * margin) {
        shift = abs(cam_w * margin - center_x);
        cam_z -= shift_function(shift);
      }
      else {
        shift = abs(cam_w * (1 - margin) - center_x);
        cam_z += shift_function(shift);
      }
    }
    if (center_y < cam_h * margin || center_y > cam_h * (1 - margin)) {
      cout << "vertical camera move required" << endl;
      if (center_y < cam_h * margin) {
        shift = abs(cam_h * margin - center_y);
        cam_y += shift_function(shift);
      }
      else {
        shift = abs(cam_h * (1 - margin) - center_y);
        cam_y -= shift_function(shift);
      }
    }
    publish_camera_move(cam_y, cam_z);
  }
}

void statusCallback(const bebop_vicos::Status status) {
  tracking = status.tracking;
  if (tracking == false)
    psr = 0;
  lost = status.lost;
  // if lost, redetect
}

void fdCallback(const facedetector::Detection d) {
  faces = d;
  // if mode = 1 (facetrack) then send initialization message to tracker
  if (mode && (!tracking || lost)) {
    std::cout << "initing tracker by face" << std::endl;
    tracking = true;
    bebop_vicos::Im_reg msg = bebop_vicos::Im_reg();
    msg.image = frame;
    bebop_vicos::Region r = bebop_vicos::Region();
    r.x = faces.x.at(0);
    r.y = faces.y.at(0);
    r.w = faces.width.at(0);
    r.h = faces.height.at(0);
    msg.region = r;
    im_reg_pub.publish(msg);
  }
}

void processFaces(cv_bridge::CvImagePtr cv_ptr) {
  if (!tracking) {
    for (int i = 0; i < faces.x.size(); i++) {
      cv::rectangle(cv_ptr->image, cv::Rect(faces.x.at(i), faces.y.at(i), faces.width.at(i), faces.height.at(i)), cv::Scalar(255, 255, 255), 2);
    }
  }
  faces = facedetector::Detection();
}

void processRegion(cv_bridge::CvImagePtr cv_ptr) {
  // processes and displays tracked region
  double opacity = 0.7;
  if (h > 0 && w > 0) {
    if (!lost) {
      //cv::rectangle(cv_ptr->image, cv::Rect(x, y, w, h), cv::Scalar(255, 0, 0), 3);
      gui::drawAlphaBorder(cv_ptr, cv::Rect(x, y, w, h), opacity, cv::Scalar(255, 255, 255), 3, true);
    }
    else {
      //cv::rectangle(cv_ptr->image, cv::Rect(x, y, w, h), cv::Scalar(0, 0, 255), 3);
      //gui::drawAlphaBorder(cv_ptr, cv::Rect(x, y, w, h), opacity, cv::Scalar(0, 0, 255), 3, true);
    }
    h = 0; w = 0;
  }
}

void drawText(cv_bridge::CvImagePtr cv_ptr) {
  // displays status test and numbers
  std::stringstream fmt;

  fmt << "pan: " << cam_z << ", tilt: " << cam_y << "\n";
  fmt << "psr: " << psr << "\n";

  cv::Point pos = cv::Point(5 * cam_w / 100.0, cam_h - 20 * cam_h / 100.0); // text position
  gui::writeText(cv_ptr, pos, fmt.str().c_str());
}

void handle_key(int key, uint32_t sec, uint32_t nsec) {
  switch (key) {
  case (27): // 'esc' triggers shutdown
    ros::shutdown();
    break;
  case ((int)('q')): // if 'q' is pressed, stop tracker
    stop_pub.publish(std_msgs::Empty());
    tracking = false;
    lost = false;
    break;
  case ((int)('w')):
    if (cam_y + camera_shift_angle <= camera_tilt_limit_up)
      cam_y += camera_shift_angle;
    else
      cam_y = camera_tilt_limit_up;
    publish_camera_move(cam_y, cam_z);
    break;
  case ((int)('s')):
    if (cam_y - camera_shift_angle >= camera_tilt_limit_down)
      cam_y -= camera_shift_angle;
    else
      cam_y = camera_tilt_limit_down;
    publish_camera_move(cam_y, cam_z);
    break;
  case ((int)('a')):
    if (cam_z - camera_shift_angle >= -camera_pan_limit)
      cam_z -= camera_shift_angle;
    else
      cam_z = -camera_pan_limit;
    publish_camera_move(cam_y, cam_z);
    break;
  case ((int)('d')):
    if (cam_z + camera_shift_angle <= camera_pan_limit)
      cam_z += camera_shift_angle;
    else
      cam_z = camera_pan_limit;
    publish_camera_move(cam_y, cam_z);
    break;
  case ((int)('h')): // sets camera position back to default
    cam_y = 0;
    cam_z = 0;
    publish_camera_move(cam_y, cam_z);
    break;
  }
}

void imageCallback(const sensor_msgs::ImageConstPtr& cam_msg) {
  cv_bridge::CvImagePtr cv_ptr;

  try {
    cv_ptr = cv_bridge::toCvCopy(cam_msg, sensor_msgs::image_encodings::BGR8);
  }
  catch (cv_bridge::Exception& e) {
    ROS_ERROR("cv_bridge exception: %s", e.what());
    return;
  }

  frame = *cam_msg;

  // draw tracker region
  processRegion(cv_ptr);

  // draw status text
  drawText(cv_ptr);

  if (faces.x.size() > 0)
    processFaces(cv_ptr);

  // draw selection rectangle
  cv::rectangle(cv_ptr->image, sel, cv::Scalar(0, 0, 0), 2);

  cv::imshow(OPENCV_WINDOW, cv_ptr->image);

  handle_key(cv::waitKey(1), cam_msg->header.stamp.sec, cam_msg->header.stamp.nsec); // needs sec and nsec for creating record directories

}

void onMouse( int event, int x, int y, int f, void* ) {
  if (clicked && !tracking) {
    if (P1.x > P2.x) {
      sel.x = P2.x;
      sel.width = P1.x - P2.x;
    }
    else {
      sel.x = P1.x;
      sel.width = P2.x - P1.x;
    }

    if (P1.y > P2.y) {
      sel.y = P2.y;
      sel.height = P1.y - P2.y;
    }
    else {
      sel.y = P1.y;
      sel.height = P2.y - P1.y;
    }

  }
  switch (event) {
  case CV_EVENT_LBUTTONDOWN:
    clicked = true;
    P1.x = x;
    P1.y = y;
    P2.x = x;
    P2.y = y;
    break;
  case CV_EVENT_LBUTTONUP:
    P2.x = x;
    P2.y = y;
    // init tracker
    if (sel.area() > 0 && !tracking) {
      // send region and image
      tracking = true;
      bebop_vicos::Im_reg msg = bebop_vicos::Im_reg();
      msg.image = frame;
      bebop_vicos::Region r = bebop_vicos::Region();
      r.x = sel.x; r.y = sel.y; r.w = sel.width; r.h = sel.height;
      msg.region = r;
      im_reg_pub.publish(msg);
    }
    sel = cv::Rect(0, 0, 0, 0);
    clicked = false;
    break;
  case CV_EVENT_MOUSEMOVE:
    if (clicked) {
      P2.x = x;
      P2.y = y;
    }
    break;
  default: break;
  }
}

void on_trackbar( int, void* )
{

}


int main(int argc, char **argv)
{

  ros::init(argc, argv, "facetrack");

  ros::NodeHandle n("~");

  // subscribers
  sub_image = n.subscribe("/bebop/image_raw", 1, imageCallback);
  sub_region = n.subscribe("/tracker/region", 1, regionCallback);
  sub_status = n.subscribe("/bebop_vicos/status", 1, statusCallback);
  sub_fd = n.subscribe("/facedetector/faces", 1, fdCallback);
  sub_cam_pos = n.subscribe("/bebop/states/ARDrone3/CameraState/Orientation", 1, cameraPositionCallback);

  // publishers
  cam_move_pub = n.advertise<geometry_msgs::Twist>("/bebop/camera_control", 1000);
  im_reg_pub = n.advertise<bebop_vicos::Im_reg>("/tracker/init", 1000);
  shutdown_pub = n.advertise<std_msgs::Empty>("/master/shutdown", 1000);
  stop_pub = n.advertise<std_msgs::Empty>("/tracker/stop", 1000);

  cv::namedWindow(OPENCV_WINDOW, CV_WINDOW_AUTOSIZE);

  setMouseCallback(OPENCV_WINDOW, onMouse, NULL);

  // example trackbars
  createTrackbar("Mode", OPENCV_WINDOW, &mode, 1, on_trackbar);
  createTrackbar("Parameter", OPENCV_WINDOW, &parameter, 100);

  ros::spin();

  return 0;
}