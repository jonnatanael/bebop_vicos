#include "master.h"

// include for custom gui class
#include "gui.h"
#include "util.h"

// include for coordinate cross drawing
#include "coordinates.h"

using namespace std;
using namespace cv;

void publish_camera_move(float x, float y) {
	geometry_msgs::Twist msg;
	msg.angular.y = y;
	msg.angular.z = x;
	cam_move_pub.publish(msg);
}

void toggle_facedetector() {
	std_msgs::Bool msg;
	if (face_detector_running) {
		msg.data = false;
		face_detector_running = false;
	}
	else {
		msg.data = true;
		face_detector_running = true;
	}
	fd_toggle_pub.publish(msg);
}

void init_PID() {
	pid_x.setGains(x_Kp, x_Ki, x_Kd);
	pid_y.setGains(y_Kp, y_Ki, y_Kd);

	pid_x.setReference(x_ref);
	pid_y.setReference(y_ref);
}

int rangeRandomAlg (int min, int max) {
// for selecting a random integer from a defined range (i.e. selecting a random detected face)
	int n = max - min + 1;
	int remainder = RAND_MAX % n;
	int x;
	do {
		x = rand();
	}
	while (x >= RAND_MAX - remainder);
	return min + x % n;
}

void snapshot() {
	cout << "Onboard snapshot triggered" << endl;
	std_msgs::Empty msg;
	snapshot_pub.publish(msg);
}

void record() {
	std_msgs::Bool msg;
	if (bebop_rec) {
		std::cout << "starting record " << std::endl;
		msg.data = true;
	}
	else {
		std::cout << "stopping record " << std::endl;
		msg.data = false;
	}
	record_pub.publish(msg);
}

void bebopPidCallback(const bebop_vicos::drone_PID& msg) {
	roll = msg.roll;
	pitch = msg.pitch;
	yaw = msg.yaw;
	z = msg.z;

	// this is received when tracking is running
	if (rec || video) {
		pid_drone_file << std::to_string(msg.stamp.sec) << '_' << std::to_string(msg.stamp.nsec) << " ";
		pid_drone_file << msg.roll << " " << msg.pitch << " " << msg.yaw << " " << msg.z << " " << msg.flying_state << " ";
		pid_drone_file << msg.dx << " " << msg.dy << " " << msg.dd << " " << msg.alpha << endl;
	}
}

void attitudeCallback(const bebop_msgs::Ardrone3PilotingStateAttitudeChanged& msg) {
	rotX = -CV_PI / 2 + msg.pitch; // for display
	rotY = msg.roll;
	rotZ = msg.yaw;

	if (rec || video) {
		// save attitude
		attitude_file << std::to_string(msg.header.stamp.sec) << '_' << std::to_string(msg.header.stamp.nsec) << " ";
		attitude_file << msg.roll << "," << msg.pitch << "," << msg.yaw << endl;
	}
}

void altitudeCallback(const bebop_msgs::Ardrone3PilotingStateAltitudeChanged& msg) {
	if (rec || video) {
		// save attitude
		altitude_file << std::to_string(msg.header.stamp.sec) << '_' << std::to_string(msg.header.stamp.nsec) << " ";
		altitude_file << msg.altitude << endl;
	}
}

void batteryCallback(const bebop_msgs::CommonCommonStateBatteryStateChanged& msg) {
	if (msg.percent < 5) {
		cout << "BATTERY LOW!" << endl;
	}
	battery = msg.percent;
}

void cameraPositionCallback(const bebop_msgs::Ardrone3CameraStateOrientation& msg) {
	// this should only be called at init, so we get initial camera state
	if (cam_y == 1e5 || cam_x == 1e5) {
		cam_y = msg.tilt;
		cam_x = msg.pan;
	}
}

void shutdownNodes() {
	// used to propagate the shutdown signal to all related nodes
	shutdown_pub.publish(std_msgs::Empty());
}

int shift_function(int shift) {
	// a testing place for different weight functions for tracker-based camera shift.
	// seems a linear function is best
	float factor = 0.1;
	return shift * factor;
}

void set_tracker_pos(int x, int y) {
	geometry_msgs::Point msg = geometry_msgs::Point();
	msg.x = x;
	msg.y = y;
	tracker_set_reg_pub.publish(msg);
}

void regionCallback(const bebop_vicos::Region reg) {
	// saves current object region
	x = reg.x; y = reg.y; h = reg.h; w = reg.w; psr = reg.psr;
	x_c = x + w / 2;
	y_c = y + h / 2;


	float margin = 0.1;
	int shift = 0;
	bool move_needed = false;

	// calculate distance from image center
	distance_from_center = sqrt(pow((x_c - image_center_x), 2) + pow((y_c - image_center_y), 2));

	// compared to look_at this is quite boring and should be removed
	if (!lost && move_cam) {
		// check if region is near edge, if the camera can be moved and if psr is large enough
		if (x_c < cam_w * margin || x_c > cam_w * (1 - margin)) {
			if (x_c < cam_w * margin) {
				shift = abs(cam_w * margin - x_c);
				cam_x -= shift_function(shift);
			}
			else {
				shift = abs(cam_w * (1 - margin) - x_c);
				cam_x += shift_function(shift);
			}
			move_needed = true;
		}
		if (y_c < cam_h * margin || y_c > cam_h * (1 - margin)) {
			if (y_c < cam_h * margin) {
				shift = abs(cam_h * margin - y_c);
				cam_y += shift_function(shift);
			}
			else {
				shift = abs(cam_h * (1 - margin) - y_c);
				cam_y -= shift_function(shift);
			}
			move_needed = true;
		}
	}

	if (look_at) {
		// calculate camera error
		float d_x = -(x_c - image_center_x) / (float)cam_w;
		float d_y = (y_c - image_center_y) / (float)cam_h;

		// move only if necessary
		if (distance_from_center > center_deadband) {

			d_x = util::rad2deg(atan2(d_x, focal_length));
			d_y = util::rad2deg(atan2(d_y, focal_length));

			pid_x.setFeedback(d_x);
			pid_y.setFeedback(d_y);

			cam_pid_x = pid_x.getOutput();
			cam_pid_y = pid_y.getOutput();

			if (abs(cam_pid_x) > camera_control_deadband) {
				cam_x += cam_pid_x;
			}

			if (!target_angle_enabled) {
				if (abs(cam_pid_y) > camera_control_deadband) {
					cam_y += cam_pid_y;
				}
			}

			move_needed = true;
		}

	}

	// check if tilt angle is correct
	if (target_angle_enabled) {
		float diff = cam_y - target_angle;
		if (diff > 0 && abs(diff) > 1) {
			move_needed = true;
			cam_y -= 1;
		}
		else if (diff < 0 && abs(diff) > 1) {
			move_needed = true;
			cam_y += 1;
		}
	}

	if (rec || video) {
		// save region
		region_file << std::to_string(reg.stamp.sec) << '_' << std::to_string(reg.stamp.nsec) << " ";
		region_file << reg.x << "," << reg.y << "," << reg.h << "," << reg.w << "," << reg.psr << endl;

		pid_cam_file << std::to_string(reg.stamp.sec) << '_' << std::to_string(reg.stamp.nsec) << " ";
		pid_cam_file << cam_pid_x << " " << cam_pid_y << endl;

	}

	// send camera move command
	cam_y < 0 ? cam_y = std::max(-35.0f, cam_y) : cam_y = std::min(cam_y, 35.0f);
	cam_x < 0 ? cam_x = std::max(-35.0f, cam_x) : cam_x = std::min(cam_x, 35.0f);
	if (move_needed)
		publish_camera_move(cam_x, cam_y);


	// set tracker position from prediction
	if (prediction_pending) {
		if (delay == 0) {
			cout << "setting pos" << endl;
			set_tracker_pos(prediction.x, prediction.y);
			delay = default_delay;
			prediction_pending = false;
			//look_at = true;
		}
		else
			delay--;
	}

	// run tests
	if (test_running && tracking) {

		if (test_mode == 0) {
			if (look_at)
				test_counter++;

			if (abs(distance_from_center - move_size) < 20) { // if move was successfully performed

				if (move_timeout == 0) {
					cout << "move finished" << endl;
					// turn on the controller and wait for convergence
					look_at = true;
					test_counter = 0;
					move_timeout = move_timeout_backup;
					first_counter = -1;
				}
				else {
					move_timeout--;
				}

			}

			if (distance_from_center < center_deadband) {
				//cout << "a" << endl;
				if (first_counter < 0 && look_at) {
					first_counter = test_counter;
				}

				if (move_timeout == 0) {
					// convergence happened, turn off the controller and schedule next move
					cout << "convergence after " << first_counter << " frames." << endl;
					cout << "moving camera" << endl;
					look_at = false;
					while (!move_circle(move_size)) {} // ensure a valid move is performed
					move_timeout = move_timeout_backup;
					first_counter = -1;
				}
				else {
					move_timeout--;
				}
			}
			else if (distance_from_center > center_deadband && look_at && first_counter > 0) {
				// if center has entered the region and then moved out, we have oscillation
				cout << "oscillation" << endl;
				first_counter = test_counter;
			}
		}
	}
}

void statusCallback(const bebop_vicos::Status status) {
	// receive status message from tracker then save data to file

	tracking = status.tracking;
	if (tracking == false) {
		psr = 0;
		prediction_pending = false;
	}
	lost = status.lost;
	if (rec || video) {
		//save status
		status_file << std::to_string(status.stamp.sec) << '_' << std::to_string(status.stamp.nsec) << " ";
		status_file << std::to_string(status.tracking) << " " << std::to_string(status.lost) << " ";
		status_file << std::to_string(target_angle_enabled) << " " << std::to_string(target_angle_enabled ? target_angle : 0) << " ";
		status_file << std::to_string(look_at) << " ";
		status_file << std::to_string(test_running) << " ";
		status_file << std::to_string(test_mode) << " ";
		status_file << std::to_string(camera_move_pending) << " ";
		status_file << std::to_string(cam_x) << " ";
		status_file << std::to_string(cam_y) << " ";
		if (camera_move_pending) {
			camera_move_pending = false;
		}
		status_file << endl;

		if (!tracking) {
			region_file << std::to_string(status.stamp.sec) << '_' << std::to_string(status.stamp.nsec) << " ";
			region_file << -1 << "," << -1 << "," << -1 << "," << -1 << "," << -1 << endl;

			pid_cam_file << std::to_string(status.stamp.sec) << '_' << std::to_string(status.stamp.nsec) << " ";
			pid_cam_file << 0 << " " << 0 << endl;

			pid_drone_file << std::to_string(status.stamp.sec) << '_' << std::to_string(status.stamp.nsec) << " ";
			pid_drone_file << 0 << " " << 0 << " " << 0 << " " << 0 << " " << 0 << " " << 0 << " " << 0 << " " << 0 << " " << 0 << endl;
		}

	}
}

void featuresCallback(const bebop_vicos::PointVector v) {
	// Harris features callback
	pv = v;
}

void featuresKPCallback(const bebop_vicos::KPVector v) {
	// OpenCV KeyPoint callback
	kpv = v;
}

void fdCallback(const facedetector::Detection d) {
	// receives face detections
	faces = d;
}

void processFaces(cv_bridge::CvImagePtr cv_ptr) {
	// displays the face detections then initializes tracker on a random face

	if (faces.x.size() > 0 && init_mode == 2) {
		if (!tracking) {
			for (int i = 0; i < faces.x.size(); i++) {
				cv::rectangle(cv_ptr->image, cv::Rect(faces.x.at(i), faces.y.at(i), faces.width.at(i), faces.height.at(i)), cv::Scalar(255, 0, 0), 2);
			}

			// if initializing on faces, choose a random face and init tracker
			int randNum = rangeRandomAlg (0, faces.x.size() - 1);
			tracking = true;
			bebop_vicos::Im_reg msg = bebop_vicos::Im_reg();
			msg.image = frame;
			bebop_vicos::Region r = bebop_vicos::Region();
			r.x = faces.x.at(randNum); r.y = faces.y.at(randNum); r.w = faces.width.at(randNum); r.h = faces.height.at(randNum);
			msg.region = r;
			im_reg_pub.publish(msg);
		}
		else
			faces = facedetector::Detection();
	}
}

void processRegion(cv_bridge::CvImagePtr cv_ptr) {
	// draws the tracker region, the size reference and the trajectories
	double opacity = 0.7;
	if (h > 0 && w > 0) {
		cv::Point statePt = util::update_KF(&KF, &measurement, x, y, h, w);
		if (!lost) {
			//cv::rectangle(cv_ptr->image, cv::Rect(x, y, w, h), cv::Scalar(255, 0, 0), 3);
			gui::drawAlphaBorder(cv_ptr, cv::Rect(x, y, w, h), opacity, cv::Scalar(255, 255, 255), 3, true);
		}
		else {
			//cv::rectangle(cv_ptr->image, cv::Rect(x, y, w, h), cv::Scalar(0, 0, 255), 3);
			gui::drawAlphaBorder(cv_ptr, cv::Rect(x, y, w, h), opacity, cv::Scalar(0, 0, 255), 3, true);
		}

		//cv::rectangle(cv_ptr->image, cv::Rect(x_c-reference.width/2, y_c-reference.height/2, reference.width, reference.height), cv::Scalar(255, 255, 255), 2);
		gui::drawAlphaBorder(cv_ptr, cv::Rect(x_c - reference.width / 2, y_c - reference.height / 2, reference.width, reference.height), 0.3, cv::Scalar(255, 255, 255), 2);

		// draw line from current vertices to reference size vertices
		gui::drawAlphaLine(cv_ptr, cv::Point(x, y), cv::Point(x_c - reference.width / 2, y_c - reference.height / 2), 0.3, Scalar(255, 255, 255), 2);
		gui::drawAlphaLine(cv_ptr, cv::Point(x + w, y), cv::Point(x_c + reference.width / 2, y_c - reference.height / 2), 0.3, Scalar(255, 255, 255), 2);
		gui::drawAlphaLine(cv_ptr, cv::Point(x, y + h), cv::Point(x_c - reference.width / 2, y_c + reference.height / 2), 0.3, Scalar(255, 255, 255), 2);
		gui::drawAlphaLine(cv_ptr, cv::Point(x + w, y + h), cv::Point(x_c + reference.width / 2, y_c + reference.height / 2), 0.3, Scalar(255, 255, 255), 2);


		if (trajectory) {
			gui::handle_queue(cv_ptr, &v, cv::Point(x + w / 2, y + h / 2), cv::Scalar(0, 0, 255)); // trajectory
			gui::handle_queue(cv_ptr, &v_kf, statePt, cv::Scalar(0, 255, 0));
		}

		//h = 0; w = 0; // if this is commented
	}
}

void drawText(cv_bridge::CvImagePtr cv_ptr) {
	// draws status text diretly on the image

	std::stringstream fmt;

	fmt << "pan: " << cam_x << ", tilt: " << cam_y << "\n";
	//fmt << "battery: " << battery << "%, psr: " << psr << "\n";
	fmt << "target angle: " << target_angle << endl;
	// rounds to 3 decimals
	//fmt << "rotX: " << roundf(rotX * 1000) / 1000 << ", rotY: " << roundf(rotY * 1000) / 1000 << ", rotZ: " << roundf(rotZ * 1000) / 1000;
	fmt << "Initialization: ";
	if (init_mode == 0) {
		fmt << "manual rectangle";
	}
	else if (init_mode == 1) {
		fmt << "manual click";
	}
	else if (init_mode == 2) {
		fmt << "face detection";
	}

	fmt << "\n";

	fmt << "Camera: ";
	if (look_at) {
		fmt << "dynamic";
	}
	else {
		fmt << "static";
	}

	cv::Point pos = cv::Point(5 * cam_w / 100.0, cam_h - 25 * cam_h / 100.0); // text position
	gui::writeText(cv_ptr, pos, fmt.str().c_str());
}

void draw_data_text(Mat& img) {
	// draws status text on a separate image, toggled by show_data parameter

	std::stringstream col1, col2;
	col1 << "Bebop data:" << endl << endl;

	col1 << "attitude:" << endl;
	col1 << "    rotX: " << roundf(rotX * 1000) / 1000 << endl;
	col1 << "    rotY: " << roundf(rotY * 1000) / 1000 << endl;
	col1 << "    rotZ: " << roundf(rotZ * 1000) / 1000 << endl;

	col1 << "altitude:" << endl;
	col1 << "    " << altitude << endl;

	col1 << "camera position:" << endl;
	col1 << "    pan: " << cam_x << endl;
	col1 << "    tilt: " << cam_y << endl;

	col1 << "battery:" << endl;
	col1 << "    " << battery << "%" << endl;



	col2 << "System data:" << endl << endl;

	col2 << "tracking: " << (tracking ? "true" : "false") << endl;
	col2 << "lost: " << (lost ? "true" : "false") << endl;
	col2 << "psr: " << (tracking ? psr : -1) << endl;

	col2 << "target angle enabled: " << (target_angle_enabled ? "true" : "false") << endl;
	col2 << "target angle: " << (target_angle_enabled ? target_angle : -1) << endl;

	//col2 << "tracker initialization: " << (init_mode == 0 ? "manual" : "face detection") << endl;

	col2 << "tracker initialization: ";
	switch (init_mode) {
	case 0:
		col2 << "rectangle" << endl;
		break;
	case 1:
		col2 << "click" << endl;
		break;
	case 2:
		col2 << "face detection" << endl;
		break;
	}

	col2 << "camera: " << (look_at ? "dynamic" : "static") << endl;

	col2 << "displaying trajectory: " << (trajectory ? "true" : "false") << endl;
	col2 << "displaying features: " << (display_features ? "true" : "false") << endl;
	col2 << "recording locally: " << (rec ? "true" : "false") << endl;
	col2 << "recording onboard: " << (bebop_rec ? "true" : "false") << endl;

	// testing
	col2 << "distance from center: " << distance_from_center << endl;


	gui::writeText(img, Point(30, 30), col1.str().c_str());
	gui::writeText(img, Point(img.cols / 2, 30), col2.str().c_str());
}

void utilButtonCallback(const std_msgs::Int16& msg) {
	// here is defined what happens at each of the util button presses
	// except if this can be done earlier in the controller (like resetting camera)
	int n = msg.data;
	switch (n) {
	case 1:
		std::cout << n << std::endl;
		// resets camera
		cam_x = 0;
		cam_y = 0;
		publish_camera_move(cam_x, cam_y);
		break;
	case 2:
		// stop tracking
		stop_pub.publish(std_msgs::Empty());
		tracking = false;
		lost = false;
		h = 0; w = 0;
		std::cout << n << std::endl;
		// send empty message to enable hovering
		drone_pub.publish(geometry_msgs::Twist());
		break;
	case 3:
		// show/hide trajectory
		std::cout << trajectory << std::endl;
		util::toggle(trajectory);
		std::cout << trajectory << std::endl;
		std::cout << n << std::endl;
		break;
	case 4:
		// toggle camera move
		//toggle(move_cam);
		std::cout << n << std::endl;
		break;
	case 5:
		//toggle(display_features);
		std::cout << n << std::endl;
		break;
	case 6:
		std::cout << n << std::endl;
		break;
	case 7:
		std::cout << n << std::endl;
		break;
	}
}

bool move_circle(int radius) {
	// move camera on a circle defined by radius (used for testing)

	double direction = rng.uniform(0.0, 2 * M_PI);

	float x_angle = util::rad2deg(atan2(radius, focal_length)) * cos(direction);
	float y_angle = util::rad2deg(atan2(radius, focal_length)) * sin(direction);

	if (x_angle < 0) {
		cam_x + x_angle < -camera_pan_limit ? x_angle = -camera_pan_limit - cam_x : x_angle;
	}
	else {
		cam_x + x_angle > camera_pan_limit ? x_angle = camera_pan_limit - cam_x : x_angle;
	}

	if (y_angle < 0) {
		cam_y + y_angle < -camera_tilt_limit ? y_angle = -camera_tilt_limit - cam_y : y_angle;
	}
	else {
		cam_y + y_angle > camera_tilt_limit ? y_angle = camera_tilt_limit - cam_y : y_angle;
	}

	// recalculate pixel displacement using the camera angle limits
	float d_x = focal_length * tan(util::deg2rad(x_angle));
	float d_y = focal_length * tan(util::deg2rad(y_angle));

	// only choose valid displacements (those that don't move the object outside of camera's FoV)

	if (d_x > 0) {
		if (d_x + x + w > cam_w)
			return false;
	}
	if (d_y > 0) {
		if (d_y + y + h > cam_h)
			return false;
	}

	if (d_x < 0) {
		if (abs(d_x) > x)
			return false;
	}
	if (d_y < 0) {
		if (abs(d_y) > y)
			return false;
	}

	cam_x += x_angle;
	cam_y += y_angle;
	publish_camera_move(cam_x, cam_y);

	// PREDICTION
	if (predict && test_mode == 1) {
		// minus in plus zato ker sicer negiraš premik, ampak je y os slike obrnjena
		prediction_pending = true;
		predicted = Rect(x - d_x, y + d_y, w, h);
		prediction = Point(x_c - d_x, y_c + d_y);
	}
	return true;

}

void handle_key(int key, uint32_t sec, uint32_t nsec) {
	key = key % 256;
	//cout << key << endl;
	switch (key) {
	case ((int)('q')): // if 'q' is pressed, stop tracker
		stop_pub.publish(std_msgs::Empty());
		tracking = false;
		lost = false;
		predicted.width = -1;
		h = 0; w = 0;
		break;
	case ((int)('w')):
		cam_y = std::min((int)cam_y + camera_shift_angle, camera_tilt_limit);
		publish_camera_move(cam_x, cam_y);
		camera_move_pending = true;
		break;
	case ((int)('s')):
		cam_y = std::max((int)cam_y - camera_shift_angle, -camera_tilt_limit);
		publish_camera_move(cam_x, cam_y);
		camera_move_pending = true;
		break;
	case ((int)('a')):
		cam_x = std::max((int)cam_x - camera_shift_angle, -camera_pan_limit);
		publish_camera_move(cam_x, cam_y);
		camera_move_pending = true;
		break;
	case ((int)('d')):
		cam_x = std::min((int)cam_x + camera_shift_angle, camera_pan_limit);
		publish_camera_move(cam_x, cam_y);
		camera_move_pending = true;
		break;
	case ((int)('h')): // sets camera position back to default
		cam_y = 0;
		cam_x = 0;
		publish_camera_move(cam_x, cam_y);
		camera_move_pending = true;
		break;
	case ((int)('t')): // sets trajectory visibility
		util::toggle(trajectory);
		break;
	case ((int)('f')): // toggle feature display
		util::toggle(display_features);
		break;
	case ((int)('e')): // toggle initialization mode
		init_mode++;
		init_mode = init_mode % 3;

		// stop tracker when switching modes, seems logical
		stop_pub.publish(std_msgs::Empty());
		tracking = false;
		lost = false;
		break;
	case ((int)('c')): // snapshot
		snapshot();
		break;
	case ((int)('v')): // launch testing A
	{
		test_mode = 0;

		if (!test_running) {
			cout << "starting testing session A" << endl;

		}
		else {
			cout << "stopping testing session A" << endl;
		}
		util::toggle(test_running);
		move_timeout = move_timeout_backup;
		look_at = false;
	}
	break;
	case ((int)('b')): // launch testing B
	{
		test_mode = 1;
		while (!move_circle(move_size)) {}
	}
	break;
	case ((int)('x')): // toggles camera centering on object
		util::toggle(look_at);
		break;
	case ((int)('g')):
		cout << "face detection toggled" << endl;
		toggle_facedetector();
		break;
	case ((int)('y')):
	{
		cout << "target angle toggled" << endl;
		util::toggle(target_angle_enabled);
		if (target_angle_enabled) {
			std_msgs::Int16 msg;
			msg.data = target_angle;
			gamma_pub.publish(msg);
		}

		std_msgs::Bool m;
		m.data = target_angle_enabled;
		target_angle_status_pub.publish(m);
		break;
	}
	case ((int)('r')): // if 's' is pressed, stop recording
		if (!rec) {
			cout << "start recording" << endl;
			rec = true;
			//subdir = std::to_string(cam_msg->header.stamp.sec) + '_' + std::to_string(cam_msg->header.stamp.nsec);
			subdir = std::to_string(sec) + '_' + std::to_string(nsec);
			boost::filesystem::create_directories(im_dir + '/' + subdir);
			status_file.open(im_dir + '/' + subdir + '/' + status_filename);
			region_file.open(im_dir + '/' + subdir + '/' + region_filename);
			attitude_file.open(im_dir + '/' + subdir + '/' + attitude_filename);
		}
		else {
			cout << "stop recording" << endl;
			status_file.close();
			region_file.close();
			rec = false;
		}
		break;
	case (49): // '1'
		init_region.width -= init_region.width * init_scale; init_region.height -= init_region.height * init_scale;
		init_region.x = mouse.x - init_region.width / 2; init_region.y = mouse.y - init_region.height / 2;
		break;
	case (50): // '2'
		init_region.width += init_region.width * init_scale; init_region.height += init_region.height * init_scale;
		init_region.x = mouse.x - init_region.width / 2; init_region.y = mouse.y - init_region.height / 2;
		break;
	case (82): { // 'up arrow'
		target_angle = min(target_angle + 5, camera_tilt_limit_up);
		if (target_angle_enabled) {
			std_msgs::Int16 msg;
			msg.data = target_angle;
			gamma_pub.publish(msg);
		}
		break;
	}
	case (84): { // 'down arrow'
		target_angle = max(target_angle - 5, camera_tilt_limit_down);
		if (target_angle_enabled) {
			std_msgs::Int16 msg;
			msg.data = target_angle;
			gamma_pub.publish(msg);
		}
		break;
	}
	case (27): // 'esc' triggers shutdown
		shutdownNodes();
		ros::shutdown();
		break;
	}

}

void draw_keypoints(cv_bridge::CvImagePtr cv_ptr) {
	// draw features
	if (display_features && pv.v.size() > 0) {
		for (std::vector<bebop_vicos::Point2D>::const_iterator i = pv.v.begin(); i != pv.v.end(); ++i) {
			bebop_vicos::Point2D cur = *i;
			cv::circle(cv_ptr->image, cv::Point(cur.x, cur.y), 1, cv::Scalar(255, 255, 255), -1);
		}
	}

	// draw keypoints
	if (display_features && kpv.v.size() > 0) {
		for (std::vector<bebop_vicos::KeyPoint>::const_iterator i = kpv.v.begin(); i != kpv.v.end(); ++i) {
			bebop_vicos::KeyPoint cur = *i;
			cv::circle(cv_ptr->image, cv::Point(cur.pt.x, cur.pt.y), 2, cv::Scalar(0, 255, 255), -1);
			cv::circle(cv_ptr->image, cv::Point(cur.pt.x, cur.pt.y), cur.size, cv::Scalar(0, 255, 255), 1);
		}
	}
}

void draw_others(cv_bridge::CvImagePtr cv_ptr) {
	// draw only if tracking
	if (tracking) {
		processRegion(cv_ptr);

		if (show_pid_error) {
			int factor_pitch = 40;
			int line = 2;
			int factor = 100;
			// yaw
			cv::line(cv_ptr->image, cv::Point(x_c, y_c), cv::Point(x_c + yaw * factor, y_c), Scalar(0, 0, 255), line);
			// z
			cv::line(cv_ptr->image, cv::Point(x_c, y_c), cv::Point(x_c, y_c - z * factor), Scalar(255, 0, 0), line);
		}
	}

	// draw always

	// draw selection rectangle
	if (init_mode == 0)
		cv::rectangle(cv_ptr->image, sel, cv::Scalar(0, 0, 0), 2);
	else if (init_mode == 1 && !tracking)
		cv::rectangle(cv_ptr->image, init_region, cv::Scalar(0, 0, 0), 2);

	// draw status text
	//drawText(cv_ptr);

	// draw battery level
	/*if (battery != -1)
	    gui::drawBattery(cv_ptr, battery);

	// draw coordinate cross
	coordinates::drawAngles(cv_ptr, rotX, rotY, rotZ);*/

	if (predicted.width > 0)
		cv::rectangle(cv_ptr->image, predicted, cv::Scalar(0, 255, 0), 3);
}

void imageCallback(const sensor_msgs::ImageConstPtr& cam_msg) {
	// receives image from drone, converts it to Mat and displays additional information on the image

	cv_bridge::CvImagePtr cv_ptr;

	try {
		cv_ptr = cv_bridge::toCvCopy(cam_msg, sensor_msgs::image_encodings::BGR8);
	}
	catch (cv_bridge::Exception& e) {
		ROS_ERROR("cv_bridge exception: %s", e.what());
		return;
	}

	frame = *cam_msg;

	// save image before modifying it
	if (save_images) {
	    string s = im_dir + "/" + subdir + "/images/" + std::to_string(cam_msg->header.stamp.sec) + '_' + std::to_string(cam_msg->header.stamp.nsec) + ".jpg";
	    cout << s << endl;
	    cv::imwrite(s, cv_ptr->image);
	    //gui::drawRec(cv_ptr);
	}

	// draw features
	draw_keypoints(cv_ptr);

	draw_others(cv_ptr);

	processFaces(cv_ptr);

	if (video)
		vw << cv_ptr->image;

	if (!tracking) {
		v.clear();
		v_kf.clear();
		util::init_KF(&KF, &measurement); // reinit KF
	}

	cv::imshow(OPENCV_WINDOW, cv_ptr->image);

	if (show_data) {
		data_image.setTo(Scalar(0, 0, 0));
		draw_data_text(data_image);
		cv::imshow(DATA_WINDOW, data_image);
	}

	handle_key(cv::waitKey(1), cam_msg->header.stamp.sec, cam_msg->header.stamp.nsec); // needs sec and nsec for creating record directories

}

void onMouse( int event, int x, int y, int f, void* ) {
	// for one-click init
	mouse.x = x; mouse.y = y;
	init_region.x = x - init_region.width / 2; init_region.y = y - init_region.height / 2;

	if (clicked && !tracking && init_mode == 0) {
		if (P1.x > P2.x) {
			sel.x = P2.x;
			sel.width = P1.x - P2.x;
		}
		else {
			sel.x = P1.x;
			sel.width = P2.x - P1.x;
		}

		if (P1.y > P2.y) {
			sel.y = P2.y;
			sel.height = P1.y - P2.y;
		}
		else {
			sel.y = P1.y;
			sel.height = P2.y - P1.y;
		}
	}
	switch (event) {
	case CV_EVENT_LBUTTONDOWN:
		clicked = true;
		P1.x = x;
		P1.y = y;
		P2.x = x;
		P2.y = y;
		break;
	case CV_EVENT_RBUTTONDOWN: {
		set_tracker_pos(x, y);
		break;
	}
	case CV_EVENT_LBUTTONUP: {
		P2.x = x;
		P2.y = y;
		// init tracker
		bebop_vicos::Im_reg msg = bebop_vicos::Im_reg();
		bebop_vicos::Region r = bebop_vicos::Region();
		if (!tracking) {
			tracking = true;
			msg.image = frame;

			// if initializing with rectangle
			if (init_mode == 0 && sel.area() > 0) {
				r.x = sel.x; r.y = sel.y; r.w = sel.width; r.h = sel.height;
				reference = sel;
				msg.region = r;
				im_reg_pub.publish(msg);
			}
			else if (init_mode == 1) {
				r.x = init_region.x;
				r.y = init_region.y;
				r.w = init_region.width;
				r.h = init_region.height;
				reference = Rect(r.x, r.y, r.w, r.h);
				msg.region = r;
				im_reg_pub.publish(msg);
			}
		}
		sel = cv::Rect(0, 0, 0, 0);
		clicked = false;
		break;
	}
	case CV_EVENT_MOUSEMOVE:
		if (clicked && init_mode == 0) {
			if (clicked) {
				P2.x = x;
				P2.y = y;
			}
		}
		break;
	default: break;
	}
}

void get_params(ros::NodeHandle n) {

	n.getParam("image_topic", image_topic);

	n.getParam("status_file", status_filename);

	n.getParam("status_file", status_filename);
	n.getParam("region_file", region_filename);
	n.getParam("attitude_file", attitude_filename);
	n.getParam("altitude_file", altitude_filename);
	n.getParam("pid_cam_file", pid_cam_filename);
	n.getParam("pid_drone_file", pid_drone_filename);
	n.getParam("image_dir", im_dir);


	n.getParam("trajectory", trajectory);
	n.getParam("show_pid_error", show_pid_error);

	n.getParam("save_video", video);
	n.getParam("save_images", save_images);
	n.getParam("move_camera", move_cam);
	n.getParam("look_at", look_at);
	n.getParam("show_data", show_data);
	n.getParam("camera_shift_angle", camera_shift_angle);
	n.getParam("init_mode", init_mode);

	n.getParam("x_Kp", x_Kp);
	n.getParam("x_Ki", x_Ki);
	n.getParam("x_Kd", x_Kd);

	n.getParam("y_Kp", y_Kp);
	n.getParam("y_Ki", y_Ki);
	n.getParam("y_Kd", y_Kd);

	n.getParam("target_angle_enabled", target_angle_enabled);
	n.getParam("target_angle", target_angle);

	n.getParam("move_timeout", move_timeout);
	move_timeout_backup = move_timeout;
	n.getParam("move_size", move_size);

	n.getParam("center_deadband", center_deadband); // distance threshold for camera correction
	n.getParam("camera_control_deadband", camera_control_deadband);

	n.getParam("predict", predict);
	n.getParam("prediction_delay", delay);
}

int main(int argc, char **argv) {

	ros::init(argc, argv, "PID_test");

	rng = RNG(::getpid());

	ros::NodeHandle n("~");

	get_params(n);

	// subscribers
	sub_image = n.subscribe(image_topic, 1, imageCallback);
	sub_attitude = n.subscribe("/bebop/states/ardrone3/PilotingState/AttitudeChanged", 1, attitudeCallback);
	sub_altitude = n.subscribe("/bebop/states/ardrone3/PilotingState/AltitudeChanged", 1, altitudeCallback);
	sub_bat = n.subscribe("/bebop/states/common/CommonState/BatteryStateChanged", 1, batteryCallback);
	sub_cam_pos = n.subscribe("/bebop/states/ardrone3/CameraState/Orientation", 1, cameraPositionCallback);
	sub_region = n.subscribe("/tracker/region", 30, regionCallback); // 30 zato, da se pri zapisovanju ne izgubi kakšna slika
	sub_status = n.subscribe("/bebop_vicos/status", 30, statusCallback);
	sub_features = n.subscribe("/bebop/features", 1, featuresCallback);
	sub_featuresKP = n.subscribe("/bebop/KP", 1, featuresKPCallback);
	sub_util_buttons = n.subscribe("/bebop/util", 1, utilButtonCallback);
	sub_fd = n.subscribe("/facedetector/faces", 1, fdCallback);
	sub_drone_PID = n.subscribe("/follower/pid", 30, bebopPidCallback);


	// publishers
	cam_move_pub = n.advertise<geometry_msgs::Twist>("/bebop/camera_control", 1000);
	drone_pub = n.advertise<geometry_msgs::Twist>("/bebop/cmd_vel", 1000);
	im_reg_pub = n.advertise<bebop_vicos::Im_reg>("/tracker/init", 1000);
	shutdown_pub = n.advertise<std_msgs::Empty>("/master/shutdown", 1000);
	tracker_set_reg_pub = n.advertise<geometry_msgs::Point>("/tracker/set", 1000);
	stop_pub = n.advertise<std_msgs::Empty>("/tracker/stop", 1000);
	snapshot_pub = n.advertise<std_msgs::Empty>("/bebop/snapshot", 1000);
	record_pub = n.advertise<std_msgs::Bool>("/bebop/record", 1000);
	fd_toggle_pub = n.advertise<std_msgs::Bool>("/facedetector/toggle", 1000);
	target_angle_status_pub = n.advertise<std_msgs::Bool>("/master/target_angle_status", 1000);
	gamma_pub = n.advertise<std_msgs::Int16>("/master/gamma", 1000);


	init_PID();

	if (video) {
		ros::Time time = ros::Time::now();
		subdir = std::to_string(time.sec) + '_' + std::to_string(time.nsec);
		boost::filesystem::create_directories(im_dir + '/' + subdir);

		// stup video file
		vw = cv::VideoWriter();
		boost::filesystem::create_directories(im_dir);
		vw.open(im_dir + '/' + subdir + '/' + std::to_string(time.sec) + '_' + std::to_string(time.nsec) + "_video.avi", CV_FOURCC('M', 'P', 'E', 'G'), 30, cv::Size(cam_w, cam_h));

		if (save_images){
			boost::filesystem::create_directories(im_dir+'/'+subdir+"/images");
		}

		// setup data files
		status_file.open(im_dir + '/' + subdir + '/' + status_filename);
		region_file.open(im_dir + '/' + subdir + '/' + region_filename);
		attitude_file.open(im_dir + '/' + subdir + '/' + attitude_filename);
		altitude_file.open(im_dir + '/' + subdir + '/' + altitude_filename);
		pid_cam_file.open(im_dir + '/' + subdir + '/' + pid_cam_filename);
		pid_drone_file.open(im_dir + '/' + subdir + '/' + pid_drone_filename);
		params_file.open(im_dir + '/' + subdir + "/params.txt");

		// write launch parameters
		params_file << "PID_camera" << endl;
		params_file << "x_Kp: " << x_Kp << endl;
		params_file << "x_Ki: " << x_Ki << endl;
		params_file << "x_Kd: " << x_Kd << endl;
		params_file << "y_Kp: " << y_Kp << endl;
		params_file << "y_Ki: " << y_Ki << endl;
		params_file << "y_Kd: " << y_Kd << endl;

		params_file << "move_size: " << move_size << endl;
		params_file << "center_deadband: " << center_deadband << endl;


	}

	//cv::namedWindow(OPENCV_WINDOW, CV_WINDOW_AUTOSIZE); // static window
	cv::namedWindow(OPENCV_WINDOW, WINDOW_NORMAL); // resizable window
	cv::resizeWindow(OPENCV_WINDOW, 960, 552);
	if (show_data) {
		cv::namedWindow(DATA_WINDOW, CV_WINDOW_AUTOSIZE); // resizable window
		data_image = Mat(800, 800, CV_8UC3, Scalar(0, 0, 0));
	}

	// set initial region size based on image size
	if (init_w == -1 && init_h == -1) {
		int size = (min(cam_h, cam_w) * 0.15f); // set initial bounding box to 15% of image height (assuming image is landscape)
		init_h = size;
		init_w = size;
		init_region = Rect(-100, -100, (int)init_w, (int)init_h);
	}

	setMouseCallback(OPENCV_WINDOW, onMouse, NULL);

	util::init_KF(&KF, &measurement);

	ros::spin();

	return 0;
}